{
    "id": "3efd9b0f-c944-48b7-bae8-50fa83716dfe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_corgi_paw_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d0d1109-081a-402f-853b-97a40f1e4dae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3efd9b0f-c944-48b7-bae8-50fa83716dfe",
            "compositeImage": {
                "id": "3a94f91c-a4e7-4073-9552-77350e56feb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d0d1109-081a-402f-853b-97a40f1e4dae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a0165e2-bffe-4394-851e-c2effef652bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d0d1109-081a-402f-853b-97a40f1e4dae",
                    "LayerId": "97834c93-af77-4ef3-be11-b74e3fe2688f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "97834c93-af77-4ef3-be11-b74e3fe2688f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3efd9b0f-c944-48b7-bae8-50fa83716dfe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 8,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 12,
    "yorig": 14
}