{
    "id": "9c69316d-03ca-4aa8-823b-fd63603e5743",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_save",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90cc8c32-2aa6-46d1-ad8f-96360ec7dfa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c69316d-03ca-4aa8-823b-fd63603e5743",
            "compositeImage": {
                "id": "979ae835-d9b8-4247-b8da-40e5788a7a26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90cc8c32-2aa6-46d1-ad8f-96360ec7dfa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e235161e-64c7-4d45-82ce-d4bcfe25cf7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90cc8c32-2aa6-46d1-ad8f-96360ec7dfa9",
                    "LayerId": "495a32a7-59d8-41b9-ab96-5e5a59327b6a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "495a32a7-59d8-41b9-ab96-5e5a59327b6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c69316d-03ca-4aa8-823b-fd63603e5743",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 14
}