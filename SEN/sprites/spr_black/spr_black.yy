{
    "id": "de768532-a59f-44b2-a429-89aa22f954e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbd2a250-bc8f-4954-9e92-e53600c358f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de768532-a59f-44b2-a429-89aa22f954e5",
            "compositeImage": {
                "id": "4eddf516-7ab0-40da-967b-605283957491",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbd2a250-bc8f-4954-9e92-e53600c358f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42f73c7a-bf2d-4ece-953d-731e0240e460",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbd2a250-bc8f-4954-9e92-e53600c358f3",
                    "LayerId": "8c382e53-acdb-408c-8170-94d240e885cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "8c382e53-acdb-408c-8170-94d240e885cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de768532-a59f-44b2-a429-89aa22f954e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}