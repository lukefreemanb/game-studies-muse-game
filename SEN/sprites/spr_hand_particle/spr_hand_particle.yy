{
    "id": "7bd18b99-d063-4db1-9141-8fe632e79ec8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hand_particle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5cf8ed23-7203-4b6e-b4f6-71f9ff19d4cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bd18b99-d063-4db1-9141-8fe632e79ec8",
            "compositeImage": {
                "id": "a950466f-1901-4745-be30-fb9573ae55a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cf8ed23-7203-4b6e-b4f6-71f9ff19d4cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "761bfee1-903a-4984-b2dd-0d70387d4dd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cf8ed23-7203-4b6e-b4f6-71f9ff19d4cc",
                    "LayerId": "a43984eb-9d6f-4ad8-a8da-73747af43a71"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "a43984eb-9d6f-4ad8-a8da-73747af43a71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7bd18b99-d063-4db1-9141-8fe632e79ec8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}