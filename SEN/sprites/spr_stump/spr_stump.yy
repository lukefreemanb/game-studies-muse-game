{
    "id": "64654f11-8d78-446d-bb39-e5e9bac798b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 8,
    "bbox_right": 40,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5ba54bb-e329-4c3a-b557-157d5d7c4078",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64654f11-8d78-446d-bb39-e5e9bac798b6",
            "compositeImage": {
                "id": "66420471-2826-4ef4-9448-92e6fcbc3a7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5ba54bb-e329-4c3a-b557-157d5d7c4078",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f291ea76-b880-427e-a789-80d787163a76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5ba54bb-e329-4c3a-b557-157d5d7c4078",
                    "LayerId": "4c006e9f-c1a6-4ebd-97a2-d625e859a440"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "4c006e9f-c1a6-4ebd-97a2-d625e859a440",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64654f11-8d78-446d-bb39-e5e9bac798b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 47
}