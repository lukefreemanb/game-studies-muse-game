{
    "id": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_font_default_serif",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a670dc57-ef42-4ee5-8d53-c82a70c30ecf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "e6bc3d04-aa14-448c-8caa-8b9011dbad98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a670dc57-ef42-4ee5-8d53-c82a70c30ecf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "640a9584-d4b1-4efc-aefa-d567e8536dcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a670dc57-ef42-4ee5-8d53-c82a70c30ecf",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "55fb3a79-6347-4a50-a9ea-2dd4006e286a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "c0520a74-f654-4dee-9a30-a8465abff039",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55fb3a79-6347-4a50-a9ea-2dd4006e286a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aad2648e-a863-49e9-a50a-80de2992261e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55fb3a79-6347-4a50-a9ea-2dd4006e286a",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "d581acce-47ee-4de4-b0d3-1e0bc92e664c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "02c886f7-9b97-4f56-bbb6-f9e66eabea12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d581acce-47ee-4de4-b0d3-1e0bc92e664c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68ddbfdc-9e35-42c6-82ac-9815e83d7e9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d581acce-47ee-4de4-b0d3-1e0bc92e664c",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "099941a2-b190-4063-af21-1edd8642d823",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "3085b10b-629a-4c67-9a27-6d3bbec7a7d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "099941a2-b190-4063-af21-1edd8642d823",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e476a1f7-9458-4e9c-8de6-7589965b2bc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "099941a2-b190-4063-af21-1edd8642d823",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "c70042e1-e98a-4b22-a78e-f2bfaf3e44d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "1408558e-0c51-47f0-a651-a5f2769b3360",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c70042e1-e98a-4b22-a78e-f2bfaf3e44d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14584aed-4810-40d0-9f0b-2b1eec062b42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c70042e1-e98a-4b22-a78e-f2bfaf3e44d2",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "5c48b6c0-8845-497d-8947-147e80e74157",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "09a91eb8-09e2-4dda-b0a9-9e0610a997a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c48b6c0-8845-497d-8947-147e80e74157",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "267fd04d-3438-4b92-b9e0-fef01b0e9e16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c48b6c0-8845-497d-8947-147e80e74157",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "aa381e4a-ddbc-453f-a339-ae800962c779",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "13ee265e-c0de-4b47-9ec1-cd9e07da3f08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa381e4a-ddbc-453f-a339-ae800962c779",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6eb37c26-099c-486f-9e57-891eefd52977",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa381e4a-ddbc-453f-a339-ae800962c779",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "3fc71190-8148-46d3-a2a6-d6d8813d7cef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "eb4a3c60-b895-4e7b-b60c-82b04419490c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fc71190-8148-46d3-a2a6-d6d8813d7cef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a550076-2b9e-4f75-8aa7-5410c2e5edcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fc71190-8148-46d3-a2a6-d6d8813d7cef",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "900b97d1-833b-4686-a3cd-42d7bdc69f42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "2394e05d-1d01-4bbc-8ae1-450bed0506d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "900b97d1-833b-4686-a3cd-42d7bdc69f42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cb39e1d-ee64-4701-8eac-da0ae211b250",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "900b97d1-833b-4686-a3cd-42d7bdc69f42",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "19c55eb6-2ddf-4d3c-8f60-47465caea041",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "d791eee8-fa22-4e0f-b4ac-0407ac5633ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19c55eb6-2ddf-4d3c-8f60-47465caea041",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c417af6-82d7-4f0d-a20e-64bee66981bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19c55eb6-2ddf-4d3c-8f60-47465caea041",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "0236280f-13cf-46e5-892b-66fba3e916ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "405760cb-01e8-4aa1-bd40-1fb86fe8713f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0236280f-13cf-46e5-892b-66fba3e916ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d9d03a2-e620-4bbf-9d6d-872988c56eed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0236280f-13cf-46e5-892b-66fba3e916ce",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "c2835916-c3ee-4fe9-a93f-1e2f21052b61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "297662ae-c441-4e31-993a-cd1bf68bdfac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2835916-c3ee-4fe9-a93f-1e2f21052b61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b549014-a8f1-40f4-97fc-8601f0d4976c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2835916-c3ee-4fe9-a93f-1e2f21052b61",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "e89c2b9a-9d84-4a63-9564-bcd22acb41be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "df02f9ac-88b1-4ccc-a58b-6cde96a51ddc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e89c2b9a-9d84-4a63-9564-bcd22acb41be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6af96a53-5389-409f-833d-3759eb95baf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e89c2b9a-9d84-4a63-9564-bcd22acb41be",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "adef019b-e012-4c46-acbb-61137b5718b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "669c4973-ffde-4a3b-adc7-5d7397d35271",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adef019b-e012-4c46-acbb-61137b5718b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7862b2f1-cc2f-49d7-9b5c-17acc78be42d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adef019b-e012-4c46-acbb-61137b5718b5",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "98d8167d-7671-42bf-ad58-98576473a29f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "93d0fb73-3ad9-4950-b149-abcdcad7cb58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98d8167d-7671-42bf-ad58-98576473a29f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b704b50-c918-46b3-8903-a4728e3df78c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98d8167d-7671-42bf-ad58-98576473a29f",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "77818ce6-cc53-4eed-8a39-e46582b73f90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "8021a8a5-b22e-4238-bfb2-5304f450d3b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77818ce6-cc53-4eed-8a39-e46582b73f90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8585811-cf10-4e03-b1e9-bccd2450d30f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77818ce6-cc53-4eed-8a39-e46582b73f90",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "428df142-76f3-424e-b7ec-e9001f467afe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "c4d5a3cb-680f-4bff-bdc9-125a44998bbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "428df142-76f3-424e-b7ec-e9001f467afe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd7abdbf-8fbf-46c9-8472-4818bd4bfb12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "428df142-76f3-424e-b7ec-e9001f467afe",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "5b55e7c5-6fe9-4535-ac56-5d03f9e62008",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "ca52da2e-1d54-47d6-9f69-54d50adc2403",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b55e7c5-6fe9-4535-ac56-5d03f9e62008",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f4dc04c-0e89-41e0-8b72-614f7b41babe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b55e7c5-6fe9-4535-ac56-5d03f9e62008",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "5f354048-636c-4bc3-a258-f4397e7a7342",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "9118a6bd-814d-40a5-a78b-085da4ff173c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f354048-636c-4bc3-a258-f4397e7a7342",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9a3f121-e52f-47cb-98e0-f063b9b3f5a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f354048-636c-4bc3-a258-f4397e7a7342",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "828e5bd0-e0b3-46ab-8df8-926a122eb7d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "6cfecbc5-eec8-4a69-bc1d-0c14b5e0a2c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "828e5bd0-e0b3-46ab-8df8-926a122eb7d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e60636c1-0af9-4fb1-a0db-7009152928b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "828e5bd0-e0b3-46ab-8df8-926a122eb7d6",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "d2c0d56f-12e4-4b93-9189-efdcc76f71a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "b844128f-82c6-4929-96cc-131f87328985",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2c0d56f-12e4-4b93-9189-efdcc76f71a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd30b0d8-7b90-4096-b254-922fc9b79e2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2c0d56f-12e4-4b93-9189-efdcc76f71a3",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "964550c4-4e40-4869-ae48-7e4ed592748c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "c4d46485-cefc-4e17-b311-8f171a4b82d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "964550c4-4e40-4869-ae48-7e4ed592748c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ea96d33-7aa5-481b-abd2-b1cbeb364dba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "964550c4-4e40-4869-ae48-7e4ed592748c",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "076e8b92-f93a-4774-8a43-45901e9a80c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "9552e180-0a0f-498f-ad01-88b00c8d1049",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "076e8b92-f93a-4774-8a43-45901e9a80c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "817e8056-5b5f-4326-8255-ac25073b5c9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "076e8b92-f93a-4774-8a43-45901e9a80c7",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "33eb04e4-47d3-4711-948b-089be8c3a6a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "c36361a5-ae22-4c37-b0bd-a99a18b39161",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33eb04e4-47d3-4711-948b-089be8c3a6a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f1588ec-295d-43fe-8c6d-a3cecc482dc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33eb04e4-47d3-4711-948b-089be8c3a6a8",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "db15217a-cdf6-4ad0-bd56-1112aea676bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "bb10a43f-f6c9-4f15-8f12-64a7f96f9516",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db15217a-cdf6-4ad0-bd56-1112aea676bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1514564f-6182-4041-b9ff-2dc1daac1aa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db15217a-cdf6-4ad0-bd56-1112aea676bb",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "e157e32a-a3b0-4e21-b41b-606e819d3e93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "2cbc2af7-c012-4f47-a458-f9fb6edd5583",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e157e32a-a3b0-4e21-b41b-606e819d3e93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c4e3a3d-6db7-4680-8a1e-aea292cf2f81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e157e32a-a3b0-4e21-b41b-606e819d3e93",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "db0eed01-bba7-4de0-99b8-d2bb3f52ea6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "53472bc5-a403-45f7-a3b1-4b09dde11964",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db0eed01-bba7-4de0-99b8-d2bb3f52ea6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34bb776d-01d0-41d8-a755-4cf93fa89e51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db0eed01-bba7-4de0-99b8-d2bb3f52ea6e",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "9aca7ee9-edb9-4113-aa5f-bfac403dfc33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "92937659-f373-42fe-8865-a581e78b4a36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9aca7ee9-edb9-4113-aa5f-bfac403dfc33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78ccd865-8486-4a47-a479-e93f1e638b6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aca7ee9-edb9-4113-aa5f-bfac403dfc33",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "ef8eff41-a821-4468-b5d7-0cac88640d3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "b1fa17d9-33d1-4960-9679-2946667e1536",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef8eff41-a821-4468-b5d7-0cac88640d3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "861648d1-82df-40b2-b6cb-cd01a1aa5d31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef8eff41-a821-4468-b5d7-0cac88640d3f",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "2a49f30e-202a-4daf-9ef4-d983b4f66dbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "82aff02e-8aa9-48cc-915f-d17338497279",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a49f30e-202a-4daf-9ef4-d983b4f66dbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eae0e23a-151f-4f87-b337-0488edd9fad6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a49f30e-202a-4daf-9ef4-d983b4f66dbf",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "c23cbb45-d831-4e50-b3cb-453ebeb6aef5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "bd042a8c-48a2-4a5d-9865-69eb1070197a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c23cbb45-d831-4e50-b3cb-453ebeb6aef5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2055dcf0-e1ac-4b07-9927-1ff38cc570c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c23cbb45-d831-4e50-b3cb-453ebeb6aef5",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "c0ccb21b-c85a-403a-be97-60d17769ab8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "7f956fb0-95cc-4755-8569-482ca95467c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0ccb21b-c85a-403a-be97-60d17769ab8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "941cf342-bb95-4de2-b39a-1d13e4c95f24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0ccb21b-c85a-403a-be97-60d17769ab8c",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "eda187eb-ee85-4930-8b90-dedbf4a81627",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "0b0f7411-9734-4749-b54e-1eb14453a414",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eda187eb-ee85-4930-8b90-dedbf4a81627",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83d37158-0a7c-4fb2-8c02-1de65f5ffc9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eda187eb-ee85-4930-8b90-dedbf4a81627",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "4d1e452a-d2f4-4295-9926-d1fb422c5e12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "bd122e96-7846-43f0-b2a8-642e67ae3acc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d1e452a-d2f4-4295-9926-d1fb422c5e12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4e4507b-4129-4d58-944a-b674fd2a6c6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d1e452a-d2f4-4295-9926-d1fb422c5e12",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "ae337676-f664-458f-83f1-8300ca802869",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "9b2025ce-2eb8-4755-9594-1dcd5bd8a072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae337676-f664-458f-83f1-8300ca802869",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b016947-d6ec-443c-9f96-ed812f0b23f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae337676-f664-458f-83f1-8300ca802869",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "bd687e72-1565-4f6c-9bf1-94e48506c472",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "68260605-2d49-46ac-882b-31d4cafd5ae0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd687e72-1565-4f6c-9bf1-94e48506c472",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fcf72b2-172f-4c55-9882-d42a93b84443",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd687e72-1565-4f6c-9bf1-94e48506c472",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "0b8d9e47-bd56-4f4e-9711-15207d663c67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "21190aaf-3bd9-4e7f-8933-e5d3a805efdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b8d9e47-bd56-4f4e-9711-15207d663c67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab024ba0-df9f-4209-ba75-c8d263b804b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b8d9e47-bd56-4f4e-9711-15207d663c67",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "4dc97c53-f82d-4021-a1a1-40ce9eebe388",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "0ea3ffc0-9c6b-49c4-a140-5fd663968780",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dc97c53-f82d-4021-a1a1-40ce9eebe388",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61b4a344-acd3-4ad8-b52a-4d5bbaf08e70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dc97c53-f82d-4021-a1a1-40ce9eebe388",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "a573d4ab-be18-4aeb-953f-2ee28522cbbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "6fba83f0-0895-4d12-8b04-7523a8776bee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a573d4ab-be18-4aeb-953f-2ee28522cbbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48abd792-3cee-453e-a6fa-87e0fcb3c734",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a573d4ab-be18-4aeb-953f-2ee28522cbbb",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "87b2f763-7343-4bde-9587-040b748ce288",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "b5e82c12-19bb-4a7e-b416-cc596f499132",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87b2f763-7343-4bde-9587-040b748ce288",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23d8aab7-621b-48ac-bb43-ffc45b6c1617",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87b2f763-7343-4bde-9587-040b748ce288",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "4e3d7587-2b9b-4056-9e92-3b8288365853",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "947658e8-918c-4a3e-83ca-cb44e569f2c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e3d7587-2b9b-4056-9e92-3b8288365853",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfc04207-ee59-4c88-ba1c-7e9b7ff28406",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e3d7587-2b9b-4056-9e92-3b8288365853",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "4ff29049-3326-4709-86b1-036ad0d4800f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "99be0743-6539-42d0-a2dd-e07cdff8d1db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ff29049-3326-4709-86b1-036ad0d4800f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d92af23-79d6-403f-8dfc-067a586563b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ff29049-3326-4709-86b1-036ad0d4800f",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "95a215e6-56b8-45ed-9cd5-556044b8f2c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "a56d2b54-e8dd-46fa-ad6f-5e8d30e7c70e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95a215e6-56b8-45ed-9cd5-556044b8f2c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8c01f35-37bd-43b9-bef1-ada14f549d57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95a215e6-56b8-45ed-9cd5-556044b8f2c4",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "f96b4400-e6c7-42ab-aeb7-9091fb52dfe9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "1700cf36-6f92-4ccd-b787-d32c43c727fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f96b4400-e6c7-42ab-aeb7-9091fb52dfe9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb57e82d-0a8e-42d9-a1aa-a7ad5a37ffcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f96b4400-e6c7-42ab-aeb7-9091fb52dfe9",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "5a52e297-f862-4633-a2c3-fea80310f52c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "2463ef0c-a5d1-4432-9f04-349799eb0e1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a52e297-f862-4633-a2c3-fea80310f52c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2e9d26c-d77b-411b-b4b5-5385f2f94103",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a52e297-f862-4633-a2c3-fea80310f52c",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "9911c078-77e4-47a4-8baf-ac0187f7b9d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "0086fbbc-f479-4e3d-9083-a828b7c48237",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9911c078-77e4-47a4-8baf-ac0187f7b9d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea3b63d9-8f09-4688-a9ff-48d8af296fc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9911c078-77e4-47a4-8baf-ac0187f7b9d8",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "6d107edb-707c-4f55-a0d2-db7a40896bfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "f5bb1c80-c7f0-4422-b98b-658f80cddfaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d107edb-707c-4f55-a0d2-db7a40896bfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c2a5cc0-45d5-4aa9-ba18-57243aeb7a91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d107edb-707c-4f55-a0d2-db7a40896bfa",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "ff6da7f4-ebb5-4890-9284-cc65b50d2cd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "3f15f5ed-4c58-4dc3-8f37-e945543dbbfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff6da7f4-ebb5-4890-9284-cc65b50d2cd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4d8bb04-c237-4d5a-8f43-37208cbeab8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff6da7f4-ebb5-4890-9284-cc65b50d2cd3",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "f204e854-037b-4603-a395-11501b16cc20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "52f2fa05-3714-4e9b-a940-80d8bf4e7b18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f204e854-037b-4603-a395-11501b16cc20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ce68512-56ae-466b-abff-519c436d0aec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f204e854-037b-4603-a395-11501b16cc20",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "e13a486d-3d9f-49c8-9b47-791a6f0c9e46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "4ca16a2d-9e05-49b6-bf27-38457659e743",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e13a486d-3d9f-49c8-9b47-791a6f0c9e46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57a20f4a-ec45-44a3-a629-fb608ebb16e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e13a486d-3d9f-49c8-9b47-791a6f0c9e46",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "aae2a778-b282-453b-a1fd-74e5bdf91b20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "5d5d5e6b-fb06-4088-abbb-7ef3d1c0337c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aae2a778-b282-453b-a1fd-74e5bdf91b20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "458127c5-bac6-40a0-8a68-0270ffbd2570",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aae2a778-b282-453b-a1fd-74e5bdf91b20",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "5d66bc7d-198b-4ff9-9010-9cd534cc7499",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "d9172dd4-24f4-4d87-b644-9c3d148f9f3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d66bc7d-198b-4ff9-9010-9cd534cc7499",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "505e7950-36c8-44b1-bf06-3800e3b52e0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d66bc7d-198b-4ff9-9010-9cd534cc7499",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "d5cecf40-27a7-4243-b30b-3ecc4a5e32c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "7c40686a-a4d6-488d-8309-c643c46680f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5cecf40-27a7-4243-b30b-3ecc4a5e32c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dcd93f7-bd2a-4cb4-8625-174548f05190",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5cecf40-27a7-4243-b30b-3ecc4a5e32c6",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "267beb8a-2080-4446-a0c5-9083146d526d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "9e28a0eb-8072-45f8-8d24-ecb05c1739cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "267beb8a-2080-4446-a0c5-9083146d526d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f519bc6-1644-42bc-93ec-45c8e58d1415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "267beb8a-2080-4446-a0c5-9083146d526d",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "342762ed-ee58-4d59-aa4c-aa02a58c19d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "18c9b2bc-3936-4cca-a797-c6ec8b39362e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "342762ed-ee58-4d59-aa4c-aa02a58c19d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ad1bb0c-a857-42d9-909a-6546b192c61f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "342762ed-ee58-4d59-aa4c-aa02a58c19d5",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "36b36b2a-ca48-4ea8-b88f-62c20792b4a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "a5c2519e-5f64-4315-86cd-c04db1e52fc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36b36b2a-ca48-4ea8-b88f-62c20792b4a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf375973-8b8d-407c-b09c-ffe40b140d82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36b36b2a-ca48-4ea8-b88f-62c20792b4a9",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "a992499d-2654-476d-9894-b6c519b041b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "8bbebbb1-7b5e-4777-9913-202dd1c38254",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a992499d-2654-476d-9894-b6c519b041b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aebc4658-c842-4576-b16c-33cca756076e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a992499d-2654-476d-9894-b6c519b041b5",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "cce7acd4-8ce0-410c-95ca-c92f8ca2edb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "ef5f5655-f65e-4aa0-b4c9-b1f9dadf899d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cce7acd4-8ce0-410c-95ca-c92f8ca2edb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b98c6bd9-7df2-4786-a9fa-caa429d6130e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cce7acd4-8ce0-410c-95ca-c92f8ca2edb2",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "730e01e7-2260-41a9-a079-d4d5a62cb099",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "6cb2d5c6-d184-4d16-8623-7b75924f4718",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "730e01e7-2260-41a9-a079-d4d5a62cb099",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4227f049-e40a-48cc-bc91-97a33563c2db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "730e01e7-2260-41a9-a079-d4d5a62cb099",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "6910ee84-0afd-4600-bd09-c021d5a5dec9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "d6c962b8-3be2-4672-8304-2277de837cb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6910ee84-0afd-4600-bd09-c021d5a5dec9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1edff41f-2761-4847-8fb2-f86259144980",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6910ee84-0afd-4600-bd09-c021d5a5dec9",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "749ac815-6b8a-441c-bd9a-e804f65acf08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "8f8372b5-8e0e-4b7c-8dc1-82305d9f2366",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "749ac815-6b8a-441c-bd9a-e804f65acf08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcd8be62-a370-44df-be76-8cb21926f7d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "749ac815-6b8a-441c-bd9a-e804f65acf08",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "a80e825a-31b0-4815-9c10-84ce3d2fd078",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "a32617a6-7051-4d60-bfcd-4698204d4af0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a80e825a-31b0-4815-9c10-84ce3d2fd078",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c7f1851-8c0e-418b-8fba-68a4e22de561",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a80e825a-31b0-4815-9c10-84ce3d2fd078",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "03759573-db99-4984-9b24-4c7748e84c6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "145cae51-13e1-4884-b2f9-ce44409fd7c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03759573-db99-4984-9b24-4c7748e84c6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bc0fc9a-627b-4553-99c4-ab5dd4a5a6ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03759573-db99-4984-9b24-4c7748e84c6f",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "636bc1b4-d790-442d-b245-ee4cc6f28142",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "3538e503-79d2-4322-a26b-fb6eac44ac2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "636bc1b4-d790-442d-b245-ee4cc6f28142",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "334885b6-eb55-41e3-a6e6-75cb14251a1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "636bc1b4-d790-442d-b245-ee4cc6f28142",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "1e5a30e1-5d87-4b11-877f-1d36321c78dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "f1cbce2b-3002-40af-a4fb-500885471ae7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e5a30e1-5d87-4b11-877f-1d36321c78dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71e9e919-8552-4d77-b907-024c74e97877",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e5a30e1-5d87-4b11-877f-1d36321c78dd",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "9eb0a525-5698-4e80-8eec-eaf9ed2ef2d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "31c1db64-0235-4a9a-939c-0cd3062b548c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eb0a525-5698-4e80-8eec-eaf9ed2ef2d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4c46d58-9f24-4ee5-83a7-358ad2221d83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eb0a525-5698-4e80-8eec-eaf9ed2ef2d2",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "c0cd817b-a107-4dcd-81b3-938ff2b8fe78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "ee35045c-2974-4dba-a2b6-66d3fa4e8e8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0cd817b-a107-4dcd-81b3-938ff2b8fe78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "485f8078-5c65-430f-b4fe-af39241d0525",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0cd817b-a107-4dcd-81b3-938ff2b8fe78",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "862bb7bd-a418-4a22-985f-21defb6fed07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "1f3ac0c6-0f88-4649-858c-56f17c0c73af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "862bb7bd-a418-4a22-985f-21defb6fed07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "366c67ca-f35f-47f2-a933-67e37b5f2537",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "862bb7bd-a418-4a22-985f-21defb6fed07",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "4a110de7-45ff-45bd-9b52-2268f2b3f27f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "5cc7cf4a-9565-4736-ade6-0011cb84197a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a110de7-45ff-45bd-9b52-2268f2b3f27f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0073cf09-1e9f-4a21-9a6a-34e5fda5e0e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a110de7-45ff-45bd-9b52-2268f2b3f27f",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "da50cafb-a839-47fd-b124-3c4608d75b8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "9006b0e4-afa0-4a8d-bef9-47bc9e2cbd3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da50cafb-a839-47fd-b124-3c4608d75b8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9607ae2e-e87d-4f21-bd42-869d2b5567e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da50cafb-a839-47fd-b124-3c4608d75b8e",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "2078b54b-0c0b-46c1-b2d5-ba60f84939c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "15894540-0ce6-4300-b93f-15c9744fcf56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2078b54b-0c0b-46c1-b2d5-ba60f84939c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ae899bb-c1f4-4d6d-b892-c59558cb3b9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2078b54b-0c0b-46c1-b2d5-ba60f84939c1",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "27077115-e485-4369-acd4-f9cc070b8736",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "2acdad91-22d0-4a5b-b460-db139ceeca5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27077115-e485-4369-acd4-f9cc070b8736",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afebecfa-9a6a-4add-bbdc-81c4fa6d4c4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27077115-e485-4369-acd4-f9cc070b8736",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "86bfaee2-d335-4d05-8153-10f4b8863c2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "8c284136-01cc-4cf8-bf2c-0bcf195d74e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86bfaee2-d335-4d05-8153-10f4b8863c2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a2fa99e-fb2f-4f61-a132-347874f0e6ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86bfaee2-d335-4d05-8153-10f4b8863c2b",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "b6c0b26d-622f-4a47-ad68-e0035a960334",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "4ff27d27-8754-42ad-9544-16528e60c5f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6c0b26d-622f-4a47-ad68-e0035a960334",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0cbaf16-7344-4ed4-8b52-70392a217374",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6c0b26d-622f-4a47-ad68-e0035a960334",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "f742578b-6b35-4ec4-8a51-687c914fe3f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "29a23950-370f-413d-b55b-c5d518750668",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f742578b-6b35-4ec4-8a51-687c914fe3f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bedfe96-ef4d-47e4-a237-1016c6301ecd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f742578b-6b35-4ec4-8a51-687c914fe3f1",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "f3b1a047-420c-4b3a-a9f6-e5fd8fd4b919",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "a8660aa8-f35f-4351-ab62-6639640e5c38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3b1a047-420c-4b3a-a9f6-e5fd8fd4b919",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efdbf01d-f1bb-42af-b248-b82700ed6eda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3b1a047-420c-4b3a-a9f6-e5fd8fd4b919",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "9f545b27-1f74-4780-af7e-45ede05d0571",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "4f02dd4b-b42b-4a2b-a2e0-1b3a9902c700",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f545b27-1f74-4780-af7e-45ede05d0571",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fd46e6d-8b7f-4d28-89ca-ad02b4a75ea2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f545b27-1f74-4780-af7e-45ede05d0571",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "fbc2e0e1-d7d9-4dcd-a0c4-44204f42a033",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "fecf7572-6af7-484f-87cc-3c6980f33ff1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbc2e0e1-d7d9-4dcd-a0c4-44204f42a033",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b15970b2-2a66-4c96-889d-aa98a6a4efbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbc2e0e1-d7d9-4dcd-a0c4-44204f42a033",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "a9e2038a-16d9-438a-929f-1196a324af52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "4325aa14-ee19-47a6-a03d-415e5703340d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9e2038a-16d9-438a-929f-1196a324af52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09636e85-25f0-4c5d-8b92-a9ef90c1dd94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9e2038a-16d9-438a-929f-1196a324af52",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "bb861d82-ac0e-42c7-9d00-96c8fd7984ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "c1b4c0a7-c7de-4bae-9d44-634d7092cbc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb861d82-ac0e-42c7-9d00-96c8fd7984ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16f9a155-ba11-4771-8b27-413d5115afad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb861d82-ac0e-42c7-9d00-96c8fd7984ac",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "6796ad05-713c-4fd7-94c4-4ca6bf09f30e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "76e304ea-7e77-4e9a-95cb-aedb5f06af90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6796ad05-713c-4fd7-94c4-4ca6bf09f30e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2907e80-1d69-4328-a668-eb582a53d7d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6796ad05-713c-4fd7-94c4-4ca6bf09f30e",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "324721d9-e4e5-4135-93b7-0ba810268ae2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "85aec376-ac25-47ca-bc44-8484abcbcb48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "324721d9-e4e5-4135-93b7-0ba810268ae2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75ae2523-784b-4d6c-924a-67d8a8f3f59c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "324721d9-e4e5-4135-93b7-0ba810268ae2",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "b01b3b5a-28e8-4cd8-b43d-2bcebd795c85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "35a056be-9059-4bd0-8212-933afd5a1eaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b01b3b5a-28e8-4cd8-b43d-2bcebd795c85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b312eebf-30cb-4335-a422-5addc72b46c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b01b3b5a-28e8-4cd8-b43d-2bcebd795c85",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "f94c3e0a-a23f-45bf-b75a-f785f719d4f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "018446e1-1680-4209-b0b6-d980dfae2336",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f94c3e0a-a23f-45bf-b75a-f785f719d4f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f3c6125-b59f-40a5-9b77-4bbe360eca88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f94c3e0a-a23f-45bf-b75a-f785f719d4f7",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "c07022e8-45c5-46f3-af22-eb801ad4fce1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "2634ef29-42c1-4333-8ef1-9c6e68ba089a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c07022e8-45c5-46f3-af22-eb801ad4fce1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3291d2e9-f273-47c9-b0a2-3a44be1da599",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c07022e8-45c5-46f3-af22-eb801ad4fce1",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "1e083bfb-9871-43e4-86b5-69dfa7ca19e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "32181e1a-06e4-4b06-abf2-87c0cbad0991",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e083bfb-9871-43e4-86b5-69dfa7ca19e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5afd4e18-2683-429d-a674-240ac4aae700",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e083bfb-9871-43e4-86b5-69dfa7ca19e2",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "1945c986-04f0-4c8a-8d8d-48a2c10a81fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "09af0059-f13e-483b-8687-8eea66822e01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1945c986-04f0-4c8a-8d8d-48a2c10a81fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5efd874-7f7d-42c9-9bbe-5464eb752aaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1945c986-04f0-4c8a-8d8d-48a2c10a81fc",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "0321f29d-5238-4a63-b36c-5876501e5407",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "92fa2f22-374d-481e-8441-dd8519596bd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0321f29d-5238-4a63-b36c-5876501e5407",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "444d045a-1a29-4e80-9791-37a864e72f5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0321f29d-5238-4a63-b36c-5876501e5407",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "33bf3f01-580c-4c87-ad35-f401e19b8b5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "b85d720f-61ef-45f8-80f2-b2269e474f53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33bf3f01-580c-4c87-ad35-f401e19b8b5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23547fcd-824c-449c-ba8d-f155e21b2715",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33bf3f01-580c-4c87-ad35-f401e19b8b5d",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "e84120dc-d4f7-4ca0-a31b-59efb38a3f77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "8cbf2e45-70f2-4a7b-ab9e-009a2bb7b490",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e84120dc-d4f7-4ca0-a31b-59efb38a3f77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fd6302f-aa70-4c01-b29e-1c5c343e4105",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e84120dc-d4f7-4ca0-a31b-59efb38a3f77",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "f05f45f1-028e-43cd-a081-31c12d75f2d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "cdda06c6-b597-4054-9bef-03c99402309a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f05f45f1-028e-43cd-a081-31c12d75f2d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cfe3510-c676-442a-a949-fa6515166617",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f05f45f1-028e-43cd-a081-31c12d75f2d1",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "7cd8d27d-327b-4500-b903-05bdea767d8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "8dccabf0-428e-41b1-b532-b06ab8539a97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cd8d27d-327b-4500-b903-05bdea767d8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b072350e-c2a7-4a46-92f8-6658994711fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cd8d27d-327b-4500-b903-05bdea767d8f",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "a71b0fe8-29d0-4974-b9b2-1c7900c5ced8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "adf090ce-d0ae-446a-8d2e-0875c58a9b09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a71b0fe8-29d0-4974-b9b2-1c7900c5ced8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0ef0937-78ae-4be4-9ccb-a9e3a6d7fa3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a71b0fe8-29d0-4974-b9b2-1c7900c5ced8",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "9e527960-827f-4b57-8e67-3172e14049f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "4b928e12-43b1-4c2a-bff6-d3030d2e3da5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e527960-827f-4b57-8e67-3172e14049f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b990b5d4-b6d9-46a9-b36f-43009ca2bb6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e527960-827f-4b57-8e67-3172e14049f1",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "5203859e-985a-4341-8895-46c18a802601",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "81092a44-1fe0-4869-b6c9-16cd822e7a11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5203859e-985a-4341-8895-46c18a802601",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a2e7d1f-ac2d-48b6-afd8-6a9f1bf7931e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5203859e-985a-4341-8895-46c18a802601",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "e78efa3a-74f9-45a9-b9c4-79bfc48109ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "73939ab7-c94c-47a3-a875-9f7849ec4724",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e78efa3a-74f9-45a9-b9c4-79bfc48109ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b48f5837-4243-45bf-9870-8ec429ba83ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e78efa3a-74f9-45a9-b9c4-79bfc48109ff",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "10939f73-e7c7-4e91-b058-f22d24ae7cb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "19f2529b-558c-495b-add8-47c4011bd7d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10939f73-e7c7-4e91-b058-f22d24ae7cb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc53bac0-e6ae-4544-b376-2125454678b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10939f73-e7c7-4e91-b058-f22d24ae7cb1",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "b4288bb1-e28e-443c-b17e-c1a42d8b85fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "85c80a43-5d6c-474e-80d5-5427c3fdb498",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4288bb1-e28e-443c-b17e-c1a42d8b85fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af2f82a3-0e83-4fe1-bfa3-9080d89da1c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4288bb1-e28e-443c-b17e-c1a42d8b85fa",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "b9a9fb56-7598-4f8a-a0bb-10087cc95a02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "cb07c42d-bf04-4809-9fb9-de29e6f26587",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9a9fb56-7598-4f8a-a0bb-10087cc95a02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c41b340-a2fd-4895-afbd-14d8f5e1726e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9a9fb56-7598-4f8a-a0bb-10087cc95a02",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "1dc9adc4-1d27-401c-a627-3e7d1b04fe88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "2e07643f-4ee7-48a5-9674-9747eb2c1854",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dc9adc4-1d27-401c-a627-3e7d1b04fe88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6fdf5bf-6ccf-483a-a7e1-3564345f5e21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dc9adc4-1d27-401c-a627-3e7d1b04fe88",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "3493b97a-a1ad-492b-b106-b86dd9fb43ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "04e59693-9a2b-4318-8670-3b777cc56255",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3493b97a-a1ad-492b-b106-b86dd9fb43ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19bea703-62ed-451d-a8af-1cc7f9c7cc64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3493b97a-a1ad-492b-b106-b86dd9fb43ac",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "3f0f2528-0164-4473-901c-1a6f2ef9b8d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "a01d67fb-b1be-4205-95c1-aff15b40d02a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f0f2528-0164-4473-901c-1a6f2ef9b8d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f17d4ec-b4d6-49ac-882d-16965593b7ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f0f2528-0164-4473-901c-1a6f2ef9b8d6",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "baf021e6-7474-4422-8973-337948372d60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "d1a077ea-ea08-4150-95b1-75b0ddd11934",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baf021e6-7474-4422-8973-337948372d60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f90522d4-eea2-4ce5-89f5-75fb5f896d40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baf021e6-7474-4422-8973-337948372d60",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "1da76376-f948-4cd5-9120-1a14d5881559",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "fad4abc5-620d-432a-af0e-ff555f936435",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1da76376-f948-4cd5-9120-1a14d5881559",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1317d795-cc59-488e-bfa0-4bb9d203c7ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1da76376-f948-4cd5-9120-1a14d5881559",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "c3cba415-3fc4-40de-a073-479763dfad06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "956aaf57-7907-4bac-8e0e-aac16dfeae74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3cba415-3fc4-40de-a073-479763dfad06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d3af3f4-d7b6-4fbc-b3d2-2ed33e00ddb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3cba415-3fc4-40de-a073-479763dfad06",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "e11c358a-d26b-46ed-9793-505936a230ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "4fa6501a-eab9-4fa5-90a1-384f307ef378",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e11c358a-d26b-46ed-9793-505936a230ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "227a48d5-4e79-440a-8367-6609cfbe14a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e11c358a-d26b-46ed-9793-505936a230ca",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "2831b8a1-c54b-4bf7-b957-687047eea924",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "5568ef46-84e4-4649-9319-bf316a30e6d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2831b8a1-c54b-4bf7-b957-687047eea924",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c92c1187-8860-4c96-82c0-edb0e346de47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2831b8a1-c54b-4bf7-b957-687047eea924",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "f1052754-0ae0-4971-a28c-a5bdd9309ecf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "35280d80-6ccb-4333-b881-15772cd34ce2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1052754-0ae0-4971-a28c-a5bdd9309ecf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa174313-ba26-43f3-92a0-8330bef08186",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1052754-0ae0-4971-a28c-a5bdd9309ecf",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "7b47df32-2e1d-4174-8afd-bee336672ce9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "be086fc4-139b-46a3-800c-5706e59bd041",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b47df32-2e1d-4174-8afd-bee336672ce9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf24a574-bdea-4cd0-a076-b7a54c2cc4dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b47df32-2e1d-4174-8afd-bee336672ce9",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "2ec1b47c-0a9d-4b0d-889e-877a4edda8b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "4a6e7fce-577f-48f0-8c82-9e2f00d50f26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ec1b47c-0a9d-4b0d-889e-877a4edda8b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1789cd6d-d743-48e5-abb9-add8421ca216",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ec1b47c-0a9d-4b0d-889e-877a4edda8b9",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "4760a370-91d2-4cc2-be95-f18d375a1e0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "97af4658-c888-41ee-9d4f-4e53f005ec9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4760a370-91d2-4cc2-be95-f18d375a1e0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b99297b-80a7-4ce9-99f5-0e2ac1df1feb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4760a370-91d2-4cc2-be95-f18d375a1e0d",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "dfbc74ef-76a0-47af-880c-315275e419c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "e99e3fbb-eb54-45d1-9ff1-8ec77a0823e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfbc74ef-76a0-47af-880c-315275e419c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e492685-e416-49c6-b83f-3bdc9b8ba7a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfbc74ef-76a0-47af-880c-315275e419c9",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        },
        {
            "id": "eb41116d-4a3e-4650-884c-7bbf68eb8ed1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "compositeImage": {
                "id": "51f6b5be-4058-4e93-95ba-00c34ace4cbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb41116d-4a3e-4650-884c-7bbf68eb8ed1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69e2d88f-eb91-464d-9bf7-cf84d15bc789",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb41116d-4a3e-4650-884c-7bbf68eb8ed1",
                    "LayerId": "45b176cc-d7c1-48ee-8134-083b81313ce8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "45b176cc-d7c1-48ee-8134-083b81313ce8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e186e7ea-d8c5-44a8-82a0-13e64fc38f6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 5
}