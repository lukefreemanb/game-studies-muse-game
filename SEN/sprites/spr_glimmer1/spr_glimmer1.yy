{
    "id": "63c38e72-a9a8-49f4-b266-eff01c1c4831",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_glimmer1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 5,
    "bbox_right": 9,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b08097c6-9f97-4acf-8092-dd24e4bf3bd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63c38e72-a9a8-49f4-b266-eff01c1c4831",
            "compositeImage": {
                "id": "ab36bb50-0c4b-4fc1-86b5-11b40b40c1da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b08097c6-9f97-4acf-8092-dd24e4bf3bd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba2aaddf-7186-43c7-b52e-48caa02cd411",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b08097c6-9f97-4acf-8092-dd24e4bf3bd7",
                    "LayerId": "ea0c2278-f934-4010-beb7-3e2f57f33509"
                }
            ]
        },
        {
            "id": "9c4a0c60-8a41-46e1-9153-8f153779a0bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63c38e72-a9a8-49f4-b266-eff01c1c4831",
            "compositeImage": {
                "id": "a016a0bf-71d1-4e2d-9954-68c45f86fa83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c4a0c60-8a41-46e1-9153-8f153779a0bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "238bb5f7-6f30-4072-af69-df64769b7a36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c4a0c60-8a41-46e1-9153-8f153779a0bf",
                    "LayerId": "ea0c2278-f934-4010-beb7-3e2f57f33509"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ea0c2278-f934-4010-beb7-3e2f57f33509",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63c38e72-a9a8-49f4-b266-eff01c1c4831",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}