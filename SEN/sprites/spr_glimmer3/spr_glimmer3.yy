{
    "id": "2a2bbeda-8628-4058-8e7c-48c5c41d2507",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_glimmer3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 4,
    "bbox_right": 12,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53f477de-b970-48e6-b539-5c650365f183",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a2bbeda-8628-4058-8e7c-48c5c41d2507",
            "compositeImage": {
                "id": "734a1682-604c-4678-9651-efc52491ccf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53f477de-b970-48e6-b539-5c650365f183",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5807daa-869a-40be-b4eb-b84537e8baf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53f477de-b970-48e6-b539-5c650365f183",
                    "LayerId": "54a14420-ab28-4210-8df3-69150283093a"
                }
            ]
        },
        {
            "id": "e67fea67-7417-48a3-9dcc-20af25d3cf49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a2bbeda-8628-4058-8e7c-48c5c41d2507",
            "compositeImage": {
                "id": "26c22768-8d1c-4ca2-be6d-036ecb6948d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e67fea67-7417-48a3-9dcc-20af25d3cf49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5085cae4-c514-4d25-b9cf-88cb5e237c17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e67fea67-7417-48a3-9dcc-20af25d3cf49",
                    "LayerId": "54a14420-ab28-4210-8df3-69150283093a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "54a14420-ab28-4210-8df3-69150283093a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a2bbeda-8628-4058-8e7c-48c5c41d2507",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}