{
    "id": "dd84a586-1991-4e89-a300-72fea8a666b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e5145f6-d229-45b3-8c73-d6726498f22c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd84a586-1991-4e89-a300-72fea8a666b0",
            "compositeImage": {
                "id": "51a94efc-cdda-4450-b90b-6cc210a55b84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e5145f6-d229-45b3-8c73-d6726498f22c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e98b2ca0-3a08-4975-91c5-b9f86b576e82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e5145f6-d229-45b3-8c73-d6726498f22c",
                    "LayerId": "300d044b-6a18-4414-ba17-b16f09dc9d9f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "300d044b-6a18-4414-ba17-b16f09dc9d9f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd84a586-1991-4e89-a300-72fea8a666b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}