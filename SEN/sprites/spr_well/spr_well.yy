{
    "id": "d2edeccc-fae0-47e3-b624-6653a5b5b3fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_well",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 8,
    "bbox_right": 48,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b17e1d3-a844-4c7b-a756-33a425c36078",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2edeccc-fae0-47e3-b624-6653a5b5b3fa",
            "compositeImage": {
                "id": "6804fb94-67f7-46cc-941f-8923d5e830b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b17e1d3-a844-4c7b-a756-33a425c36078",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89b6463c-ff3b-48c3-9a2e-66b67e18b02d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b17e1d3-a844-4c7b-a756-33a425c36078",
                    "LayerId": "99415e94-1010-4f45-abaa-19954728c034"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "99415e94-1010-4f45-abaa-19954728c034",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d2edeccc-fae0-47e3-b624-6653a5b5b3fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 63
}