{
    "id": "535f937e-1e6d-453f-a659-d9f84c70f329",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_puzzle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0ba36f8-e128-47af-a0f7-15cab1e19401",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535f937e-1e6d-453f-a659-d9f84c70f329",
            "compositeImage": {
                "id": "8a751d8f-1c7d-42fa-a71e-69332090136e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0ba36f8-e128-47af-a0f7-15cab1e19401",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c5dfa0a-7130-46fb-8250-0878fa953107",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0ba36f8-e128-47af-a0f7-15cab1e19401",
                    "LayerId": "75dc4c9b-875c-41ff-8dee-9d7d83f3876d"
                }
            ]
        }
    ],
    "gridX": 2,
    "gridY": 2,
    "height": 32,
    "layers": [
        {
            "id": "75dc4c9b-875c-41ff-8dee-9d7d83f3876d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "535f937e-1e6d-453f-a659-d9f84c70f329",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}