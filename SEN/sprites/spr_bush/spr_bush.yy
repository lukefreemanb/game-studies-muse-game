{
    "id": "4f8b9712-81da-4bc6-b6d5-a687129a3e7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bush",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 8,
    "bbox_right": 32,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae19afe5-25b3-4c6e-894d-849a877b1142",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f8b9712-81da-4bc6-b6d5-a687129a3e7c",
            "compositeImage": {
                "id": "5156d5f6-626c-4228-88f6-0c97b2b8993f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae19afe5-25b3-4c6e-894d-849a877b1142",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0728ffe8-21ea-4e5a-842b-ca22e64abef1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae19afe5-25b3-4c6e-894d-849a877b1142",
                    "LayerId": "df6ca84f-f322-404b-bef7-dcc7cfed3760"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "df6ca84f-f322-404b-bef7-dcc7cfed3760",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f8b9712-81da-4bc6-b6d5-a687129a3e7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 39
}