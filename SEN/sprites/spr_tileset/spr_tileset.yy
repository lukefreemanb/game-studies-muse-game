{
    "id": "b4aa4fc1-27ba-4f25-a6f7-5ddbd3159173",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c8d9352-0ce5-4517-8a60-9170cc0b2365",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4aa4fc1-27ba-4f25-a6f7-5ddbd3159173",
            "compositeImage": {
                "id": "48bd4e6f-131b-4a00-8e70-21c1e3d935b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c8d9352-0ce5-4517-8a60-9170cc0b2365",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0b1a75d-6471-4028-a756-d4730c8c4525",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c8d9352-0ce5-4517-8a60-9170cc0b2365",
                    "LayerId": "68781bff-bb69-435f-a6db-58fca2efd2a1"
                }
            ]
        }
    ],
    "gridX": 16,
    "gridY": 16,
    "height": 128,
    "layers": [
        {
            "id": "68781bff-bb69-435f-a6db-58fca2efd2a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4aa4fc1-27ba-4f25-a6f7-5ddbd3159173",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}