{
    "id": "cd2b8460-3127-4634-9f39-a40e6cd46d44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wisp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f488ea4f-36da-4154-83a8-85c0e08c731d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd2b8460-3127-4634-9f39-a40e6cd46d44",
            "compositeImage": {
                "id": "9adc5c23-88fd-4017-88fe-f1d5fcb00fe1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f488ea4f-36da-4154-83a8-85c0e08c731d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaed5c74-7c3f-48a4-ae57-844cb1477682",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f488ea4f-36da-4154-83a8-85c0e08c731d",
                    "LayerId": "86bce9d3-207d-465f-a078-89d508b03343"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "86bce9d3-207d-465f-a078-89d508b03343",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd2b8460-3127-4634-9f39-a40e6cd46d44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}