{
    "id": "ae4d1738-658b-407e-83c0-0c33b84f124c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_overworld_menu_bar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "20f28c3d-183d-485c-b55f-167a006a9be9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae4d1738-658b-407e-83c0-0c33b84f124c",
            "compositeImage": {
                "id": "fff0803f-5806-4009-b9ab-b0c27ab10085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20f28c3d-183d-485c-b55f-167a006a9be9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36fe711b-d336-461b-808d-b2eec20ea2db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20f28c3d-183d-485c-b55f-167a006a9be9",
                    "LayerId": "d13011fd-924a-41ff-b504-ddba948ad19f"
                }
            ]
        },
        {
            "id": "d3c0a126-e5d4-48ab-be06-cbcc82443f43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae4d1738-658b-407e-83c0-0c33b84f124c",
            "compositeImage": {
                "id": "2ccdf72c-943f-4fe3-bdcf-325c61f74eac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3c0a126-e5d4-48ab-be06-cbcc82443f43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63617d44-33eb-453e-8701-382720871a36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3c0a126-e5d4-48ab-be06-cbcc82443f43",
                    "LayerId": "d13011fd-924a-41ff-b504-ddba948ad19f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "d13011fd-924a-41ff-b504-ddba948ad19f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae4d1738-658b-407e-83c0-0c33b84f124c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}