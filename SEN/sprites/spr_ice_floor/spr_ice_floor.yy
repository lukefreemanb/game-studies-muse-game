{
    "id": "4fc46e8c-7918-4589-865e-03e9247a5f03",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ice_floor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28a68ca6-88ae-40f0-9ca0-a558c75df0c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4fc46e8c-7918-4589-865e-03e9247a5f03",
            "compositeImage": {
                "id": "4121c06f-ea91-482d-9b9f-52af8ecf3a7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28a68ca6-88ae-40f0-9ca0-a558c75df0c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23ca3984-131f-4c03-8c9b-27b067aa6613",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28a68ca6-88ae-40f0-9ca0-a558c75df0c7",
                    "LayerId": "880f1544-301b-42ef-b76d-db51df84a0d3"
                },
                {
                    "id": "18448927-0c33-4ced-b8b6-c545d320fc9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28a68ca6-88ae-40f0-9ca0-a558c75df0c7",
                    "LayerId": "6e04a24d-045a-467d-a251-305b953f01d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6e04a24d-045a-467d-a251-305b953f01d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4fc46e8c-7918-4589-865e-03e9247a5f03",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "880f1544-301b-42ef-b76d-db51df84a0d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4fc46e8c-7918-4589-865e-03e9247a5f03",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}