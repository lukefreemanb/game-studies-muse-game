{
    "id": "4834766d-73ba-4649-8b14-3d18fb753129",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 88,
    "bbox_left": 28,
    "bbox_right": 36,
    "bbox_top": 32,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84d041e7-9cee-4955-a591-f13e35c5dacf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4834766d-73ba-4649-8b14-3d18fb753129",
            "compositeImage": {
                "id": "e9347ee3-6181-41e6-93fd-39591b34dea7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84d041e7-9cee-4955-a591-f13e35c5dacf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3874f03f-47aa-44fd-99d8-b28bb9e459b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84d041e7-9cee-4955-a591-f13e35c5dacf",
                    "LayerId": "19f7ad68-d1cf-43e3-8a77-09c642359984"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "19f7ad68-d1cf-43e3-8a77-09c642359984",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4834766d-73ba-4649-8b14-3d18fb753129",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 95
}