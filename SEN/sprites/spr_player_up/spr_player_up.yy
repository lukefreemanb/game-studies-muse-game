{
    "id": "2362a4f3-86d8-428f-8379-2c2624a73817",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 4,
    "bbox_right": 20,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46c2b20e-2135-4f9f-8572-625a3a3ed0ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2362a4f3-86d8-428f-8379-2c2624a73817",
            "compositeImage": {
                "id": "273b38a7-5c4a-4f19-9d2e-b6f31186fb5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46c2b20e-2135-4f9f-8572-625a3a3ed0ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b506269-0f69-43cc-a501-f47e5dd2af6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46c2b20e-2135-4f9f-8572-625a3a3ed0ce",
                    "LayerId": "e6480a37-dc79-43f5-9a9c-8946a3635c30"
                }
            ]
        },
        {
            "id": "1de0452e-dbda-463d-b706-b607d7096f2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2362a4f3-86d8-428f-8379-2c2624a73817",
            "compositeImage": {
                "id": "62310e6c-5241-45e0-80c7-6d05189e3b32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1de0452e-dbda-463d-b706-b607d7096f2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bca076ce-17b7-4c0b-88af-d3b5ea4578e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1de0452e-dbda-463d-b706-b607d7096f2a",
                    "LayerId": "e6480a37-dc79-43f5-9a9c-8946a3635c30"
                }
            ]
        },
        {
            "id": "75ba5b21-1d47-4927-9d9e-c9bf09943fd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2362a4f3-86d8-428f-8379-2c2624a73817",
            "compositeImage": {
                "id": "d286c371-f747-4bb1-8183-904cf5a9175e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75ba5b21-1d47-4927-9d9e-c9bf09943fd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74022232-fd0c-4ad2-91cd-c150e077264a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75ba5b21-1d47-4927-9d9e-c9bf09943fd4",
                    "LayerId": "e6480a37-dc79-43f5-9a9c-8946a3635c30"
                }
            ]
        },
        {
            "id": "eb350ba5-81f5-4c2a-a383-8eddde1728f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2362a4f3-86d8-428f-8379-2c2624a73817",
            "compositeImage": {
                "id": "b9e38baa-4e6a-4319-918e-b7000b92b000",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb350ba5-81f5-4c2a-a383-8eddde1728f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c5cfeb3-7156-400c-aeb1-a451cc7c1970",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb350ba5-81f5-4c2a-a383-8eddde1728f7",
                    "LayerId": "e6480a37-dc79-43f5-9a9c-8946a3635c30"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "e6480a37-dc79-43f5-9a9c-8946a3635c30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2362a4f3-86d8-428f-8379-2c2624a73817",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 39
}