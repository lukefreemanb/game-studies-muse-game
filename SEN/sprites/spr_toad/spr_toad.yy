{
    "id": "54ee379c-01a2-4c8c-a5ee-32a947778cf4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_toad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 9,
    "bbox_right": 31,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "be4082ce-09ea-4f80-8794-0810296883e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54ee379c-01a2-4c8c-a5ee-32a947778cf4",
            "compositeImage": {
                "id": "861a4810-e131-4eb6-9891-7eaee77c4d7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be4082ce-09ea-4f80-8794-0810296883e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16a7cb07-774b-486d-8c6c-bb049bef21f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be4082ce-09ea-4f80-8794-0810296883e8",
                    "LayerId": "bbb71044-64b4-401d-8785-f05220cc606b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "bbb71044-64b4-401d-8785-f05220cc606b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54ee379c-01a2-4c8c-a5ee-32a947778cf4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 39
}