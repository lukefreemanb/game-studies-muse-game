{
    "id": "c74011a5-d8fa-4645-8e53-c9b62db6449d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_corgi_paw_hindright",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1459390e-ea21-45f8-9903-b8ee2c26f1cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c74011a5-d8fa-4645-8e53-c9b62db6449d",
            "compositeImage": {
                "id": "6592c14c-69de-4050-9207-ff2227f3435f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1459390e-ea21-45f8-9903-b8ee2c26f1cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "085ec66e-4b21-41ed-8c17-5c2baacfbab2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1459390e-ea21-45f8-9903-b8ee2c26f1cd",
                    "LayerId": "8a6448bc-cfb1-43ea-85e5-6d3505c5b49e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "8a6448bc-cfb1-43ea-85e5-6d3505c5b49e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c74011a5-d8fa-4645-8e53-c9b62db6449d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 13
}