{
    "id": "29f29ba6-f833-4c32-a178-167b8a42cab9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_typing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a743c62-68db-4700-8f10-9a17876b8645",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29f29ba6-f833-4c32-a178-167b8a42cab9",
            "compositeImage": {
                "id": "8f443c4f-0323-4aaf-b3c4-ba4673d13714",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a743c62-68db-4700-8f10-9a17876b8645",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33ca1cf4-122c-42fb-b063-778f126d0d84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a743c62-68db-4700-8f10-9a17876b8645",
                    "LayerId": "b03d1031-d837-4bd8-b324-dc994d113244"
                }
            ]
        },
        {
            "id": "a3794e0d-4714-40ea-8b83-b1a9adbc8089",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29f29ba6-f833-4c32-a178-167b8a42cab9",
            "compositeImage": {
                "id": "f8f78f4c-bc57-4c03-9742-3a9b2b452d39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3794e0d-4714-40ea-8b83-b1a9adbc8089",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6084926-a8d9-4670-88c5-be221d533835",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3794e0d-4714-40ea-8b83-b1a9adbc8089",
                    "LayerId": "b03d1031-d837-4bd8-b324-dc994d113244"
                }
            ]
        },
        {
            "id": "8475e7cc-97f8-4131-92fb-84ab312a15f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29f29ba6-f833-4c32-a178-167b8a42cab9",
            "compositeImage": {
                "id": "6ddb0acd-e57a-46d9-b7d4-dd99c26265b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8475e7cc-97f8-4131-92fb-84ab312a15f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37375075-d5dd-4305-818f-7ff695c0fffa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8475e7cc-97f8-4131-92fb-84ab312a15f7",
                    "LayerId": "b03d1031-d837-4bd8-b324-dc994d113244"
                }
            ]
        },
        {
            "id": "147d468f-2dfa-408f-9e46-5b8a236b9432",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29f29ba6-f833-4c32-a178-167b8a42cab9",
            "compositeImage": {
                "id": "4a559ca2-4c68-46b2-93a1-d2109d979d13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "147d468f-2dfa-408f-9e46-5b8a236b9432",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "478d5215-b56b-47aa-b012-496126f8c0f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "147d468f-2dfa-408f-9e46-5b8a236b9432",
                    "LayerId": "b03d1031-d837-4bd8-b324-dc994d113244"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b03d1031-d837-4bd8-b324-dc994d113244",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29f29ba6-f833-4c32-a178-167b8a42cab9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}