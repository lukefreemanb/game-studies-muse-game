{
    "id": "4d78e20d-0f13-4908-a0d7-48e541023aed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tree_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 29,
    "bbox_right": 49,
    "bbox_top": 42,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "68234c3c-f4ef-420e-bc49-d60502fb5ce9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d78e20d-0f13-4908-a0d7-48e541023aed",
            "compositeImage": {
                "id": "3b32d1dd-af6f-4278-9d42-d15ee05c777a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68234c3c-f4ef-420e-bc49-d60502fb5ce9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa5fc708-4c47-4935-aa5e-52177ef7b761",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68234c3c-f4ef-420e-bc49-d60502fb5ce9",
                    "LayerId": "16c6dbd3-7dac-4db3-a330-32795cfb4211"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "16c6dbd3-7dac-4db3-a330-32795cfb4211",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d78e20d-0f13-4908-a0d7-48e541023aed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 79
}