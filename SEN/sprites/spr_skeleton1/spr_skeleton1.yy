{
    "id": "be3b2c01-69b0-46db-b404-987dadacd460",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skeleton1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bfa14e1e-ae99-42d4-9d16-f2d943974726",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be3b2c01-69b0-46db-b404-987dadacd460",
            "compositeImage": {
                "id": "c1a6e174-4201-4327-87b0-32b0fe7af83f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfa14e1e-ae99-42d4-9d16-f2d943974726",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7711903a-0ede-4ded-a7fc-651f59a1dcaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfa14e1e-ae99-42d4-9d16-f2d943974726",
                    "LayerId": "fb48220c-8345-4180-b565-19ab423d46d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fb48220c-8345-4180-b565-19ab423d46d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be3b2c01-69b0-46db-b404-987dadacd460",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}