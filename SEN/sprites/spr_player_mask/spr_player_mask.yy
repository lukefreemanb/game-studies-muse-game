{
    "id": "08b56bae-b00d-4a2b-bf76-77c35f4e6f50",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 4,
    "bbox_right": 18,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "736e21d7-87b4-46ff-a5ee-d7be7dc126f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08b56bae-b00d-4a2b-bf76-77c35f4e6f50",
            "compositeImage": {
                "id": "cc7c104d-d88f-44ef-87bf-dabf3da983e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "736e21d7-87b4-46ff-a5ee-d7be7dc126f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3f90d04-1e5a-4c09-b95b-5182e6041aec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "736e21d7-87b4-46ff-a5ee-d7be7dc126f6",
                    "LayerId": "502bd252-a034-401a-a49c-d02fe4f339ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "502bd252-a034-401a-a49c-d02fe4f339ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08b56bae-b00d-4a2b-bf76-77c35f4e6f50",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 39
}