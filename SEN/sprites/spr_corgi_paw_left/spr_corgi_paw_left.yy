{
    "id": "3ea0655c-7893-4367-a02c-88da67890a3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_corgi_paw_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84689959-2646-4e52-b5aa-8edb08dd10ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ea0655c-7893-4367-a02c-88da67890a3e",
            "compositeImage": {
                "id": "c07abda9-6c6a-46eb-9025-a2056f52d4ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84689959-2646-4e52-b5aa-8edb08dd10ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d20ef01-fe36-44f2-85a4-2177a1cd0aab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84689959-2646-4e52-b5aa-8edb08dd10ec",
                    "LayerId": "82f2decf-764f-4e2d-9dc6-ebd05a4422ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "82f2decf-764f-4e2d-9dc6-ebd05a4422ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ea0655c-7893-4367-a02c-88da67890a3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 0,
    "yorig": 12
}