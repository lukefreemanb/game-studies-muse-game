{
    "id": "2c66c96a-e7d4-4e24-8463-15201ccd4f45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ice_slide",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a2fb9c3-4cee-4c07-b996-b83c6568875a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c66c96a-e7d4-4e24-8463-15201ccd4f45",
            "compositeImage": {
                "id": "e2f48e7f-2155-42bd-821f-908aa358ebfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a2fb9c3-4cee-4c07-b996-b83c6568875a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e65bc8a-d18f-4b39-b06a-14cf4711fdc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a2fb9c3-4cee-4c07-b996-b83c6568875a",
                    "LayerId": "dcba8115-23e2-4e6b-8100-c5a831a873d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "dcba8115-23e2-4e6b-8100-c5a831a873d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c66c96a-e7d4-4e24-8463-15201ccd4f45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}