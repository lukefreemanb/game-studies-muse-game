{
    "id": "260feaea-790c-4b80-8bf5-04f19f596e0c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_house",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 121,
    "bbox_left": 22,
    "bbox_right": 96,
    "bbox_top": 60,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8265d301-91a9-4db9-a2ad-311324b6c496",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "260feaea-790c-4b80-8bf5-04f19f596e0c",
            "compositeImage": {
                "id": "e816a24e-1d6b-43e7-915b-3ba010006c90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8265d301-91a9-4db9-a2ad-311324b6c496",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6058fb5-0776-4395-b2ae-f7ac03c65f5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8265d301-91a9-4db9-a2ad-311324b6c496",
                    "LayerId": "9bcb2e39-7f22-451a-b424-a84a0543cb51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 122,
    "layers": [
        {
            "id": "9bcb2e39-7f22-451a-b424-a84a0543cb51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "260feaea-790c-4b80-8bf5-04f19f596e0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 123,
    "xorig": 61,
    "yorig": 121
}