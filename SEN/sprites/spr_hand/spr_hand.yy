{
    "id": "10cd0000-0c5a-4ee1-8054-65d8e151b4b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 6,
    "bbox_right": 27,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56f8b665-e6a1-4043-8dc6-3d006a6acc3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10cd0000-0c5a-4ee1-8054-65d8e151b4b9",
            "compositeImage": {
                "id": "a4735989-8f83-4501-9249-f59d42dbada1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56f8b665-e6a1-4043-8dc6-3d006a6acc3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fa482e7-9ddf-4c3a-8de1-8a52eb1827f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56f8b665-e6a1-4043-8dc6-3d006a6acc3a",
                    "LayerId": "7c355cb1-25de-4024-8fc6-2049575d78b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "7c355cb1-25de-4024-8fc6-2049575d78b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10cd0000-0c5a-4ee1-8054-65d8e151b4b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 37,
    "xorig": 18,
    "yorig": 26
}