{
    "id": "10fe6c54-82a7-49bb-8e41-67c684251def",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 3,
    "bbox_right": 19,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f3b0c06-4f1a-46f8-90e1-7a6e3aa7e5db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10fe6c54-82a7-49bb-8e41-67c684251def",
            "compositeImage": {
                "id": "012f5773-5008-4861-aa3f-c331811df1ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f3b0c06-4f1a-46f8-90e1-7a6e3aa7e5db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8da57ee1-6d80-46d4-8264-6b594cc395db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f3b0c06-4f1a-46f8-90e1-7a6e3aa7e5db",
                    "LayerId": "4b231b2b-0b76-4fb7-8781-d75e7c9df610"
                }
            ]
        },
        {
            "id": "a5f0b5b2-75cb-4a90-a2f7-4bfd20f3fb1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10fe6c54-82a7-49bb-8e41-67c684251def",
            "compositeImage": {
                "id": "71af05a7-af75-456e-96b9-161b5deb8f79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5f0b5b2-75cb-4a90-a2f7-4bfd20f3fb1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab9701b0-a213-49d9-8091-c693a2501219",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5f0b5b2-75cb-4a90-a2f7-4bfd20f3fb1d",
                    "LayerId": "4b231b2b-0b76-4fb7-8781-d75e7c9df610"
                }
            ]
        },
        {
            "id": "a7a4f5fd-b033-4433-824a-824305f51836",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10fe6c54-82a7-49bb-8e41-67c684251def",
            "compositeImage": {
                "id": "19b93e80-5f2c-49fd-b2c9-0b7da6bac39e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7a4f5fd-b033-4433-824a-824305f51836",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43a1674c-d0db-4978-9053-d33791ef75c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7a4f5fd-b033-4433-824a-824305f51836",
                    "LayerId": "4b231b2b-0b76-4fb7-8781-d75e7c9df610"
                }
            ]
        },
        {
            "id": "8e33b6b9-9db1-4e53-83b0-c5b0ac95ad67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10fe6c54-82a7-49bb-8e41-67c684251def",
            "compositeImage": {
                "id": "c47266b2-6166-42e6-bbcd-53e71dd34f11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e33b6b9-9db1-4e53-83b0-c5b0ac95ad67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0217a993-66e9-4062-9ed5-b76cb3745f41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e33b6b9-9db1-4e53-83b0-c5b0ac95ad67",
                    "LayerId": "4b231b2b-0b76-4fb7-8781-d75e7c9df610"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "4b231b2b-0b76-4fb7-8781-d75e7c9df610",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10fe6c54-82a7-49bb-8e41-67c684251def",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 39
}