{
    "id": "56664389-7aeb-4a09-accf-5ac9007a1a3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 5,
    "bbox_right": 19,
    "bbox_top": 33,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed69ffe2-f8e1-417f-a459-2d82ebd7d2c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56664389-7aeb-4a09-accf-5ac9007a1a3b",
            "compositeImage": {
                "id": "36cee006-f5c6-42ad-9ce5-ed0e64a002f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed69ffe2-f8e1-417f-a459-2d82ebd7d2c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fa66dbd-3f83-4d0c-8fe1-ae091b33c383",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed69ffe2-f8e1-417f-a459-2d82ebd7d2c1",
                    "LayerId": "3055e113-7283-4154-b6a8-a74b0a22d12d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "3055e113-7283-4154-b6a8-a74b0a22d12d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56664389-7aeb-4a09-accf-5ac9007a1a3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 39
}