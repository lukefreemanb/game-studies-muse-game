{
    "id": "5ee8dded-33b5-4dcb-8e17-3c50ae434f72",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 18,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "39e303d0-9477-431b-949f-16a6ce523ab2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ee8dded-33b5-4dcb-8e17-3c50ae434f72",
            "compositeImage": {
                "id": "6a613da1-ac3a-4c5f-a68a-095d6e7f4017",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39e303d0-9477-431b-949f-16a6ce523ab2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ac5a813-77c9-4c49-b0d2-b71821cbf33e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39e303d0-9477-431b-949f-16a6ce523ab2",
                    "LayerId": "141cd4de-437a-4a98-885d-2b4fd5ff7c57"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "141cd4de-437a-4a98-885d-2b4fd5ff7c57",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ee8dded-33b5-4dcb-8e17-3c50ae434f72",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 4
}