{
    "id": "beb345b1-8608-4737-9316-7d15d9a9a60c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wallCrack1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11214131-9020-4b0a-a6d0-2181d4e1f32b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beb345b1-8608-4737-9316-7d15d9a9a60c",
            "compositeImage": {
                "id": "38367301-f5a0-4dc5-b8b3-355ad2f3882f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11214131-9020-4b0a-a6d0-2181d4e1f32b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37275975-13ad-4db1-b155-19ff22f9dd73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11214131-9020-4b0a-a6d0-2181d4e1f32b",
                    "LayerId": "40712765-df05-4ab9-b472-55c27d32e82e"
                }
            ]
        },
        {
            "id": "b3d5a89e-710e-4e0f-ad90-cb327c6eec1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beb345b1-8608-4737-9316-7d15d9a9a60c",
            "compositeImage": {
                "id": "d602469c-6dd5-402e-88d2-a96d6764c019",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3d5a89e-710e-4e0f-ad90-cb327c6eec1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bbb03b5-a187-4eb8-85f4-0022e0efcf6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3d5a89e-710e-4e0f-ad90-cb327c6eec1f",
                    "LayerId": "40712765-df05-4ab9-b472-55c27d32e82e"
                }
            ]
        },
        {
            "id": "d7c04cbd-bd11-490c-953b-104d5660b60b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beb345b1-8608-4737-9316-7d15d9a9a60c",
            "compositeImage": {
                "id": "4ddac539-02c2-41c0-9965-b6e85b897e61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7c04cbd-bd11-490c-953b-104d5660b60b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6e605b1-6fd9-4278-aeb9-b623037e4884",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7c04cbd-bd11-490c-953b-104d5660b60b",
                    "LayerId": "40712765-df05-4ab9-b472-55c27d32e82e"
                }
            ]
        },
        {
            "id": "69b62ce1-30c0-4769-8051-4fa3dc02951b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beb345b1-8608-4737-9316-7d15d9a9a60c",
            "compositeImage": {
                "id": "35cbb3ad-2e08-4911-a673-b44bb9c6679b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69b62ce1-30c0-4769-8051-4fa3dc02951b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54a20fbf-6c1a-4061-81f6-faba06bc28d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69b62ce1-30c0-4769-8051-4fa3dc02951b",
                    "LayerId": "40712765-df05-4ab9-b472-55c27d32e82e"
                }
            ]
        },
        {
            "id": "9b2493c2-d2be-45de-809b-f7ae2a4e367b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beb345b1-8608-4737-9316-7d15d9a9a60c",
            "compositeImage": {
                "id": "eefa5295-0e39-407c-abd0-44e6bf04be48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b2493c2-d2be-45de-809b-f7ae2a4e367b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0371a98b-2ca1-4320-8d82-e295b2e19a2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b2493c2-d2be-45de-809b-f7ae2a4e367b",
                    "LayerId": "40712765-df05-4ab9-b472-55c27d32e82e"
                }
            ]
        },
        {
            "id": "18cf2baa-bdef-4668-ad3f-5d236680ce96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beb345b1-8608-4737-9316-7d15d9a9a60c",
            "compositeImage": {
                "id": "8c4eca04-a56f-474d-8980-e514e022765e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18cf2baa-bdef-4668-ad3f-5d236680ce96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9b7e2c6-3cab-4c02-a507-9a37a291fae5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18cf2baa-bdef-4668-ad3f-5d236680ce96",
                    "LayerId": "40712765-df05-4ab9-b472-55c27d32e82e"
                }
            ]
        },
        {
            "id": "65e7296a-a0f1-4ea7-82f9-b40f095410b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beb345b1-8608-4737-9316-7d15d9a9a60c",
            "compositeImage": {
                "id": "90b775b6-dfbb-445a-b7ca-ac08a02dfd5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65e7296a-a0f1-4ea7-82f9-b40f095410b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ccfb7df-b334-46c3-b2e1-cdabac9fcb68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65e7296a-a0f1-4ea7-82f9-b40f095410b6",
                    "LayerId": "40712765-df05-4ab9-b472-55c27d32e82e"
                }
            ]
        },
        {
            "id": "6e93ba96-7121-45fc-8404-590a96734bad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beb345b1-8608-4737-9316-7d15d9a9a60c",
            "compositeImage": {
                "id": "b5d6d210-4b5f-44e6-a47f-7746fc10a23b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e93ba96-7121-45fc-8404-590a96734bad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27eb07dc-f1d2-4c79-a7a1-826336e6ec2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e93ba96-7121-45fc-8404-590a96734bad",
                    "LayerId": "40712765-df05-4ab9-b472-55c27d32e82e"
                }
            ]
        },
        {
            "id": "3ed7d0d3-ed15-4fc7-8d73-e2b87eeebdc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beb345b1-8608-4737-9316-7d15d9a9a60c",
            "compositeImage": {
                "id": "74775404-94e4-48b0-a2fc-99849a9c8cd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ed7d0d3-ed15-4fc7-8d73-e2b87eeebdc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11ad2db1-5f66-45c0-96be-3cc7b9f8a60d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ed7d0d3-ed15-4fc7-8d73-e2b87eeebdc6",
                    "LayerId": "40712765-df05-4ab9-b472-55c27d32e82e"
                }
            ]
        },
        {
            "id": "1c009307-3831-47d4-8192-a01f362de4f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beb345b1-8608-4737-9316-7d15d9a9a60c",
            "compositeImage": {
                "id": "52f11799-b9d8-4ecb-adad-451a8ed52221",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c009307-3831-47d4-8192-a01f362de4f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddc624ea-e77a-486e-ad15-c9a4c158aab9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c009307-3831-47d4-8192-a01f362de4f2",
                    "LayerId": "40712765-df05-4ab9-b472-55c27d32e82e"
                }
            ]
        },
        {
            "id": "4ce830b2-7fd2-48dd-aae9-a576366cdf83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beb345b1-8608-4737-9316-7d15d9a9a60c",
            "compositeImage": {
                "id": "bb39ea59-923e-4afe-8a0c-ed8ac85bd9bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ce830b2-7fd2-48dd-aae9-a576366cdf83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3495b235-a896-4869-b988-a7296508ce07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ce830b2-7fd2-48dd-aae9-a576366cdf83",
                    "LayerId": "40712765-df05-4ab9-b472-55c27d32e82e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "40712765-df05-4ab9-b472-55c27d32e82e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "beb345b1-8608-4737-9316-7d15d9a9a60c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}