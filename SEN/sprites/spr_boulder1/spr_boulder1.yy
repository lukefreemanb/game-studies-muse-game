{
    "id": "6bb75211-5d95-45b3-9199-3b8732fb3cd7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boulder1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b783d923-ad57-4f89-b40b-ad4340712fc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bb75211-5d95-45b3-9199-3b8732fb3cd7",
            "compositeImage": {
                "id": "bb2c6baf-74de-4581-a9f1-17a8d2a5c14b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b783d923-ad57-4f89-b40b-ad4340712fc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebac217b-0724-4661-a694-b63446e6f6b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b783d923-ad57-4f89-b40b-ad4340712fc5",
                    "LayerId": "1c58ac41-4ded-4c30-be1b-ede447ec6c1d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1c58ac41-4ded-4c30-be1b-ede447ec6c1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6bb75211-5d95-45b3-9199-3b8732fb3cd7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 15
}