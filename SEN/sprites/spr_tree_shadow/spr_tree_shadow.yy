{
    "id": "c118eb44-5504-4635-b708-cbc14ee6809a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tree_shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1748cfc-4ca6-4931-a435-30a32a470324",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c118eb44-5504-4635-b708-cbc14ee6809a",
            "compositeImage": {
                "id": "5448da09-5693-46cf-990a-9c8419fdbc74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1748cfc-4ca6-4931-a435-30a32a470324",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d0309fe-9fe0-4c05-acda-b818bc4bd041",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1748cfc-4ca6-4931-a435-30a32a470324",
                    "LayerId": "f9b99f7b-d616-4eb2-90cc-df90ffdc5e73"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f9b99f7b-d616-4eb2-90cc-df90ffdc5e73",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c118eb44-5504-4635-b708-cbc14ee6809a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 8
}