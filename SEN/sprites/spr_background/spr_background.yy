{
    "id": "289a2838-40dc-482f-98d3-a5c47396af53",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 999,
    "bbox_left": 0,
    "bbox_right": 999,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c2788cde-ba92-4ab0-bdac-fedf1855f661",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "289a2838-40dc-482f-98d3-a5c47396af53",
            "compositeImage": {
                "id": "50f06a1a-a6d4-4f5e-bc5f-8bc823b79078",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2788cde-ba92-4ab0-bdac-fedf1855f661",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbe78b16-acf1-493e-8377-34ed76f89a58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2788cde-ba92-4ab0-bdac-fedf1855f661",
                    "LayerId": "b51d3aa0-f82c-48c5-90dc-0e6a7d41dc59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1000,
    "layers": [
        {
            "id": "b51d3aa0-f82c-48c5-90dc-0e6a7d41dc59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "289a2838-40dc-482f-98d3-a5c47396af53",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1000,
    "xorig": 0,
    "yorig": 0
}