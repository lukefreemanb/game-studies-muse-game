{
    "id": "c45831a7-1bd3-4e45-b10b-ec7cd877bf76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pot1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "afd7cdda-be8c-4d57-ad24-5dd9a82dfd3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c45831a7-1bd3-4e45-b10b-ec7cd877bf76",
            "compositeImage": {
                "id": "69f83b4b-4d39-4ef9-9a22-9a4a31622c9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afd7cdda-be8c-4d57-ad24-5dd9a82dfd3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd10d8cb-f879-4d58-afec-3da704c57d89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afd7cdda-be8c-4d57-ad24-5dd9a82dfd3a",
                    "LayerId": "479c3fbc-2f44-4d24-9492-e2c43a691ffe"
                }
            ]
        }
    ],
    "gridX": 1,
    "gridY": 1,
    "height": 32,
    "layers": [
        {
            "id": "479c3fbc-2f44-4d24-9492-e2c43a691ffe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c45831a7-1bd3-4e45-b10b-ec7cd877bf76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}