{
    "id": "042780f4-c802-457f-a0f2-c7aa7d008a49",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_room_transition",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9814560-0cc6-4421-a909-9f1b3d2e23a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "042780f4-c802-457f-a0f2-c7aa7d008a49",
            "compositeImage": {
                "id": "4c301892-4b5c-4bbe-bad8-f201f1d81938",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9814560-0cc6-4421-a909-9f1b3d2e23a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cf06e13-f6fb-4afc-a51f-8bfdc6e73ef0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9814560-0cc6-4421-a909-9f1b3d2e23a0",
                    "LayerId": "79891225-8ff6-448f-83d0-5f086ef00097"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "79891225-8ff6-448f-83d0-5f086ef00097",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "042780f4-c802-457f-a0f2-c7aa7d008a49",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}