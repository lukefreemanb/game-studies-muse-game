{
    "id": "983aa7ab-9162-436b-9eb2-a08ce4b7dfea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_corgi_paw_hindleft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "feeb9a8c-1c24-4e77-a023-bf534e8304f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983aa7ab-9162-436b-9eb2-a08ce4b7dfea",
            "compositeImage": {
                "id": "6e08fb8f-87db-4266-abe8-4cf2500132a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "feeb9a8c-1c24-4e77-a023-bf534e8304f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "294fb95c-db5e-4fb0-b68d-4e1764fe399a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "feeb9a8c-1c24-4e77-a023-bf534e8304f8",
                    "LayerId": "1bb01ac3-2373-4fe2-8bab-80b0916a34c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "1bb01ac3-2373-4fe2-8bab-80b0916a34c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "983aa7ab-9162-436b-9eb2-a08ce4b7dfea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 14
}