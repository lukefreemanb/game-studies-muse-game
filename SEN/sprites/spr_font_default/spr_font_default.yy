{
    "id": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_font_default",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b9ba4c0-7984-4e2a-a389-676596e937dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "28a969bb-dbe4-4f7f-8b21-8f39c8dfa76d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b9ba4c0-7984-4e2a-a389-676596e937dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f67bca3-f50e-4878-b998-1fb0d258c18f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b9ba4c0-7984-4e2a-a389-676596e937dc",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "4df385b9-95e9-4361-9f45-5d624f58184a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "cc5fd6bd-687f-4737-ad15-925e485f6333",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4df385b9-95e9-4361-9f45-5d624f58184a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4c6c19c-622d-4b24-b52d-ce0390321919",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4df385b9-95e9-4361-9f45-5d624f58184a",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "7bc84a7b-941b-4bf6-8db4-d713c8a93a20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "f4c5371e-f84b-4942-b833-8ca7ec84c3a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bc84a7b-941b-4bf6-8db4-d713c8a93a20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d9e2fca-9cbc-4b71-821d-13c2d98de357",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bc84a7b-941b-4bf6-8db4-d713c8a93a20",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "eb8af5e6-8a8a-419e-8db9-625ee0e5eaa1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "8f8ddd81-5aec-4035-afc8-15faf2724595",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb8af5e6-8a8a-419e-8db9-625ee0e5eaa1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c88c302-f22a-4623-a70a-f9b1145dabab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb8af5e6-8a8a-419e-8db9-625ee0e5eaa1",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "1e418390-109b-4b35-8843-57f87d77d6cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "714686ff-f42b-41c0-878d-2ce262ad5cc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e418390-109b-4b35-8843-57f87d77d6cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d4750a3-c6cc-4a2b-9b2d-b631c8355cd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e418390-109b-4b35-8843-57f87d77d6cd",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "f3f0a9a5-2e01-4e7c-a72d-156556aa534d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "36e1a278-b870-46eb-b619-781b187dded6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3f0a9a5-2e01-4e7c-a72d-156556aa534d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "062ec435-a546-41b6-ac51-63f93ac4e912",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3f0a9a5-2e01-4e7c-a72d-156556aa534d",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "e0712be1-637c-4434-8ed5-60e01c6f7b22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "b26c92de-5f03-4526-a18a-dba026194aed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0712be1-637c-4434-8ed5-60e01c6f7b22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40dd558f-dc36-4f9e-89f2-2d1dd773a0c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0712be1-637c-4434-8ed5-60e01c6f7b22",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "aeeba928-c300-4e4c-a534-e65607bcbf1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "509df2ad-89db-4e33-8ef3-3eac9663bf0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aeeba928-c300-4e4c-a534-e65607bcbf1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32311d95-2ba5-44b7-be81-0751ed5cd7af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aeeba928-c300-4e4c-a534-e65607bcbf1b",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "63beb091-4c45-42d5-9692-626024961045",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "9a9011df-c889-4f9d-950f-0cf46f4f36d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63beb091-4c45-42d5-9692-626024961045",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3c257cd-2eef-448c-8cad-b282276faf5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63beb091-4c45-42d5-9692-626024961045",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "a0df54bb-c894-420e-94f8-e461d89fb22e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "92743541-4b06-43f2-8931-a26b83dc14b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0df54bb-c894-420e-94f8-e461d89fb22e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aae56927-0a4c-4c1c-9e2e-824d5bbc1af0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0df54bb-c894-420e-94f8-e461d89fb22e",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "55b934e5-6ebf-463d-b5f8-e2d937ab650a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "1bc29012-455b-4809-a2ec-d654a0c0a267",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55b934e5-6ebf-463d-b5f8-e2d937ab650a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03a813af-26b4-4b40-aa45-2aff4a73f36d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55b934e5-6ebf-463d-b5f8-e2d937ab650a",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "5971e158-4c52-440e-bf29-84aef8ae0834",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "631441cc-ec11-4433-9795-7899053eae23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5971e158-4c52-440e-bf29-84aef8ae0834",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1c11118-1e7c-4171-b0c1-11a55be51e78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5971e158-4c52-440e-bf29-84aef8ae0834",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "a97210d5-dd0d-4bf1-8cb5-7e15e619a22d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "7f579c6b-dd5b-4a89-8dd8-5f2e53e33610",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a97210d5-dd0d-4bf1-8cb5-7e15e619a22d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c08478f-a1c1-43ec-8bab-cf5cb5e64ad1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a97210d5-dd0d-4bf1-8cb5-7e15e619a22d",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "28c78f38-5aef-4cc9-95cc-14d2fa625214",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "1950bbfc-c7a8-486c-8680-248ef414cae9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28c78f38-5aef-4cc9-95cc-14d2fa625214",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b7d29d8-74c4-4f1c-837d-b2b025cdd1a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28c78f38-5aef-4cc9-95cc-14d2fa625214",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "c14c96b5-71c4-4562-bf1d-257db9acc99e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "94700e43-8b35-4e86-9125-9444e95a708c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c14c96b5-71c4-4562-bf1d-257db9acc99e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bad1ff7a-63cc-45bf-b7f9-c9ad80519947",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c14c96b5-71c4-4562-bf1d-257db9acc99e",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "c63ef74c-0225-49dc-a292-8d84c7ed086b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "c540a336-321f-42c5-8dce-b59d99a8cfb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c63ef74c-0225-49dc-a292-8d84c7ed086b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c92d8e92-d6ab-41c2-b184-6155252d46d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c63ef74c-0225-49dc-a292-8d84c7ed086b",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "58c0c965-a453-4a58-a412-0eaef2ddfd67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "8b729d2a-fdbf-4c32-8c97-580643e1f148",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58c0c965-a453-4a58-a412-0eaef2ddfd67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5ebdea6-fd90-465a-bee7-80898458a9ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58c0c965-a453-4a58-a412-0eaef2ddfd67",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "50a9c198-611e-4b5b-a406-a9ae32965e1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "87720c4f-bae8-463f-ba66-b89d4cd3e297",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50a9c198-611e-4b5b-a406-a9ae32965e1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2724e858-75d5-4aa7-b2a7-6ddd5cd2adc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50a9c198-611e-4b5b-a406-a9ae32965e1e",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "0e0a82a3-687c-4447-bb76-af63f89e8965",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "87b1d1c5-deba-40d2-8a56-bc57d0c423ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e0a82a3-687c-4447-bb76-af63f89e8965",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e61275f5-6ac3-4f75-a668-94e679db0a42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e0a82a3-687c-4447-bb76-af63f89e8965",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "c73c8550-6443-4a9d-9d21-242ae10655a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "67631d6c-ded4-46e5-a13b-5c28ef595860",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c73c8550-6443-4a9d-9d21-242ae10655a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "906e2e2d-9500-4bc2-8b82-3b46f2cb4062",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c73c8550-6443-4a9d-9d21-242ae10655a5",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "25b517d5-ff1b-4f2d-8247-829bf0ebf203",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "c0e41fa5-3ba6-4a4d-ae0d-f0b2d4c21dc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25b517d5-ff1b-4f2d-8247-829bf0ebf203",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6f4ab7a-2923-451c-a606-558e316013e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25b517d5-ff1b-4f2d-8247-829bf0ebf203",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "95445d9e-ed7e-44c4-b3f3-33d162876696",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "254b2590-ebaa-4fa0-9207-645efd8d3d07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95445d9e-ed7e-44c4-b3f3-33d162876696",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27a0bddf-add6-4989-b52d-46ac38855711",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95445d9e-ed7e-44c4-b3f3-33d162876696",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "079b2684-8197-4ddf-aeb1-d9b7cb2e00c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "cd154967-e8e1-4e8c-999e-e15709cf57b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "079b2684-8197-4ddf-aeb1-d9b7cb2e00c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87c731a8-919d-4227-92b9-8e180677e154",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "079b2684-8197-4ddf-aeb1-d9b7cb2e00c7",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "1f889b3d-7e46-4954-9c2c-1b26b4645b15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "69da260f-7e69-4408-8f21-57fe54abc516",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f889b3d-7e46-4954-9c2c-1b26b4645b15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a82002e5-410f-4578-a13d-275db48acc19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f889b3d-7e46-4954-9c2c-1b26b4645b15",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "0042ab45-bfe4-45a1-8397-30d2937a0bde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "dfbb82e4-28a6-4f81-aea3-7b7a3165905d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0042ab45-bfe4-45a1-8397-30d2937a0bde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3adafeec-e12b-48b5-95f8-f5b34f8d9e4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0042ab45-bfe4-45a1-8397-30d2937a0bde",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "2d29fd01-9989-4142-8360-9f85f91162d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "3c65c162-be68-4611-81f0-7c1b4f837d9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d29fd01-9989-4142-8360-9f85f91162d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17ed435d-1703-40b7-9c04-4948314cba1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d29fd01-9989-4142-8360-9f85f91162d3",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "c28a45f6-5532-41ab-86d3-5cd4b2c516ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "f3cc0173-ff4b-48b6-9819-16e87463a3d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c28a45f6-5532-41ab-86d3-5cd4b2c516ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ae9eeb6-7882-4aad-bcd7-0d028508e35e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c28a45f6-5532-41ab-86d3-5cd4b2c516ad",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "85fa49da-b8ee-45f1-8c61-af5d8ba69995",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "c4c8461d-f171-442f-b62a-7d6d3f0b8f25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85fa49da-b8ee-45f1-8c61-af5d8ba69995",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8c02241-7501-4349-b120-e66221acd7b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85fa49da-b8ee-45f1-8c61-af5d8ba69995",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "c4190ded-d1a4-4ce2-8ff3-93eb4d1f6cef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "37e8bb28-fb0d-4e62-9ce3-55dff4863b35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4190ded-d1a4-4ce2-8ff3-93eb4d1f6cef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a843b2c3-1f79-479e-a09a-c42485bca7cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4190ded-d1a4-4ce2-8ff3-93eb4d1f6cef",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "e6ac1103-e8cd-4324-b588-e6c9800afe7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "399f1e8a-f077-4485-acfc-fde4b9317082",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6ac1103-e8cd-4324-b588-e6c9800afe7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41534167-29f8-43d3-83e6-685673aa8f10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6ac1103-e8cd-4324-b588-e6c9800afe7c",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "016cc5c4-fc85-44fc-81b9-fbb838161472",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "79bd8eed-fcfb-41b8-953e-29d868d21396",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "016cc5c4-fc85-44fc-81b9-fbb838161472",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55d3315e-08f7-4068-9374-82caaf6c0078",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "016cc5c4-fc85-44fc-81b9-fbb838161472",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "19eb3416-cc58-4026-81bd-1522ebad1777",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "bba5c92c-d57e-41bc-87f3-a4d78341b0b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19eb3416-cc58-4026-81bd-1522ebad1777",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c95da30f-021e-4ea2-a6e6-c4e01ac6a5d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19eb3416-cc58-4026-81bd-1522ebad1777",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "d8125b48-b9dd-4c86-8762-7781908a0a36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "d2da38e1-914f-4deb-a727-5b6405849058",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8125b48-b9dd-4c86-8762-7781908a0a36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69764e8e-6d1f-4b5e-872b-a2887a6f09c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8125b48-b9dd-4c86-8762-7781908a0a36",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "b17bd505-bf88-41f9-8b14-793044e2a184",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "a70bf276-ce62-4775-8a94-c500fb569119",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b17bd505-bf88-41f9-8b14-793044e2a184",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64548d90-1167-4d7b-85cf-c49843f32e70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b17bd505-bf88-41f9-8b14-793044e2a184",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "3782ec34-ad83-48e6-8549-aa7b22bf81f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "ee91ab1f-c11d-4779-9ecb-9496229d94e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3782ec34-ad83-48e6-8549-aa7b22bf81f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5c2517d-7b81-466e-aad6-dac27ce195c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3782ec34-ad83-48e6-8549-aa7b22bf81f9",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "1e8f7fe9-cd95-4b3a-b9c5-14c53a7dfda8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "8ae72048-d66e-41de-aebb-84e4f0d27990",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e8f7fe9-cd95-4b3a-b9c5-14c53a7dfda8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c075912-fbfe-4ad2-8dd0-6de16432f5d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e8f7fe9-cd95-4b3a-b9c5-14c53a7dfda8",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "d26b33ad-9be2-4ffc-8bfe-bafeeca550c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "c8c64885-d4a0-49f3-a102-4741a2030b0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d26b33ad-9be2-4ffc-8bfe-bafeeca550c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e68a73f9-fa14-496e-9b5b-2cd3886658c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d26b33ad-9be2-4ffc-8bfe-bafeeca550c4",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "8a9d6080-3d24-4b98-a955-4e04dcaa9f47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "73d349d2-58b6-45a5-aaf8-0e117cdac839",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a9d6080-3d24-4b98-a955-4e04dcaa9f47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9743e359-d930-4801-870a-81a588257bfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a9d6080-3d24-4b98-a955-4e04dcaa9f47",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "b2f4ce07-5f2f-4328-8d3d-e660fc33e583",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "1b1b7190-acb7-4af6-9d70-97437471bfb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2f4ce07-5f2f-4328-8d3d-e660fc33e583",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1901b1be-2696-42e8-90bd-9ad5588e9f1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2f4ce07-5f2f-4328-8d3d-e660fc33e583",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "dedecd39-96ca-44e6-99a1-36532acf37c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "f9ce73c6-5c38-4abf-b2cb-bfeac7254e86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dedecd39-96ca-44e6-99a1-36532acf37c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e505ba8-ec14-4e2a-9662-0940f8e05553",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dedecd39-96ca-44e6-99a1-36532acf37c1",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "26ed4d33-903a-4f45-a336-3e8abfd52418",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "123e142e-1f09-487e-8aaa-81712b155430",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26ed4d33-903a-4f45-a336-3e8abfd52418",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b617d51d-0ebc-4ee7-93a3-0e19e5f3eac2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26ed4d33-903a-4f45-a336-3e8abfd52418",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "134a2952-9624-4848-aab1-521c04735fbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "6f1d6e93-b21c-401e-9fcb-ea370c64c166",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "134a2952-9624-4848-aab1-521c04735fbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b0fb1b5-ebef-491a-b1ee-d3d4d2f9befb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "134a2952-9624-4848-aab1-521c04735fbd",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "34666975-b726-4639-80d2-f20f832b0322",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "18d0a242-dae9-42e3-a867-3edfb9d257f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34666975-b726-4639-80d2-f20f832b0322",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35a148b5-a3dc-43ff-99b9-d69e39677ffc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34666975-b726-4639-80d2-f20f832b0322",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "9afaa699-18dd-47df-8323-1319fb32636c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "fada0392-ed2e-46fb-b46e-1013699a680d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9afaa699-18dd-47df-8323-1319fb32636c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d60d5d4-7d60-4099-a808-f57e5ae7713f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9afaa699-18dd-47df-8323-1319fb32636c",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "9efd8fa5-693f-47b3-96b2-5ef2174b4425",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "87b7f3a9-3644-458a-8925-407e9ce77561",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9efd8fa5-693f-47b3-96b2-5ef2174b4425",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c303ca2b-54e6-4250-b64b-170391533e21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9efd8fa5-693f-47b3-96b2-5ef2174b4425",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "05c1c871-fa62-476e-975f-54c7e102c442",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "cf8072b4-a00a-4223-9ada-7cf768b6d2a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05c1c871-fa62-476e-975f-54c7e102c442",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6d7628a-b69d-415c-b451-3899d62df32e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05c1c871-fa62-476e-975f-54c7e102c442",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "96ddd22d-7592-4419-841d-5b250594b462",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "a7a9f95e-5326-404b-bbaf-9bd8e06ad201",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96ddd22d-7592-4419-841d-5b250594b462",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c29dee9f-3bcd-491b-af76-7c1d282c44b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96ddd22d-7592-4419-841d-5b250594b462",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "7a7cd91c-73f8-4317-8b2e-11c8f409abe7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "e60dcb3b-2a6f-4e5d-a51b-e766fd7a6a29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a7cd91c-73f8-4317-8b2e-11c8f409abe7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "868493a3-ba43-49e1-bcf9-b6e65baaf707",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a7cd91c-73f8-4317-8b2e-11c8f409abe7",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "3dba47ed-79c8-4a7f-9858-8bc4dd4045cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "d990156c-9a79-45a7-906b-e4dfee5cd371",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dba47ed-79c8-4a7f-9858-8bc4dd4045cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20f21577-13fd-4061-84ec-1a1e4d16a40c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dba47ed-79c8-4a7f-9858-8bc4dd4045cc",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "ee477947-db0c-49dc-8818-bdc9a4c66c63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "1cee5686-9e5f-4789-a534-3e20698d7913",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee477947-db0c-49dc-8818-bdc9a4c66c63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23921c3d-3272-4eb2-a132-556cfbf4d1a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee477947-db0c-49dc-8818-bdc9a4c66c63",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "094eabe6-ef2b-4842-ad9a-609fa1f7cb15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "610e0bd9-94c1-4485-95a1-f3ab496a2bda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "094eabe6-ef2b-4842-ad9a-609fa1f7cb15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e6a7157-2cc2-4270-8740-f76f64c39839",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "094eabe6-ef2b-4842-ad9a-609fa1f7cb15",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "f0bd0862-05cb-4904-a25e-dd1ca6086408",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "b7799a64-b24b-4f8a-b2c4-f631e04dd506",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0bd0862-05cb-4904-a25e-dd1ca6086408",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08edbae7-26bc-4079-88d6-2895d0413249",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0bd0862-05cb-4904-a25e-dd1ca6086408",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "bb067d18-5205-43c2-b88f-0daab030e30d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "b3edbd8e-c4bf-41db-8043-76aabaa6be9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb067d18-5205-43c2-b88f-0daab030e30d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "922c2ace-ef6d-49a4-80e2-e1488635f100",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb067d18-5205-43c2-b88f-0daab030e30d",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "7f18f763-e05c-4b3f-893f-4bda63f06d59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "49a0dd23-70a5-4f7c-93cc-22cb94d2c7cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f18f763-e05c-4b3f-893f-4bda63f06d59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b24076f5-d3ee-4a1e-9a77-a3e9a329253f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f18f763-e05c-4b3f-893f-4bda63f06d59",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "32c89f96-b268-485e-b95f-59ffcea4d442",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "ad167324-69b1-4063-b2a1-39725b8c9add",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32c89f96-b268-485e-b95f-59ffcea4d442",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48f2036d-9c3c-4cac-a7ee-9c4ed1a779ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32c89f96-b268-485e-b95f-59ffcea4d442",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "a6371788-7bb5-43b7-828e-db7ab19cabae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "c31d3d63-9eb8-41d5-9361-63920a7a9575",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6371788-7bb5-43b7-828e-db7ab19cabae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cd06f8e-93f8-459e-841b-115b02f7088c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6371788-7bb5-43b7-828e-db7ab19cabae",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "23aa3611-dac1-485d-bfea-7580a6f05423",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "6d994a08-8852-4c37-bf59-4cd6b80366a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23aa3611-dac1-485d-bfea-7580a6f05423",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73362ba1-50ff-4163-a471-4945b87e9049",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23aa3611-dac1-485d-bfea-7580a6f05423",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "c83c3db6-6845-41d4-b7ac-c4b341ad3741",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "d87ee9de-406e-4cea-bf8c-352e3d24a36c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c83c3db6-6845-41d4-b7ac-c4b341ad3741",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83df27cb-6873-4c25-85b5-a47209baa22f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c83c3db6-6845-41d4-b7ac-c4b341ad3741",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "f3202597-479f-4a1e-9efd-b14416d2eea4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "82b50fdf-bccd-41d3-a908-5aeff25c3351",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3202597-479f-4a1e-9efd-b14416d2eea4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac5273aa-de41-474c-b720-96560b2b9b07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3202597-479f-4a1e-9efd-b14416d2eea4",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "1be40075-0859-4322-a22c-6d2861f46da3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "f8528fa6-3a60-4cfb-a933-3082f4387558",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1be40075-0859-4322-a22c-6d2861f46da3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "070fcd2f-b605-43df-ae13-793d6dcc2f67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1be40075-0859-4322-a22c-6d2861f46da3",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "90161770-1ab1-4091-a023-d1e960030257",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "77fa4d29-4934-470f-9fb8-c07d7f967cfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90161770-1ab1-4091-a023-d1e960030257",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ff77cc2-5803-439c-a714-2bf8931c37b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90161770-1ab1-4091-a023-d1e960030257",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "228eba44-f916-43cd-8589-244526791b43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "794e0269-9d84-4fa8-9266-f36c7f8679ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "228eba44-f916-43cd-8589-244526791b43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79dd09d3-f601-464d-8b19-dda122049d5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "228eba44-f916-43cd-8589-244526791b43",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "530da411-666e-47de-90b1-42c5321fc7ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "07c3709c-d1c7-45b5-a3bd-dc8415fd8942",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "530da411-666e-47de-90b1-42c5321fc7ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f44a0789-68e4-47fd-aa7c-dcae155c9185",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "530da411-666e-47de-90b1-42c5321fc7ee",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "d8567145-ea54-4e37-ac21-ab63e28e192a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "8c4b7f1c-0413-43f0-b267-017848335255",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8567145-ea54-4e37-ac21-ab63e28e192a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe301547-d752-456d-b5ff-bcfdba4a4529",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8567145-ea54-4e37-ac21-ab63e28e192a",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "1bb8a740-ce0b-489e-9528-8163af63a24e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "f7af9920-c10b-485e-a559-e26b4f60a81e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bb8a740-ce0b-489e-9528-8163af63a24e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af03ff4b-e6e0-45d0-8393-aa72e3abd21a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bb8a740-ce0b-489e-9528-8163af63a24e",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "df156b50-066d-4362-8d86-3eba01f03e8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "1d4ea6c3-2284-49e3-bb02-8070d9d42bae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df156b50-066d-4362-8d86-3eba01f03e8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b18ffbb9-eb08-4788-8a2d-10bd386f23c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df156b50-066d-4362-8d86-3eba01f03e8f",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "46ed2ded-fc27-453e-84b3-dcf366bd9642",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "7e5f357b-d348-449c-a9c5-7b39594e5b38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46ed2ded-fc27-453e-84b3-dcf366bd9642",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8af42ba2-25e2-4f41-b65f-20b433b4c4c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46ed2ded-fc27-453e-84b3-dcf366bd9642",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "7ce008ac-bb46-4b54-a7f5-4e88eaf3a2b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "010e2398-be1b-435d-a398-ffb87fa78437",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ce008ac-bb46-4b54-a7f5-4e88eaf3a2b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcd1ea60-20fc-4ebf-a71d-d918b0228c78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ce008ac-bb46-4b54-a7f5-4e88eaf3a2b7",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "7a190611-fee6-4b4f-a57d-c8d81d7f2b86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "46c3804d-67df-4212-bdc6-d45b4c5431af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a190611-fee6-4b4f-a57d-c8d81d7f2b86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55d8a74d-9234-47f6-8b78-2e8229021f42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a190611-fee6-4b4f-a57d-c8d81d7f2b86",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "c34e9744-8e6e-44e4-8b8a-830663cce0ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "49c2ca3f-a8a1-4fc3-ac18-98c24be4ce8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c34e9744-8e6e-44e4-8b8a-830663cce0ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30c325ae-a8bf-4201-ad0f-80a7a3de3980",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c34e9744-8e6e-44e4-8b8a-830663cce0ff",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "d2e26d7a-9874-4216-ad03-ef1368ea6022",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "84377583-1db9-464a-b469-fafc6d9fe37e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2e26d7a-9874-4216-ad03-ef1368ea6022",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f2dbfd2-af0f-46fc-97ce-46d63a7e6f7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2e26d7a-9874-4216-ad03-ef1368ea6022",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "5c4aa0ca-68d5-424a-91b5-acc41c54e5b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "a01d3aed-a437-45db-8033-76abd43597c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c4aa0ca-68d5-424a-91b5-acc41c54e5b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66839d14-1b0b-484a-aa32-ea06dba10719",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c4aa0ca-68d5-424a-91b5-acc41c54e5b7",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "a17c7c49-cacf-4522-8d54-082e6b58c67e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "613e81d8-a035-484d-9d18-337149a9cd40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a17c7c49-cacf-4522-8d54-082e6b58c67e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c92aef3-435b-462b-86a2-4f73817a6fe3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a17c7c49-cacf-4522-8d54-082e6b58c67e",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "0bbacd9f-00b5-4577-b0eb-ef56458df0fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "ec6647fd-3214-4faa-831c-09b31d070e18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bbacd9f-00b5-4577-b0eb-ef56458df0fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9464706-9f7b-472f-b695-643c22a26b10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bbacd9f-00b5-4577-b0eb-ef56458df0fc",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "6dee5ea0-4c12-4062-99cc-fc769e322d6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "0fbbcbfa-820c-4db4-8994-143f0ea0b2ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dee5ea0-4c12-4062-99cc-fc769e322d6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "926e28c3-7164-44c6-a2fb-00d48fe752ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dee5ea0-4c12-4062-99cc-fc769e322d6e",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "2dfc7d2f-b4c9-4ca5-aec8-48e6cd288ea9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "71b8562b-5ff8-49ab-a922-72a251818165",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dfc7d2f-b4c9-4ca5-aec8-48e6cd288ea9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e70914e-ab50-4da9-8072-55dedbb211fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dfc7d2f-b4c9-4ca5-aec8-48e6cd288ea9",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "aa4b6a3f-e5a7-4e37-b4f1-f2320ea17f21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "0c5eaf30-f17f-4a4f-8d33-5227cfb0cb20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa4b6a3f-e5a7-4e37-b4f1-f2320ea17f21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbfbadc6-969b-409c-8bb2-af10a8aae95d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa4b6a3f-e5a7-4e37-b4f1-f2320ea17f21",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "f6b9ce06-e13f-497f-b0de-033cc10c5eda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "f218f60a-6bd2-4d16-be38-9c4b92b2d26d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6b9ce06-e13f-497f-b0de-033cc10c5eda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2e4735c-182a-4bf2-bec9-92e030148e59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6b9ce06-e13f-497f-b0de-033cc10c5eda",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "09996c5a-394a-4673-819c-742814cb3661",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "4adb465e-0a29-465d-8db1-6c3c83e3c239",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09996c5a-394a-4673-819c-742814cb3661",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7bb1a0e-38d3-4172-9cea-2d9245261c8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09996c5a-394a-4673-819c-742814cb3661",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "7a9db31a-79eb-4022-9a56-d93331249551",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "8c94290b-a0fb-41e7-ab5e-64352a61b024",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a9db31a-79eb-4022-9a56-d93331249551",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2acab26-ba82-4e71-b762-7a7f59963abf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a9db31a-79eb-4022-9a56-d93331249551",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "de67e3f5-85db-44b5-b634-c444788a1802",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "c2522e3a-5e10-48c7-a870-5671fa8d1157",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de67e3f5-85db-44b5-b634-c444788a1802",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6b1abde-6bbe-4acc-a202-7412128ef7fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de67e3f5-85db-44b5-b634-c444788a1802",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "a2234185-5b04-4295-912b-d45a4e22ff01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "95828624-0cd6-454e-9d96-df673b13715e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2234185-5b04-4295-912b-d45a4e22ff01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15bf1e7b-6c9b-4250-aebb-0a63a8678bdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2234185-5b04-4295-912b-d45a4e22ff01",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "68d81f10-bd2f-4f37-90b3-a05503720e73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "3484e8a5-15b0-4171-bdbd-a4510db47d68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68d81f10-bd2f-4f37-90b3-a05503720e73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5f47de9-7dbf-459b-9b4e-2be2416d2f44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68d81f10-bd2f-4f37-90b3-a05503720e73",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "e08a851b-56ad-4c29-8aa0-71d1124876f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "ca1315cd-ec73-4690-a826-4ca22f869709",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e08a851b-56ad-4c29-8aa0-71d1124876f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c36a1da-3175-4a3a-99a7-c64aadb0a4bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e08a851b-56ad-4c29-8aa0-71d1124876f5",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "f6bd64c4-cfff-4dc8-8021-0c3a2a991ba3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "e8234d88-5039-4eb8-bdf0-1bf46a7cf4be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6bd64c4-cfff-4dc8-8021-0c3a2a991ba3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ba34bc9-cd52-4b14-a6b0-f1fefb4716ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6bd64c4-cfff-4dc8-8021-0c3a2a991ba3",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "6533a4c2-6b20-4e1b-9882-9a5ea180826d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "21658567-e97f-49d9-94c9-8016cbb952ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6533a4c2-6b20-4e1b-9882-9a5ea180826d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ed4c17e-8c3a-47e5-98dd-03e6b25b856a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6533a4c2-6b20-4e1b-9882-9a5ea180826d",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "88b1fb54-423b-46b7-b217-70a22920bc8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "b4d9272b-dbf2-4051-a19c-2958e7695dd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88b1fb54-423b-46b7-b217-70a22920bc8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80e6ddf5-6d30-4c7b-894d-37439582e396",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88b1fb54-423b-46b7-b217-70a22920bc8c",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "fc91ec53-4b53-48df-9713-954a2bbad090",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "fc89bc36-436e-4e09-acf7-039a2857b68c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc91ec53-4b53-48df-9713-954a2bbad090",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e06f97d-53ff-4c52-93e0-20bd71d61dab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc91ec53-4b53-48df-9713-954a2bbad090",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "ebcdfaf9-4291-42d6-97be-380f990d8e25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "d15a6a24-b940-4874-b8bc-9a955d44389b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebcdfaf9-4291-42d6-97be-380f990d8e25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36b0b476-f6a7-48ac-bed3-254c32a03daa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebcdfaf9-4291-42d6-97be-380f990d8e25",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "bebad38d-1347-4621-b5e7-8acf9c4003a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "4ea3a514-ff65-481b-ab06-31bf602399cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bebad38d-1347-4621-b5e7-8acf9c4003a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0436a70a-e951-4dbb-b767-d5c6f643ba66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bebad38d-1347-4621-b5e7-8acf9c4003a0",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "616da39f-24e6-4a63-bbd4-3d0154263074",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "96c9f3c9-5e40-413a-8ebb-904d77dbd3a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "616da39f-24e6-4a63-bbd4-3d0154263074",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e91adc38-7c31-4aed-8b36-445881e71ce6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "616da39f-24e6-4a63-bbd4-3d0154263074",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "372b84d6-9cd6-4591-b884-240769324142",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "88a02819-4701-4cea-86eb-a2d1ffd3a472",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "372b84d6-9cd6-4591-b884-240769324142",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ef49881-8b37-4121-a307-73398d1bcb5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "372b84d6-9cd6-4591-b884-240769324142",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "ec93997b-e2a7-4ca7-b3c3-f048167f88f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "ecf84667-ee20-4255-b834-6733b9cfb45f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec93997b-e2a7-4ca7-b3c3-f048167f88f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffd996af-c749-4286-956e-2628d8d596d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec93997b-e2a7-4ca7-b3c3-f048167f88f9",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "adff538d-62bf-4f36-9dd8-2b9486433077",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "c920cc8a-b1b2-4855-a50f-cd9ab1812350",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adff538d-62bf-4f36-9dd8-2b9486433077",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc828116-0314-4855-9a6d-16f7449cabf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adff538d-62bf-4f36-9dd8-2b9486433077",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "7634386d-f69c-4707-8480-5024a0df293b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "2c15572b-2c58-4af9-b72a-a6a43454971e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7634386d-f69c-4707-8480-5024a0df293b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e67ab424-4f68-4731-8714-ced1a30b4f4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7634386d-f69c-4707-8480-5024a0df293b",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "99dd4baf-242e-4cf1-b0c7-e7e275e63a5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "da62ee1d-ff8c-404a-97ae-e25cab5591ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99dd4baf-242e-4cf1-b0c7-e7e275e63a5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d28630ae-ffa5-4eda-a206-03b15ae2380e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99dd4baf-242e-4cf1-b0c7-e7e275e63a5c",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "56c51968-2510-47b3-b5a9-b4c62a77b2a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "612a89b3-1c29-455d-8db9-6fc06a5c2c71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56c51968-2510-47b3-b5a9-b4c62a77b2a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "916649d9-b720-4fc8-8413-e7857e7370db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56c51968-2510-47b3-b5a9-b4c62a77b2a2",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "3430cf47-7f3a-46cf-bcd8-54bc25ba744b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "dbd282e9-7e97-4926-b8a0-7d39c075fe64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3430cf47-7f3a-46cf-bcd8-54bc25ba744b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8fad5f5-8155-4bf2-b686-59a724ce041a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3430cf47-7f3a-46cf-bcd8-54bc25ba744b",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "d9e777e1-b2c4-4dad-94e2-29cad33872f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "924174a1-ce19-4b7d-aeb6-496da17f3868",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9e777e1-b2c4-4dad-94e2-29cad33872f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b121273-19f2-486a-9915-9371777e6e67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9e777e1-b2c4-4dad-94e2-29cad33872f7",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "e2e3ff8e-2b1b-41cb-ab68-77ed52ffb045",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "83046fec-32a3-4465-9ed2-24edf69a6dae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2e3ff8e-2b1b-41cb-ab68-77ed52ffb045",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ba61682-6429-4996-b827-1cd41b20768e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2e3ff8e-2b1b-41cb-ab68-77ed52ffb045",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "be39024c-7943-4334-92b2-5b7ff4de61a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "a335464f-1d30-4974-a479-5cc6a4244217",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be39024c-7943-4334-92b2-5b7ff4de61a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5691f436-fdaa-4186-9c47-516b4dd54c19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be39024c-7943-4334-92b2-5b7ff4de61a0",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "4eff0895-0064-47f4-99ca-ce1d6d41f567",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "6743e601-5943-4108-95c6-f4afa6e5116d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4eff0895-0064-47f4-99ca-ce1d6d41f567",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd77e4b6-4ae7-4815-bc6d-de24ffcea1f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4eff0895-0064-47f4-99ca-ce1d6d41f567",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "2bd591a5-a1b5-4a74-a231-d88d5e268795",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "c757deb6-6e38-4e4f-a473-35d155f6e46f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bd591a5-a1b5-4a74-a231-d88d5e268795",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "311b2ea1-3cac-4733-b2cd-c4a508f4f64b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bd591a5-a1b5-4a74-a231-d88d5e268795",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "14eee08c-e66a-4c06-aa8e-d40e80fb8fb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "4fa9dba6-d67b-4431-956e-a4aa8c4289d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14eee08c-e66a-4c06-aa8e-d40e80fb8fb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac63b047-b369-4ea5-a870-32c1350d51ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14eee08c-e66a-4c06-aa8e-d40e80fb8fb5",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "4f2ac3f6-f92f-4c93-9f6b-7076007ef5e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "0e0f7826-bb33-4925-b868-93a229923db8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f2ac3f6-f92f-4c93-9f6b-7076007ef5e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bd89917-5be5-4b43-8784-f8dd939fa132",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f2ac3f6-f92f-4c93-9f6b-7076007ef5e8",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "3fa1b650-115a-48f1-9197-fab8e64052a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "c127b19e-c0d4-47ba-93c6-9f48e35bc1bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fa1b650-115a-48f1-9197-fab8e64052a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cd304a7-95d6-4e86-aa41-ebb3d4dae6fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fa1b650-115a-48f1-9197-fab8e64052a7",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "9763c436-d726-457f-a781-9aec0e148794",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "5f0cb096-26ef-4ac2-be73-face7e952ae5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9763c436-d726-457f-a781-9aec0e148794",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4e6f47e-cdef-43d2-b834-407e44640273",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9763c436-d726-457f-a781-9aec0e148794",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "567d7486-785b-4417-9cfe-dcbde3028534",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "a3f1b40d-8dba-4e6e-b290-4fea8330ed53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "567d7486-785b-4417-9cfe-dcbde3028534",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb67d2aa-b2d4-4861-bf3a-0def300195b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "567d7486-785b-4417-9cfe-dcbde3028534",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "dee474f0-8154-4f79-a46e-c818e4c2dd9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "459efa8a-eab4-4597-ad3e-fbb940a2cb67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dee474f0-8154-4f79-a46e-c818e4c2dd9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d26019d7-8c07-4187-ae4d-44296cab841c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dee474f0-8154-4f79-a46e-c818e4c2dd9b",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "6be0b6b7-004c-4c27-83f4-d873e6d332f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "8e7a530a-d6b9-4086-a43c-fafe08b650d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6be0b6b7-004c-4c27-83f4-d873e6d332f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1391b914-7048-4f22-9ec7-5219fe8a9b0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6be0b6b7-004c-4c27-83f4-d873e6d332f8",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "cafcaf3f-15a9-4f09-8ea1-9df08f94579e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "b646d05b-b8bd-4282-88a2-484ae630004f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cafcaf3f-15a9-4f09-8ea1-9df08f94579e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a71f85cc-2682-4826-ad6d-c66524611a0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cafcaf3f-15a9-4f09-8ea1-9df08f94579e",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        },
        {
            "id": "0bc9edb3-664a-49b1-a8bf-1893e708ae12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "compositeImage": {
                "id": "b9185736-2434-47e5-a0bd-a4466b981be7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bc9edb3-664a-49b1-a8bf-1893e708ae12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6dac88c-61fd-41eb-a808-78957f9b0317",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bc9edb3-664a-49b1-a8bf-1893e708ae12",
                    "LayerId": "11c5bd3a-da64-461c-90d1-91a5abf553e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "11c5bd3a-da64-461c-90d1-91a5abf553e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0fdb714-c6d4-4883-8679-a0e6663c9889",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 0,
    "yorig": 0
}