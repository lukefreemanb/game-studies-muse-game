{
    "id": "d494aa31-af3e-4db4-a126-b2ea08ca5d10",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_froghouse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 16,
    "bbox_right": 128,
    "bbox_top": 48,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b03591f7-a3d2-44bf-bc49-cff5b4afeacd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d494aa31-af3e-4db4-a126-b2ea08ca5d10",
            "compositeImage": {
                "id": "28f1185f-346d-424b-b131-1d672416b82e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b03591f7-a3d2-44bf-bc49-cff5b4afeacd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8aad974-ed77-4d7b-8c2d-6c545efe3ea4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b03591f7-a3d2-44bf-bc49-cff5b4afeacd",
                    "LayerId": "003c9bb4-595f-4c72-9df4-7e64681fdc64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 112,
    "layers": [
        {
            "id": "003c9bb4-595f-4c72-9df4-7e64681fdc64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d494aa31-af3e-4db4-a126-b2ea08ca5d10",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 144,
    "xorig": 72,
    "yorig": 111
}