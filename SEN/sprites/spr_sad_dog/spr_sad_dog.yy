{
    "id": "4064730e-4483-45aa-8e93-ca91285950d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sad_dog",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 4,
    "bbox_right": 28,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "daa02263-000c-49d1-960e-97451bf9d14b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4064730e-4483-45aa-8e93-ca91285950d1",
            "compositeImage": {
                "id": "f1fdd480-4612-4320-bfc0-419edaded417",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daa02263-000c-49d1-960e-97451bf9d14b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a016a32-628c-4b7a-9f2c-e533f95da7e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daa02263-000c-49d1-960e-97451bf9d14b",
                    "LayerId": "4543d3e7-3c27-4cff-a7b4-7da97c7f761f"
                }
            ]
        },
        {
            "id": "873de48e-218b-4c13-9f20-f3499e16806a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4064730e-4483-45aa-8e93-ca91285950d1",
            "compositeImage": {
                "id": "2cc88156-93ff-4294-9194-e5d952049b0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "873de48e-218b-4c13-9f20-f3499e16806a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ce9ed0e-e907-41f3-9e1b-6e25b349395a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "873de48e-218b-4c13-9f20-f3499e16806a",
                    "LayerId": "4543d3e7-3c27-4cff-a7b4-7da97c7f761f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "4543d3e7-3c27-4cff-a7b4-7da97c7f761f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4064730e-4483-45aa-8e93-ca91285950d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 47
}