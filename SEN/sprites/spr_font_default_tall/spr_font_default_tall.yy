{
    "id": "c801214a-9b83-498e-b79c-c53f2f0db061",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_font_default_tall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb5b901e-8b10-450d-832b-2e7b79c0826c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "1a709bec-74b5-4a6e-ae70-af58ef9745d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb5b901e-8b10-450d-832b-2e7b79c0826c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93706377-6ad3-4e6a-9d3a-470236912357",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb5b901e-8b10-450d-832b-2e7b79c0826c",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "6bfb493c-8864-4900-a30a-49a064677e7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "df449779-69ef-43ce-824e-2cb87ac83579",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bfb493c-8864-4900-a30a-49a064677e7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5c858b9-3708-4060-b814-b4c4cddcd5fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bfb493c-8864-4900-a30a-49a064677e7b",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "94a3a148-ee9e-4f73-9acd-7b1c570e640a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "a8a24151-00a7-476f-80ae-4091e7fa85a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94a3a148-ee9e-4f73-9acd-7b1c570e640a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cd372f4-e252-4196-8405-927a760e5205",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94a3a148-ee9e-4f73-9acd-7b1c570e640a",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "5983703a-28dd-4d52-a584-95f2fa35460e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "1f2a06b8-453d-4955-8386-286a16b13a98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5983703a-28dd-4d52-a584-95f2fa35460e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7728185a-c07c-4195-94c0-ce1d05810cf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5983703a-28dd-4d52-a584-95f2fa35460e",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "bf0882e0-b2e7-40a5-8974-601d93cf7386",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "434c3f34-0590-46d9-9a85-2246b9449931",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf0882e0-b2e7-40a5-8974-601d93cf7386",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3bc6cb6-cd25-4f5f-a2cc-885320f0e2f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf0882e0-b2e7-40a5-8974-601d93cf7386",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "e168249f-468e-4bdb-8e33-fb2c57e86fcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "3ee3e94b-0dbf-4385-9a4e-7dd5c28fa0cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e168249f-468e-4bdb-8e33-fb2c57e86fcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c66544c-7afb-4eed-9b38-9e99ecd25e84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e168249f-468e-4bdb-8e33-fb2c57e86fcb",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "ba30a09f-0576-4cf4-8bfd-62640df7b78b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "1aad9a40-b08b-4d3e-ad92-e6e3a3b0fa70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba30a09f-0576-4cf4-8bfd-62640df7b78b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd44718e-bbf9-4a73-8bd0-0da2ebc1e09c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba30a09f-0576-4cf4-8bfd-62640df7b78b",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "216e185e-88a9-4482-b7c1-b62f91aa112d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "97a89a24-a266-442f-9f81-6b266059b433",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "216e185e-88a9-4482-b7c1-b62f91aa112d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c1cf62e-158e-40da-beff-727b726844c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "216e185e-88a9-4482-b7c1-b62f91aa112d",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "d0c4cbd6-11aa-45ab-ad0d-6973c5f5a30b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "e121a16d-67c5-4481-a9b7-8c68199f7a07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0c4cbd6-11aa-45ab-ad0d-6973c5f5a30b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "845ff0fc-92a3-40ad-a962-e64f2cbf379f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0c4cbd6-11aa-45ab-ad0d-6973c5f5a30b",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "ec9dd6a2-4637-4539-80e9-d0149a6917ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "df55a65f-9155-4c24-a6e4-0d4da5becc7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec9dd6a2-4637-4539-80e9-d0149a6917ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f00eaaa9-85cd-40b6-a9f4-134ef73885eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec9dd6a2-4637-4539-80e9-d0149a6917ac",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "6a9e16b0-0985-4235-aa95-272eaad6cb0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "f169863c-fada-463f-89f3-f7fea1a51135",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a9e16b0-0985-4235-aa95-272eaad6cb0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faf65673-58a2-432e-ac8d-afd9ca20d4b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a9e16b0-0985-4235-aa95-272eaad6cb0b",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "d5689018-fd5c-4b6c-81ec-344aa564020a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "fea3cdd2-ccb8-4fbc-9ec9-c7b6122fca8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5689018-fd5c-4b6c-81ec-344aa564020a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f3f355d-6baa-4615-9384-9cda2e380521",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5689018-fd5c-4b6c-81ec-344aa564020a",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "659fdac2-96c9-4017-9311-0025f1ebfd57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "b803dd1d-c1a0-4cdb-baa3-797aa6e95eb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "659fdac2-96c9-4017-9311-0025f1ebfd57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf904b00-e486-4c14-a6d9-fe435a91d6e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "659fdac2-96c9-4017-9311-0025f1ebfd57",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "e88e190b-f011-4e24-84ae-60fbf57b0cad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "2180b8db-1256-4220-8281-57e566296923",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e88e190b-f011-4e24-84ae-60fbf57b0cad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5056c88-6291-4433-854e-82d943771c78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e88e190b-f011-4e24-84ae-60fbf57b0cad",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "c2f3833c-804e-47ff-bd04-2fa44fab708b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "8f3013df-d7ec-4369-a245-99b7624d3f96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2f3833c-804e-47ff-bd04-2fa44fab708b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19122f95-271a-40f2-9fe7-a61296912f58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2f3833c-804e-47ff-bd04-2fa44fab708b",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "d1790851-7f3b-4639-8b53-c1479f7e3042",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "4fb4bd27-5910-42c8-a1b0-e9a30d99c83d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1790851-7f3b-4639-8b53-c1479f7e3042",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf5a7f27-053b-4c1f-af2c-e117a3adeecb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1790851-7f3b-4639-8b53-c1479f7e3042",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "69c37930-7013-4c53-ae35-1d53fa18755f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "db406745-bc8b-4173-a2cf-41cd4adff2d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69c37930-7013-4c53-ae35-1d53fa18755f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98e77383-19ac-47c6-acdb-584b8b7433f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69c37930-7013-4c53-ae35-1d53fa18755f",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "2116e7ee-6115-488e-a291-743c4493854f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "e9494b8f-8ffa-46f8-b48e-05ae120b41dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2116e7ee-6115-488e-a291-743c4493854f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e0affa4-c9ca-406c-857b-d48d999890c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2116e7ee-6115-488e-a291-743c4493854f",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "c891e403-da4b-4941-bb3e-05473a0599e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "49b7d291-180f-4509-aa36-3b7f99afe33b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c891e403-da4b-4941-bb3e-05473a0599e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c938044d-7fd8-454b-86ea-04452fd00ccf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c891e403-da4b-4941-bb3e-05473a0599e2",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "cf895b09-5bb7-4df5-a56c-59894f189bc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "2fb4b545-b8f1-455e-ae5a-b90ba668b106",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf895b09-5bb7-4df5-a56c-59894f189bc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dabe9359-5561-4f04-a911-ed0133870f6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf895b09-5bb7-4df5-a56c-59894f189bc2",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "2e929b6e-ffa2-4603-bffb-0c575a308a9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "d691c050-0e8b-48f2-b8ac-770c95c3f5e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e929b6e-ffa2-4603-bffb-0c575a308a9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58dc4554-555a-4449-8bf3-407bfea8bd91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e929b6e-ffa2-4603-bffb-0c575a308a9b",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "59966654-55f5-4e54-82b1-47778f6a936a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "14aa0d73-2f04-46bb-b99d-1a158ff10d0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59966654-55f5-4e54-82b1-47778f6a936a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3afd75f9-7434-4e61-a305-7bb5056c0771",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59966654-55f5-4e54-82b1-47778f6a936a",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "c0c672ac-6c4e-46b6-89a6-94367fd30326",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "412d10ad-2b06-4931-9ae8-dcfe5e273e91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0c672ac-6c4e-46b6-89a6-94367fd30326",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca0c9c08-f797-4111-b159-2ff59c006ff0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0c672ac-6c4e-46b6-89a6-94367fd30326",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "78af0087-1338-4fc1-b120-842a7a4bbccf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "6cf7265e-2ad6-4031-86ed-ab3b2a9a8d62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78af0087-1338-4fc1-b120-842a7a4bbccf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9f63d2c-3fef-4de5-99e8-9b56b0bbbe15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78af0087-1338-4fc1-b120-842a7a4bbccf",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "f5cb47fb-8e70-49fb-ac12-212aa1197a5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "337cbad1-6916-4cd9-834e-70df88362304",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5cb47fb-8e70-49fb-ac12-212aa1197a5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab0ccb85-bbae-447f-9bd4-065430e02834",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5cb47fb-8e70-49fb-ac12-212aa1197a5f",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "58bc2bad-669d-478f-8e65-0ad585d6f9aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "0634dad0-028b-45f2-8213-9b956d220037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58bc2bad-669d-478f-8e65-0ad585d6f9aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d90916d-8912-42fa-bcf9-abc5d2e4fecc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58bc2bad-669d-478f-8e65-0ad585d6f9aa",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "41c60895-9efa-452d-b227-2469d30c803b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "3c37bbb1-2113-448d-9838-88048040faf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41c60895-9efa-452d-b227-2469d30c803b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "177ed739-079e-4dab-be5c-bf85fd833b65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41c60895-9efa-452d-b227-2469d30c803b",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "87cdf186-f635-4010-a2c2-94f15dac8034",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "704941bc-3f18-4469-9681-e6d2b51e889d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87cdf186-f635-4010-a2c2-94f15dac8034",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c38f333-478d-4da9-968b-854c4f249595",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87cdf186-f635-4010-a2c2-94f15dac8034",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "13ef4ae0-177d-4bbd-bce8-9ab74cfa7f82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "eb8ef5c6-93cb-4041-a88d-dce82c3a4b16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13ef4ae0-177d-4bbd-bce8-9ab74cfa7f82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a2a2281-5fad-4503-bf73-bcf0e3747917",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13ef4ae0-177d-4bbd-bce8-9ab74cfa7f82",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "b4f5f5aa-15f7-4cf3-a910-73893599fa0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "50e9075f-b0ae-456c-ad96-0e11a31cf6e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4f5f5aa-15f7-4cf3-a910-73893599fa0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b781797-bc01-4a7e-a2b4-5b6907449f3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4f5f5aa-15f7-4cf3-a910-73893599fa0f",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "f5636806-6e6d-4f40-ab7d-4506d1fec6da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "0982b9ed-f987-4f19-8e54-da3e7d30eda3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5636806-6e6d-4f40-ab7d-4506d1fec6da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3feb255e-b1ec-4bbd-9211-4ad19bfd2925",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5636806-6e6d-4f40-ab7d-4506d1fec6da",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "ae26a307-d077-45b0-b45c-8bf5316aec1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "24346a1d-8033-4114-912a-7389ac45d683",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae26a307-d077-45b0-b45c-8bf5316aec1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80609779-5e73-42f7-8c88-c579857f2a12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae26a307-d077-45b0-b45c-8bf5316aec1d",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "51ebc37e-5d7c-4553-8c00-0e603c4ce7e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "9f0e084d-8e47-4ac8-95b1-33a6c3a72a61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51ebc37e-5d7c-4553-8c00-0e603c4ce7e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "523fe33d-5554-4b8c-9c1d-024624e0e331",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51ebc37e-5d7c-4553-8c00-0e603c4ce7e6",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "31c71489-91f5-4266-aa8a-3ef31c7e6059",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "25223e0b-321d-4e5b-80b9-cd53e777d40b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31c71489-91f5-4266-aa8a-3ef31c7e6059",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6430007-92f6-4be6-bb7b-2eeccbd10b68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31c71489-91f5-4266-aa8a-3ef31c7e6059",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "bcf5d8de-0cd9-404f-ace8-c104bac2c566",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "c0245b14-3f47-473c-85c3-c93af3451bdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcf5d8de-0cd9-404f-ace8-c104bac2c566",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99aef7f9-08ce-464e-84ee-980676710914",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcf5d8de-0cd9-404f-ace8-c104bac2c566",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "3b77c80b-91cc-4be0-bb62-eb5c998389be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "e7b87159-9c3d-43aa-bef8-20c96694878d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b77c80b-91cc-4be0-bb62-eb5c998389be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4900b416-9f7a-43e0-8d4f-eeb63c44f979",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b77c80b-91cc-4be0-bb62-eb5c998389be",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "c0e2a2c7-d8bc-4863-9f75-bb8d2f69a293",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "17925195-fb4c-41e3-b93d-2536e1ff9c15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0e2a2c7-d8bc-4863-9f75-bb8d2f69a293",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8f17ca4-aa75-4373-a77b-7510e95aa11a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0e2a2c7-d8bc-4863-9f75-bb8d2f69a293",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "752f0f5a-fead-4b85-b93a-06402eec714e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "254f2016-5f65-49b7-9f20-3853131cbaa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "752f0f5a-fead-4b85-b93a-06402eec714e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6d86f59-5f74-4a79-86cb-4852116016d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "752f0f5a-fead-4b85-b93a-06402eec714e",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "35a8d8be-0b60-4b1f-ac88-00ee9d056365",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "253666ab-3f98-481a-bf72-c1724754ef35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35a8d8be-0b60-4b1f-ac88-00ee9d056365",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ee2aa53-9697-4c0d-b9b5-1df26f1bdd07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35a8d8be-0b60-4b1f-ac88-00ee9d056365",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "75031b01-4081-4001-a219-01a1def4046f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "f61dcd8a-d95c-4bb5-a766-ec3d7ae18110",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75031b01-4081-4001-a219-01a1def4046f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a7084f1-6ff5-4451-8904-1a98056b7253",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75031b01-4081-4001-a219-01a1def4046f",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "27354b9f-b38e-4e5b-a648-bb1f58c43f77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "7536c9c7-ca5d-43af-a2f3-961f437eebb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27354b9f-b38e-4e5b-a648-bb1f58c43f77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41eecc26-7ab5-4b02-9d94-35fe2da65461",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27354b9f-b38e-4e5b-a648-bb1f58c43f77",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "73d89714-1af7-4aed-a905-f484e90a57d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "a36c9382-9321-4d18-88a4-2b0e2076fa7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73d89714-1af7-4aed-a905-f484e90a57d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "640b13e6-a783-4645-b247-ba2a07cc62bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73d89714-1af7-4aed-a905-f484e90a57d3",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "e72dc0d9-b974-4841-8540-c415fb75d178",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "f3e1a07b-10cf-4d0c-854c-c2272ae8f68d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e72dc0d9-b974-4841-8540-c415fb75d178",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7b47d46-8ff3-4fc1-8a55-cfad4c2d7af6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e72dc0d9-b974-4841-8540-c415fb75d178",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "54b929e4-61b9-4b72-b81a-37e8031d579f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "7ddc41ad-650e-43bd-845c-f0836c3941cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54b929e4-61b9-4b72-b81a-37e8031d579f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6129c0c-098c-4145-be17-20e70fd098eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54b929e4-61b9-4b72-b81a-37e8031d579f",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "ac32d922-01c0-4fc1-be70-c8114a330678",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "fd45e267-97a5-42eb-a376-9bebf9f90f4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac32d922-01c0-4fc1-be70-c8114a330678",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a0c847f-5bb5-4169-98b0-c7759e0a0406",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac32d922-01c0-4fc1-be70-c8114a330678",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "7f96ac2a-3cdc-4042-bfa9-a1f6e9bf3351",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "981eb78d-8411-4473-8c4c-065df80117f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f96ac2a-3cdc-4042-bfa9-a1f6e9bf3351",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8df0df25-98ff-4fb7-9bdb-cc49ffff1fa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f96ac2a-3cdc-4042-bfa9-a1f6e9bf3351",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "bb801826-8d94-4b63-b413-d2d9f612d92f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "8e2898bb-3736-4968-8957-782f29223d06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb801826-8d94-4b63-b413-d2d9f612d92f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eefe864e-d8d1-484e-997b-5b57a044ca50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb801826-8d94-4b63-b413-d2d9f612d92f",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "337f29e0-91bf-4239-8d90-cfbb7bccf7aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "9a50d135-5f6d-4c27-a7dc-75b8ed615a70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "337f29e0-91bf-4239-8d90-cfbb7bccf7aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50ed5689-0508-4d51-bafa-dc8194605bbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "337f29e0-91bf-4239-8d90-cfbb7bccf7aa",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "a2807b3b-633c-4ec2-b850-c60a0ef331f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "680b15a3-3ae3-4b9e-a011-bed9d49b20ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2807b3b-633c-4ec2-b850-c60a0ef331f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26eaca2e-8745-429f-a826-28eb328bada7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2807b3b-633c-4ec2-b850-c60a0ef331f2",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "72cde288-d5f4-48b3-8e1c-0c97f2807ea9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "5faeb2d4-0c3d-4bcb-80d0-8f5d2b0332f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72cde288-d5f4-48b3-8e1c-0c97f2807ea9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d651349-eba1-49c0-900a-cd40a87d4cf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72cde288-d5f4-48b3-8e1c-0c97f2807ea9",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "bbfb5e67-89a3-40f2-8c68-b68b6817ecf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "1cb1e111-b12c-4e38-b03e-f1959b04a248",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbfb5e67-89a3-40f2-8c68-b68b6817ecf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f60d193d-0d5e-4e9f-85f8-64426f77062b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbfb5e67-89a3-40f2-8c68-b68b6817ecf1",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "6551ccde-b1bd-4d4c-9fe2-e45b32138210",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "82d636bc-686f-41ac-b889-163633abef92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6551ccde-b1bd-4d4c-9fe2-e45b32138210",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e7194e6-967f-4ed1-8b28-b9a7749fe066",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6551ccde-b1bd-4d4c-9fe2-e45b32138210",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "9c56371b-d70b-40bd-ba19-c444e6224417",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "59c03dc8-d9ca-4e1a-9380-326f43ca79e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c56371b-d70b-40bd-ba19-c444e6224417",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3c14a53-4bed-4551-a2da-073b239b27f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c56371b-d70b-40bd-ba19-c444e6224417",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "f5631798-b018-4b78-9a03-da9cecb0a4ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "be7bd5ee-eba2-423a-a9dc-08913e0e0d0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5631798-b018-4b78-9a03-da9cecb0a4ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa7a7459-5934-41f7-8075-10d87640332c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5631798-b018-4b78-9a03-da9cecb0a4ff",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "d2f9f2c9-3eb5-424a-961d-863a9921d094",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "e1828ae9-c8ff-4111-bab4-047b6d8ef470",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2f9f2c9-3eb5-424a-961d-863a9921d094",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a0a4057-f692-4588-a92a-5fdc5a3624bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2f9f2c9-3eb5-424a-961d-863a9921d094",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "61ce44e9-126b-483d-b642-83c2f1635c08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "3ef0037a-057b-432b-a1fc-b8c0d551bd31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61ce44e9-126b-483d-b642-83c2f1635c08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dcdd3bc-cca9-4fee-b85a-770d093fd587",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61ce44e9-126b-483d-b642-83c2f1635c08",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "1ba2f4f4-a86a-46c7-85e8-210b51bbd544",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "d46c7813-6b7e-4122-b3b4-b7214fbb11b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ba2f4f4-a86a-46c7-85e8-210b51bbd544",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b35bacff-5534-49ff-8bc3-f5eb57b1f911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ba2f4f4-a86a-46c7-85e8-210b51bbd544",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "f71f780c-eb9e-4168-b71f-8d46e9ff9e05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "e3ae59ae-60b1-45c6-9283-bd132b86b737",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f71f780c-eb9e-4168-b71f-8d46e9ff9e05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c30fff6f-a75d-4f34-92f5-523f2ba94a74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f71f780c-eb9e-4168-b71f-8d46e9ff9e05",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "5e07b84c-4b52-4c79-8b3f-d6bac6df9037",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "6da5dd67-75a6-4ceb-a327-e6aba11cb191",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e07b84c-4b52-4c79-8b3f-d6bac6df9037",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "173bf246-66d1-4315-a40d-a5e74c9b2bfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e07b84c-4b52-4c79-8b3f-d6bac6df9037",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "c5c10c07-a5f6-421f-9bde-ecf01ea354ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "bda77252-ff37-47f5-86e7-4677eae7b244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5c10c07-a5f6-421f-9bde-ecf01ea354ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "548d8a5e-3432-4aa9-98e8-c50815fc32e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5c10c07-a5f6-421f-9bde-ecf01ea354ae",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "15b52ae2-6927-4a71-875b-7c13227cd032",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "de388ca8-b403-420a-b992-5ba10218052b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15b52ae2-6927-4a71-875b-7c13227cd032",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcf708bc-9e97-4a1d-a44f-e8c70cacd8d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15b52ae2-6927-4a71-875b-7c13227cd032",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "612701dd-1415-47a0-9147-0c1d4c110bbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "09143220-5d7a-47da-a266-5ac743a9f302",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "612701dd-1415-47a0-9147-0c1d4c110bbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00fa6914-e429-426b-a6cd-81854bdcacdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "612701dd-1415-47a0-9147-0c1d4c110bbc",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "82d4a5e5-aa18-49db-b0ce-79715f6e4302",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "abc88aee-8ad9-4f5e-9b0f-5e8ca2bb0c85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82d4a5e5-aa18-49db-b0ce-79715f6e4302",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2990c1f4-a374-4ccd-a322-33d8ec7129eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82d4a5e5-aa18-49db-b0ce-79715f6e4302",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "ed9883e5-f71a-48f2-ae5d-b5f49c8113e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "284d5688-5e22-4e22-827b-d63d8ad1847c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed9883e5-f71a-48f2-ae5d-b5f49c8113e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1025cd4c-54d5-48f1-b292-5365278d77ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed9883e5-f71a-48f2-ae5d-b5f49c8113e7",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "29362018-f387-4907-bc4f-ec90bed6b245",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "70cfa2ed-748d-4749-ad66-98c5a06e9c40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29362018-f387-4907-bc4f-ec90bed6b245",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6642263b-33aa-4393-9ee7-c625aa9b6c54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29362018-f387-4907-bc4f-ec90bed6b245",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "c2a30d7e-f8bf-4536-8e8b-9f4b601cdec6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "dc26b04c-096c-4564-ab1f-adc991814057",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2a30d7e-f8bf-4536-8e8b-9f4b601cdec6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "861eeb59-42be-4f3a-9a1f-4e2a8fe5d5ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2a30d7e-f8bf-4536-8e8b-9f4b601cdec6",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "2390e49b-f163-4484-b2f7-23d7d3c8ca29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "ae1c6e07-e373-446e-8997-f9a84577e31b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2390e49b-f163-4484-b2f7-23d7d3c8ca29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d0ea9cc-debf-4c7b-8ef4-400bcca3ec87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2390e49b-f163-4484-b2f7-23d7d3c8ca29",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "636c641c-61e4-4496-9bf7-568377ffebba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "e9db4794-d4a8-4ea6-b163-b4b0ef1030d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "636c641c-61e4-4496-9bf7-568377ffebba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "768c3d62-d0ae-454f-9ac0-0bfd05d19a7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "636c641c-61e4-4496-9bf7-568377ffebba",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "7303bd6b-81e9-4382-ae88-3d55d82a681f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "4adb3913-808e-4a67-8b9d-b779f5a52c61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7303bd6b-81e9-4382-ae88-3d55d82a681f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "129307b8-3ce8-4dbd-aeab-f4c1e46732ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7303bd6b-81e9-4382-ae88-3d55d82a681f",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "ad837b57-29e2-44e4-8041-751fdb010781",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "8dd903d8-d636-4918-9f3c-70950b3ccb40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad837b57-29e2-44e4-8041-751fdb010781",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a65e7c1e-a3de-47f0-85ae-3c6e83bfbb92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad837b57-29e2-44e4-8041-751fdb010781",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "65195b03-6150-4adf-87b3-447381b35619",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "ae9ae42b-7320-4f2e-8084-91141787f523",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65195b03-6150-4adf-87b3-447381b35619",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3d2b500-eedd-4d2e-9adf-b1ac44c6253f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65195b03-6150-4adf-87b3-447381b35619",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "083747cf-ed7c-44ad-a681-219151ee36b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "d6a45ec7-8def-4aaf-a782-cbba839637ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "083747cf-ed7c-44ad-a681-219151ee36b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efd658d7-3d02-4544-b910-35c6860d8e5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "083747cf-ed7c-44ad-a681-219151ee36b8",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "9e16c62b-47fd-45be-bbe8-1938d75e2bc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "435a0a0d-de32-4ccf-8d50-ac46ae3aa13c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e16c62b-47fd-45be-bbe8-1938d75e2bc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b43a692-86ed-41c7-9a98-23504dcddfd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e16c62b-47fd-45be-bbe8-1938d75e2bc9",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "1deaa542-2fc6-4945-9678-76b226b55f27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "99e5405d-098d-4ded-a99e-1424beed19b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1deaa542-2fc6-4945-9678-76b226b55f27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20d10037-624e-4f8c-8772-116f986b90bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1deaa542-2fc6-4945-9678-76b226b55f27",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "c4b746b2-038c-4994-bd7b-c762b1021eb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "f689bdf5-7af0-48da-a84d-6ae30baf37af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4b746b2-038c-4994-bd7b-c762b1021eb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47d9cf83-72cd-430f-9ad3-8e9e0cd89d1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4b746b2-038c-4994-bd7b-c762b1021eb8",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "72729a5d-3c46-4e06-b1c0-a3e57cfe9a60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "fe9344e6-2fcd-40b1-8733-34d96099c7c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72729a5d-3c46-4e06-b1c0-a3e57cfe9a60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b111a86-fdf9-4f08-b834-e7a0401aa65d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72729a5d-3c46-4e06-b1c0-a3e57cfe9a60",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "5b943fa2-3fec-4666-9417-bcd7cec46e48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "22d7fb7c-cc6e-405c-9811-36e35b21f4e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b943fa2-3fec-4666-9417-bcd7cec46e48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29ce4892-45c4-40ab-a362-16f93b361a64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b943fa2-3fec-4666-9417-bcd7cec46e48",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "17eafc77-7d96-4d10-b18b-d5b26c106c12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "c8fe26dd-5732-41a3-b266-9322c76e584f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17eafc77-7d96-4d10-b18b-d5b26c106c12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1ea4f3a-cc43-405b-9ada-520144c99900",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17eafc77-7d96-4d10-b18b-d5b26c106c12",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "f5f3c978-95c7-4d0a-946b-1b51dbc45a03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "7fd12094-1e3e-4213-a011-a277e10f6335",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5f3c978-95c7-4d0a-946b-1b51dbc45a03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23a2fbe6-f82e-4f73-a72b-1dffa108d6f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5f3c978-95c7-4d0a-946b-1b51dbc45a03",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "c231748c-442c-468c-9587-2087ffaadc73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "16c231a0-25d2-4d37-89a5-6a3850af95d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c231748c-442c-468c-9587-2087ffaadc73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea630ab5-b63e-48ed-8c77-5534dd613216",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c231748c-442c-468c-9587-2087ffaadc73",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "2eb862d6-d157-4186-a4dd-aaf1ecf69ba6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "ac5dcb70-5e5b-4d06-9f10-ce8522d4778f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2eb862d6-d157-4186-a4dd-aaf1ecf69ba6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a5ad51c-1c9f-4cd2-9b23-a33370063ced",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2eb862d6-d157-4186-a4dd-aaf1ecf69ba6",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "8f7b8451-b422-4736-a846-5b8563f440cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "7fa77fce-7b9a-4dca-a21f-53ceb75d63e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f7b8451-b422-4736-a846-5b8563f440cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62597d5a-c574-4726-b1f6-6a2051841fea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f7b8451-b422-4736-a846-5b8563f440cc",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "8fb2ebe1-1d36-4d22-988d-6e10209f3461",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "fff48e4b-cd91-4c7a-975e-8f67811ef4fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fb2ebe1-1d36-4d22-988d-6e10209f3461",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fb5aad3-ba3c-4096-a7a9-1697d1c598e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fb2ebe1-1d36-4d22-988d-6e10209f3461",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "c016bf88-b9b1-4106-8c97-a68e4c158e5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "6ffb8f4a-d587-4e49-88f5-7ce53a4475af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c016bf88-b9b1-4106-8c97-a68e4c158e5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d185978-61ce-45f9-8e6d-357298193b1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c016bf88-b9b1-4106-8c97-a68e4c158e5b",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "6b98eb86-f682-45be-ba61-9f8dbac2f64f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "d6860ccf-cc5a-46a1-96eb-5ad05dc3d59f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b98eb86-f682-45be-ba61-9f8dbac2f64f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "032631f0-bd67-4d98-ad47-3202b6630b2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b98eb86-f682-45be-ba61-9f8dbac2f64f",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "a55dfd8a-4933-4732-b9c4-0abf52369cb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "802c9982-6d2c-4c44-bc50-2b7c9fe4d676",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a55dfd8a-4933-4732-b9c4-0abf52369cb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa0900cf-f6d6-485e-99ed-13955d33bf7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a55dfd8a-4933-4732-b9c4-0abf52369cb9",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "d56b7b19-cda5-4723-b53a-cffc40325720",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "26fe65a3-e188-4432-a5fd-eee5c4a883f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d56b7b19-cda5-4723-b53a-cffc40325720",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab90ea93-277e-4d69-b5e4-fcbdd115088c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d56b7b19-cda5-4723-b53a-cffc40325720",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "e38b016b-0afb-406d-a040-cf6d22fda3aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "e0432b1f-255f-4ba8-bdeb-f0bf26ebd140",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e38b016b-0afb-406d-a040-cf6d22fda3aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9757aab0-cf32-473c-ac50-507b3a640f59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e38b016b-0afb-406d-a040-cf6d22fda3aa",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "304cc104-01cd-4e0b-8efb-a15b29955a13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "0cfc360c-377d-45db-a387-650a9e96eeb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "304cc104-01cd-4e0b-8efb-a15b29955a13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edfd6c50-2028-4191-aa86-74505aadea58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "304cc104-01cd-4e0b-8efb-a15b29955a13",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "86f3c7bb-8f79-464a-b4a1-229ff525e1e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "e043f670-3606-47ae-8ff4-b93109302ba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86f3c7bb-8f79-464a-b4a1-229ff525e1e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d22c8bc9-3d41-4e15-9910-7bbaf6221b9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86f3c7bb-8f79-464a-b4a1-229ff525e1e9",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "70fa14fa-f77d-45d6-a6c0-736afc67f5e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "7b46bee8-80ca-439a-b5fe-c0ccd15d4013",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70fa14fa-f77d-45d6-a6c0-736afc67f5e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8faf636f-0472-4cc9-a915-eed8415e55d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70fa14fa-f77d-45d6-a6c0-736afc67f5e9",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "c768542c-680c-420b-bb92-5ff5220ab8e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "fa647f2a-9afa-4ca6-af0d-f163843a90f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c768542c-680c-420b-bb92-5ff5220ab8e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f48831d1-8630-4891-8d85-435b78a8e919",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c768542c-680c-420b-bb92-5ff5220ab8e4",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "39c3ea92-c0eb-4d9a-af09-773879569353",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "a9769d35-b692-432f-b9f0-a34754b63d18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39c3ea92-c0eb-4d9a-af09-773879569353",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a2aa945-ba19-4aad-b0ef-8e75e4862151",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39c3ea92-c0eb-4d9a-af09-773879569353",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "5ef12969-cd0c-4f4c-8964-d37a614fdddb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "130d9eca-578b-4f88-9b00-9607bc0c8ba0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ef12969-cd0c-4f4c-8964-d37a614fdddb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a05e0f26-b916-4bc1-983f-de1a6ee8a971",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ef12969-cd0c-4f4c-8964-d37a614fdddb",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "3f10311a-c999-4222-a90e-ebf19fc9619b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "32388ba0-6be8-412a-b127-f0e09083abf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f10311a-c999-4222-a90e-ebf19fc9619b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44e18071-4fae-45a1-b3ea-839c59406d90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f10311a-c999-4222-a90e-ebf19fc9619b",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "f22f2bfb-8ca8-4054-bd4f-59ef73229ac8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "7349fba2-0b94-4f91-8ce9-356acf02f291",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f22f2bfb-8ca8-4054-bd4f-59ef73229ac8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "417f0ae6-538e-4896-98e3-f6011f0675b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f22f2bfb-8ca8-4054-bd4f-59ef73229ac8",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "3c352ea5-1fe4-44d3-b32a-a2fec6fc3809",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "36c185f5-4855-4a56-bffd-237967855752",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c352ea5-1fe4-44d3-b32a-a2fec6fc3809",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3abf1e5f-8637-471a-a74b-6d7d7605d2e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c352ea5-1fe4-44d3-b32a-a2fec6fc3809",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "b1425534-ccbf-42d0-a5b0-9c56af1da689",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "e8086c58-619c-4a2a-a14e-c01a293bbe33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1425534-ccbf-42d0-a5b0-9c56af1da689",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b057ed6-3ad8-40c1-996b-88395cbad0b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1425534-ccbf-42d0-a5b0-9c56af1da689",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "4c5230f7-6d4b-4f85-ae9f-f4e87d1c054f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "c517d68b-7fd7-4d68-9301-c8336fb93dc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c5230f7-6d4b-4f85-ae9f-f4e87d1c054f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59dad56d-a26c-487e-b0bc-ea7ebdc1ba86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c5230f7-6d4b-4f85-ae9f-f4e87d1c054f",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "af839809-a523-47be-aa19-011bb2deaa67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "0ff76c81-d7d7-4683-a582-472f28822711",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af839809-a523-47be-aa19-011bb2deaa67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74723f40-3031-4a6f-a785-ef6e704ac94e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af839809-a523-47be-aa19-011bb2deaa67",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "771d67de-b45b-4557-a4a9-582e9a15323e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "dd5af5ec-c683-4216-a8ea-ab19ce5c9ab3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "771d67de-b45b-4557-a4a9-582e9a15323e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b2967f1-59a0-4c3d-9588-475ff3e36c40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "771d67de-b45b-4557-a4a9-582e9a15323e",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "16a11e51-a536-4221-b134-8eb9ea06e9db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "b6a23167-7442-46f7-8a00-62e44797ae8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16a11e51-a536-4221-b134-8eb9ea06e9db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a176b80-347a-4a61-9aa5-cc24810a65cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16a11e51-a536-4221-b134-8eb9ea06e9db",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "03953fe6-4674-4abb-9f13-6555f3e5a671",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "ca7c57f8-552d-4eae-8b8d-4f7aadcb8227",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03953fe6-4674-4abb-9f13-6555f3e5a671",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25f0b0d8-02d9-413a-91fb-ae52220d403f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03953fe6-4674-4abb-9f13-6555f3e5a671",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "bcef78aa-2765-4308-a439-1e236a9efd6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "e33d53f1-c9ab-48b1-86b9-c0116740a35b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcef78aa-2765-4308-a439-1e236a9efd6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09398205-b24b-448c-99d4-34da1d8f2859",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcef78aa-2765-4308-a439-1e236a9efd6c",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "068fb693-4a4f-42ad-9be1-a0c0c9a3d019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "b8598f28-6feb-4fff-b019-0c29d675b17d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "068fb693-4a4f-42ad-9be1-a0c0c9a3d019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "736f43bc-d6a8-407b-b963-55f61f31287f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "068fb693-4a4f-42ad-9be1-a0c0c9a3d019",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "dad00aaa-6df8-4853-a53c-9fec1c938396",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "35f022be-a626-4632-8541-5964f20aa56b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dad00aaa-6df8-4853-a53c-9fec1c938396",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "265d6a6b-c2ea-4eef-9702-eda97034273b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dad00aaa-6df8-4853-a53c-9fec1c938396",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "511a500d-a77d-496d-93d8-35c81735699a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "a8eb3703-e4e4-4141-994c-df992e39bf30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "511a500d-a77d-496d-93d8-35c81735699a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69a84ce7-a046-4126-bd47-95f3a1d9e3e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "511a500d-a77d-496d-93d8-35c81735699a",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "c0fa2142-e9ba-419c-a488-2db995726ef8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "e82d0c68-12a9-4c05-8c57-495684fe2af3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0fa2142-e9ba-419c-a488-2db995726ef8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82edfad2-a8d1-4602-b511-5a6851eb5bd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0fa2142-e9ba-419c-a488-2db995726ef8",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "36c6630a-b1b6-4860-9173-7a7abe00f05f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "c9bfb21f-4112-4ca3-af15-5dfe9b95fa4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36c6630a-b1b6-4860-9173-7a7abe00f05f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a387e4f0-5176-4fe4-861f-3e2672722141",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36c6630a-b1b6-4860-9173-7a7abe00f05f",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "c368368a-21b0-4bab-9931-f4fe00ae712a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "533591ee-3f60-4ed6-84cf-ac8a69f2846f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c368368a-21b0-4bab-9931-f4fe00ae712a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d5c3fc5-f912-4f13-a302-8933ec1c326e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c368368a-21b0-4bab-9931-f4fe00ae712a",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "777785ee-a0f2-4eb3-8dde-cc2cffd5173a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "a44b4d42-6f3a-434f-b74e-318c0f457c90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "777785ee-a0f2-4eb3-8dde-cc2cffd5173a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07da9ef6-cd71-4417-8dbe-4c954cbdcda8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "777785ee-a0f2-4eb3-8dde-cc2cffd5173a",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        },
        {
            "id": "49c6f320-1a7d-45ff-8e6e-9b33df77719b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "compositeImage": {
                "id": "2fc1d318-cd1b-4c44-8edc-73adebda7265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49c6f320-1a7d-45ff-8e6e-9b33df77719b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eacb836-52d9-4ac7-a16d-4e6473b35b58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49c6f320-1a7d-45ff-8e6e-9b33df77719b",
                    "LayerId": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "ca1fb6dc-5c78-472a-8cb3-b63cc7782a78",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c801214a-9b83-498e-b79c-c53f2f0db061",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 0,
    "yorig": 0
}