{
    "id": "fdf1eb25-ede0-42df-8049-0fcf03c12a05",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_off",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98ec79a9-39b6-4ad3-aa4e-13ca6ac19c94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdf1eb25-ede0-42df-8049-0fcf03c12a05",
            "compositeImage": {
                "id": "d62d8b58-8995-4a4e-a7fa-501f5eea88b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98ec79a9-39b6-4ad3-aa4e-13ca6ac19c94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48ebca1e-e922-4e60-8576-3361b23d6901",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98ec79a9-39b6-4ad3-aa4e-13ca6ac19c94",
                    "LayerId": "fb1d1c35-5e6b-405f-abb3-c121c1fd7a41"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fb1d1c35-5e6b-405f-abb3-c121c1fd7a41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fdf1eb25-ede0-42df-8049-0fcf03c12a05",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}