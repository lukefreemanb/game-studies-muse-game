{
    "id": "a32ba94c-12ad-42a6-87f2-54dc30c92a41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 4,
    "bbox_right": 20,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9a638977-32bf-46bb-9713-ed922eede106",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a32ba94c-12ad-42a6-87f2-54dc30c92a41",
            "compositeImage": {
                "id": "19b8f334-56a8-4d1b-8a1c-936fd17a13fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a638977-32bf-46bb-9713-ed922eede106",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3037b94-5f8c-43bd-9f84-9ac48bfdd83f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a638977-32bf-46bb-9713-ed922eede106",
                    "LayerId": "b23f728a-ed79-49ae-b13d-6e1c647c9839"
                }
            ]
        },
        {
            "id": "106a2ddb-99b3-44fe-b76f-a02fa2aa82c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a32ba94c-12ad-42a6-87f2-54dc30c92a41",
            "compositeImage": {
                "id": "b54e3fe9-6ed0-4410-ac9b-3702e0cd6c75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "106a2ddb-99b3-44fe-b76f-a02fa2aa82c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85a4c7a1-a594-467d-bd68-8b115382e37e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "106a2ddb-99b3-44fe-b76f-a02fa2aa82c3",
                    "LayerId": "b23f728a-ed79-49ae-b13d-6e1c647c9839"
                }
            ]
        },
        {
            "id": "a473da9b-d8ad-42e0-944e-ec95977cd0c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a32ba94c-12ad-42a6-87f2-54dc30c92a41",
            "compositeImage": {
                "id": "91f49565-1c08-459c-85d3-3fa3a88c2eb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a473da9b-d8ad-42e0-944e-ec95977cd0c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fff4a9b9-4a78-4b50-ac28-78c2c08e9ea1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a473da9b-d8ad-42e0-944e-ec95977cd0c2",
                    "LayerId": "b23f728a-ed79-49ae-b13d-6e1c647c9839"
                }
            ]
        },
        {
            "id": "32937fbe-cd29-4c24-a371-ea78d6ab29d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a32ba94c-12ad-42a6-87f2-54dc30c92a41",
            "compositeImage": {
                "id": "ab0aa975-cd20-49d2-b8f1-2e316e333328",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32937fbe-cd29-4c24-a371-ea78d6ab29d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f3900fa-432d-4f38-ab72-1669d7b3d7d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32937fbe-cd29-4c24-a371-ea78d6ab29d8",
                    "LayerId": "b23f728a-ed79-49ae-b13d-6e1c647c9839"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "b23f728a-ed79-49ae-b13d-6e1c647c9839",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a32ba94c-12ad-42a6-87f2-54dc30c92a41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 39
}