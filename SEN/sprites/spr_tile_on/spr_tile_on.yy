{
    "id": "d71f6dc7-2d99-4bc5-a45c-5c5e11ae3d8b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_on",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b525720-4d1c-46b2-8379-e2ecc944dbcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d71f6dc7-2d99-4bc5-a45c-5c5e11ae3d8b",
            "compositeImage": {
                "id": "dcd0f8f7-b0de-4753-bc62-52a961b96533",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b525720-4d1c-46b2-8379-e2ecc944dbcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c9db480-847e-442b-8aca-bfaf5e2d2847",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b525720-4d1c-46b2-8379-e2ecc944dbcf",
                    "LayerId": "3cfa02a7-00bb-4074-a1bc-a02f384002b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3cfa02a7-00bb-4074-a1bc-a02f384002b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d71f6dc7-2d99-4bc5-a45c-5c5e11ae3d8b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}