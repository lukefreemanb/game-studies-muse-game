{
    "id": "83ae3731-e4ef-498f-a00e-a9667d3ab1ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tl_cave",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f069aeba-57be-4869-8cf6-803d24fd5a0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83ae3731-e4ef-498f-a00e-a9667d3ab1ff",
            "compositeImage": {
                "id": "b45261a1-bca4-46bd-b0b0-3fea6dfa312e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f069aeba-57be-4869-8cf6-803d24fd5a0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0010026e-d496-479f-935a-b4c144bbca62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f069aeba-57be-4869-8cf6-803d24fd5a0c",
                    "LayerId": "be5d8e22-32d3-4fb2-8e9f-2606a97d02e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "be5d8e22-32d3-4fb2-8e9f-2606a97d02e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83ae3731-e4ef-498f-a00e-a9667d3ab1ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}