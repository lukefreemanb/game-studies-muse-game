{
    "id": "a55024fb-7c69-4e07-bd7a-fdc44aee1f23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tree1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 77,
    "bbox_left": 15,
    "bbox_right": 63,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af94ac68-46db-4153-b493-a0d469a24acb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a55024fb-7c69-4e07-bd7a-fdc44aee1f23",
            "compositeImage": {
                "id": "e1b27d39-424f-432e-9a35-56eada8d2339",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af94ac68-46db-4153-b493-a0d469a24acb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6c67304-e59f-4b9c-b86c-003c2c7ca27e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af94ac68-46db-4153-b493-a0d469a24acb",
                    "LayerId": "1fe00d4b-2e4e-43e7-8239-47d2813f9ec4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "1fe00d4b-2e4e-43e7-8239-47d2813f9ec4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a55024fb-7c69-4e07-bd7a-fdc44aee1f23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 79
}