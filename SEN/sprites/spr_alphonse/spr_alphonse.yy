{
    "id": "df2c24fa-9553-4bf2-9e58-44642ba26a67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_alphonse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 6,
    "bbox_right": 39,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f129be93-9e51-4b3a-bdec-3e4dacdab018",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df2c24fa-9553-4bf2-9e58-44642ba26a67",
            "compositeImage": {
                "id": "53d37404-ec25-4590-91a1-24e4ff4dac97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f129be93-9e51-4b3a-bdec-3e4dacdab018",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1513b8e0-f6a2-4f85-b8b8-e69b6e1b1ea0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f129be93-9e51-4b3a-bdec-3e4dacdab018",
                    "LayerId": "0af4b86b-7f54-416a-8eff-8a0c80499d7d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "0af4b86b-7f54-416a-8eff-8a0c80499d7d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df2c24fa-9553-4bf2-9e58-44642ba26a67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 40
}