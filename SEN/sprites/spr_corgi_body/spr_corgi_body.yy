{
    "id": "3de39eff-4dc3-4fc3-bfd4-c60af2df5c08",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_corgi_body",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 5,
    "bbox_right": 43,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b5f49fd-2ec7-470f-ac80-d896763a94ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3de39eff-4dc3-4fc3-bfd4-c60af2df5c08",
            "compositeImage": {
                "id": "7f4009b3-c604-45c3-b3ee-7995c92f4e06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b5f49fd-2ec7-470f-ac80-d896763a94ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63d77e88-30ef-4e3f-a2ce-db84425c4e7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b5f49fd-2ec7-470f-ac80-d896763a94ec",
                    "LayerId": "140d5913-2d0a-4c70-902d-44df684ddf6d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "140d5913-2d0a-4c70-902d-44df684ddf6d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3de39eff-4dc3-4fc3-bfd4-c60af2df5c08",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}