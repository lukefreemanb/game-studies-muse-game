{
    "id": "a96217ba-2cb0-4592-9ba6-45c7070eae33",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 28,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9afaa85a-7af3-425a-8a32-0c71fe0fc763",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a96217ba-2cb0-4592-9ba6-45c7070eae33",
            "compositeImage": {
                "id": "cbc2c4f2-dce8-416b-bb6c-873114894e4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9afaa85a-7af3-425a-8a32-0c71fe0fc763",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca6877f2-f177-4d7c-9ebd-7af1c283f235",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9afaa85a-7af3-425a-8a32-0c71fe0fc763",
                    "LayerId": "a089f404-e5f5-4ce7-987a-4a1745dd32ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a089f404-e5f5-4ce7-987a-4a1745dd32ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a96217ba-2cb0-4592-9ba6-45c7070eae33",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}