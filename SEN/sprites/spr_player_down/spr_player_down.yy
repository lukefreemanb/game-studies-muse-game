{
    "id": "dfba731b-1e03-403c-88f8-376201d87c8d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 4,
    "bbox_right": 20,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63b7d83c-e97c-40d6-8d70-86288528d5be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfba731b-1e03-403c-88f8-376201d87c8d",
            "compositeImage": {
                "id": "a43e671d-f379-4191-92fd-30532b6915fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63b7d83c-e97c-40d6-8d70-86288528d5be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b923bd0c-ffd4-490f-8e1f-706d2c87fe3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63b7d83c-e97c-40d6-8d70-86288528d5be",
                    "LayerId": "24fcba20-aafc-4cce-bb08-fd3b254933a6"
                }
            ]
        },
        {
            "id": "8248b39c-2e8f-4c35-8ebe-29d3608cbc1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfba731b-1e03-403c-88f8-376201d87c8d",
            "compositeImage": {
                "id": "3263bc38-889e-423c-8724-b2f4c4cd6aff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8248b39c-2e8f-4c35-8ebe-29d3608cbc1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bdf4f80-0539-46bf-bf73-08337ec6fca0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8248b39c-2e8f-4c35-8ebe-29d3608cbc1e",
                    "LayerId": "24fcba20-aafc-4cce-bb08-fd3b254933a6"
                }
            ]
        },
        {
            "id": "bc0b2920-dda1-4b5b-9d7e-8722e5f105ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfba731b-1e03-403c-88f8-376201d87c8d",
            "compositeImage": {
                "id": "ad3e95ba-f73b-43c1-be97-8480d40f805b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc0b2920-dda1-4b5b-9d7e-8722e5f105ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3192fd3f-ba7a-408d-a7c0-9b15d003dd7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc0b2920-dda1-4b5b-9d7e-8722e5f105ca",
                    "LayerId": "24fcba20-aafc-4cce-bb08-fd3b254933a6"
                }
            ]
        },
        {
            "id": "8d04871c-aa16-42e3-86ba-f9a455f6229d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfba731b-1e03-403c-88f8-376201d87c8d",
            "compositeImage": {
                "id": "83489f94-0488-4abe-bb89-3084fe34b490",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d04871c-aa16-42e3-86ba-f9a455f6229d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45d4934f-a4a7-4f2f-adef-251e27112471",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d04871c-aa16-42e3-86ba-f9a455f6229d",
                    "LayerId": "24fcba20-aafc-4cce-bb08-fd3b254933a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "24fcba20-aafc-4cce-bb08-fd3b254933a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dfba731b-1e03-403c-88f8-376201d87c8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 39
}