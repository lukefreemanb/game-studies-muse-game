{
    "id": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_font_default_serif_centred",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "760673ab-e795-43b9-a04f-9c12578ae888",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "e011b706-bd9f-4365-8ab5-113e8ba76b18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "760673ab-e795-43b9-a04f-9c12578ae888",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57d2bc64-1cc0-4f8f-bd11-5e1360942e90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "760673ab-e795-43b9-a04f-9c12578ae888",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "c6bdc718-028f-4e6a-9c38-77cd7bb3469d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "c1b63a5b-cd81-4599-aa92-d58bbfd39b82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6bdc718-028f-4e6a-9c38-77cd7bb3469d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adc8fcc4-b4d4-40bf-b7cc-1c8421846bae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6bdc718-028f-4e6a-9c38-77cd7bb3469d",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "ad2bcfcc-9919-4507-8713-3168ec3b84ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "1dfb2497-4277-46da-84f4-991969dbee88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad2bcfcc-9919-4507-8713-3168ec3b84ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d5652f7-7870-4ac5-a604-14f7b81e03eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad2bcfcc-9919-4507-8713-3168ec3b84ec",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "0be52610-cd68-47ef-8800-798edf256ba1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "2c0f1c23-47dd-4fb7-a403-16ee978f2971",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0be52610-cd68-47ef-8800-798edf256ba1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "743a417e-1e42-4d3d-852b-04f67fe6db58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0be52610-cd68-47ef-8800-798edf256ba1",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "2314a2ed-b3d2-4336-9ff9-65038fdd6149",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "3c2c14bd-60b9-4bfc-af3e-4b33f1569314",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2314a2ed-b3d2-4336-9ff9-65038fdd6149",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c791d2a7-8a8c-4924-8034-3d6c9ffd9917",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2314a2ed-b3d2-4336-9ff9-65038fdd6149",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "245312c9-6c5b-4de7-a6cb-7ed0e5274fbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "f120c22b-33c5-427f-9539-4e76d893ef28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "245312c9-6c5b-4de7-a6cb-7ed0e5274fbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a3d3965-9231-4429-9c40-225814118670",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "245312c9-6c5b-4de7-a6cb-7ed0e5274fbd",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "3f0ce992-28d6-461f-8c6b-c14cc36f3826",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "b5b90c7a-1dd8-4bcb-a66c-0a7fcc848cef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f0ce992-28d6-461f-8c6b-c14cc36f3826",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4bcfb3d-ac5a-4515-92fe-d33a3a09c1ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f0ce992-28d6-461f-8c6b-c14cc36f3826",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "9574591c-4f36-40ed-8dcc-78db0d619546",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "891ba77f-e021-43c2-9ad9-03ebe390f226",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9574591c-4f36-40ed-8dcc-78db0d619546",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccd3940a-e376-4360-ba45-d7c53541a7b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9574591c-4f36-40ed-8dcc-78db0d619546",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "692cd802-339a-42c3-adfb-9048c05797ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "c4c70ddd-c163-40ce-98b1-6f5ac762e6d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "692cd802-339a-42c3-adfb-9048c05797ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c8480ad-ce43-496e-9551-f033e26548b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "692cd802-339a-42c3-adfb-9048c05797ab",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "5cbb9b75-9d35-4871-8a61-abd97f679704",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "f56dcb9a-cf54-4cf4-b340-a1d10fd09eda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cbb9b75-9d35-4871-8a61-abd97f679704",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3694ee65-c18f-4940-9500-c32e3253c3a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cbb9b75-9d35-4871-8a61-abd97f679704",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "d1ae70b6-43e6-45e8-b2e9-05d09b658bdc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "c7c7f724-18c1-4c45-b3cd-cbf454a6cfb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1ae70b6-43e6-45e8-b2e9-05d09b658bdc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "547d2410-1303-4a60-b148-a7ec934986af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1ae70b6-43e6-45e8-b2e9-05d09b658bdc",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "f5df0000-be50-45ee-852b-232aa9beeea6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "1634b9e7-94c8-44d0-b978-6ee58649f75a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5df0000-be50-45ee-852b-232aa9beeea6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b8bd373-a40c-474d-8c68-0e74fe8d9343",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5df0000-be50-45ee-852b-232aa9beeea6",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "3f3eb96c-bf75-4c16-88f8-bc7536b60650",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "664f6c09-ac6d-4fa2-ae60-93f29195142c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f3eb96c-bf75-4c16-88f8-bc7536b60650",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "388ea81d-03d9-46cf-a587-3ea1030f467b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f3eb96c-bf75-4c16-88f8-bc7536b60650",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "2fcf8a12-3f95-4252-83a3-734b85d03738",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "11326cbc-72a3-423d-890a-116f4c950c86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fcf8a12-3f95-4252-83a3-734b85d03738",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "610f745d-153c-4ce5-a9b8-7bd346e15c6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fcf8a12-3f95-4252-83a3-734b85d03738",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "d45d14a8-6540-4168-8179-2d13e1eb4730",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "511459f7-811f-492c-bbb8-e40e5eb16452",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d45d14a8-6540-4168-8179-2d13e1eb4730",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a143e1d2-1214-418b-9393-3279348a863d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d45d14a8-6540-4168-8179-2d13e1eb4730",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "2f2716ea-7a28-4706-9973-2da38b46b408",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "a8b8a9d8-4066-4c1d-8708-1383881b5b5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f2716ea-7a28-4706-9973-2da38b46b408",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d968b93-020c-4331-a3e1-54c7aaaf2983",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f2716ea-7a28-4706-9973-2da38b46b408",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "8d0d5b3e-c153-43dc-9f81-1a0933e07a60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "ce9be9b5-0972-4d0b-af91-602afcd92d7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d0d5b3e-c153-43dc-9f81-1a0933e07a60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3217dd44-7168-4797-9651-588c20fe9366",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d0d5b3e-c153-43dc-9f81-1a0933e07a60",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "c414963c-a220-4d78-8413-00485778f989",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "a7f59427-13e6-4881-9baa-94c1944c7add",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c414963c-a220-4d78-8413-00485778f989",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f6baf27-4aa4-4396-8bbd-bd693dedb0eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c414963c-a220-4d78-8413-00485778f989",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "e345f914-2284-4021-a015-6ba3b4ba8942",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "1ad08a55-f860-44ca-b65a-8d98bf839d9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e345f914-2284-4021-a015-6ba3b4ba8942",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e7bd36e-579b-4e1b-9374-5d9309a0e7ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e345f914-2284-4021-a015-6ba3b4ba8942",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "4f922f25-3ef8-4b3a-9659-a85c82ac7b98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "23beacee-e7f0-4ba5-8a3e-5e45697307c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f922f25-3ef8-4b3a-9659-a85c82ac7b98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d455b2eb-c112-4fe6-a218-220a89402f90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f922f25-3ef8-4b3a-9659-a85c82ac7b98",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "dc6697b7-47d2-460f-8809-fb88032f7063",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "2098d411-8f73-4327-8b58-4f094c594092",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc6697b7-47d2-460f-8809-fb88032f7063",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "360cbba7-8441-4af9-a8af-f339f83e5e05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc6697b7-47d2-460f-8809-fb88032f7063",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "25300d72-3e40-4714-854c-59ffc3e724f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "523cc235-2bf9-4c65-9964-6e2cef366dac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25300d72-3e40-4714-854c-59ffc3e724f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9638ec37-f7ec-4458-8d97-bf20753b8aeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25300d72-3e40-4714-854c-59ffc3e724f1",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "17520015-b72b-4a83-85c3-42217dc7f0d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "4e31f140-733e-456e-b728-4ff3d10aa9b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17520015-b72b-4a83-85c3-42217dc7f0d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29798948-9c1e-48f3-b46b-39af5c2e9632",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17520015-b72b-4a83-85c3-42217dc7f0d1",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "add0e76c-7d7a-49f6-9179-a05da7d75550",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "a479701a-57e4-45e6-86e1-16fa4ccd1bf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "add0e76c-7d7a-49f6-9179-a05da7d75550",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fbb9eed-3bc0-49a6-b73e-41ee23b6bfc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "add0e76c-7d7a-49f6-9179-a05da7d75550",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "4ae915d4-e3bd-4fb6-b6aa-ac8a049e911e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "04d73eaf-7748-42b4-9d86-541a736f4bd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ae915d4-e3bd-4fb6-b6aa-ac8a049e911e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4860634a-6bf7-4fc8-9db8-47bed3208118",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ae915d4-e3bd-4fb6-b6aa-ac8a049e911e",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "7065c979-e2f4-4d79-baed-fed352ee6565",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "a42c60f1-cc56-4b61-a46c-a4d60f84d27d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7065c979-e2f4-4d79-baed-fed352ee6565",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f27c8938-1b6f-46f5-8642-ffdf3b918288",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7065c979-e2f4-4d79-baed-fed352ee6565",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "178172f7-b8dd-41dd-b05f-ccd30fc2e0d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "c02ec759-b25c-4a00-85dc-b1baaa47b1b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "178172f7-b8dd-41dd-b05f-ccd30fc2e0d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af4bc8be-4514-4e45-bad5-100f5711fbcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "178172f7-b8dd-41dd-b05f-ccd30fc2e0d2",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "f3076483-7261-4a7a-b50e-7332ba7ce429",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "1498abd0-f2c8-4f76-b7bc-edf80f9e747c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3076483-7261-4a7a-b50e-7332ba7ce429",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fb7d4b5-0af6-4941-968b-b4f2a7fb8865",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3076483-7261-4a7a-b50e-7332ba7ce429",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "511f9dc9-adb9-4aad-bb3c-db5b5653e48d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "958cb94f-557d-4980-bd3e-d7fb857f323c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "511f9dc9-adb9-4aad-bb3c-db5b5653e48d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1695177-1e0d-41c6-8677-e43be51daeb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "511f9dc9-adb9-4aad-bb3c-db5b5653e48d",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "ed043929-bae0-4b57-9b7a-44076b152e79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "8f491eb4-43dc-47ef-826a-2a2d5af0e339",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed043929-bae0-4b57-9b7a-44076b152e79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ad46888-c694-4469-9963-93762acbcbd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed043929-bae0-4b57-9b7a-44076b152e79",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "896eb8a2-2aaf-48c5-9868-717b6efbc1d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "d7dbdf75-a7d0-4dca-8bc8-21da204398ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "896eb8a2-2aaf-48c5-9868-717b6efbc1d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d075bcec-79a0-4910-a42b-a30b51039d14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "896eb8a2-2aaf-48c5-9868-717b6efbc1d4",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "57901703-c261-4642-be7c-bfaf6083702f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "f1d49224-1bf3-4fa1-8de2-a23dfa171122",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57901703-c261-4642-be7c-bfaf6083702f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfc387bc-c634-49b9-a58e-27993da61da1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57901703-c261-4642-be7c-bfaf6083702f",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "2c416ad0-dc6a-42d3-8a20-30bd29a94c18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "46609f18-96bc-4bc9-a892-b224df22c741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c416ad0-dc6a-42d3-8a20-30bd29a94c18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eaaa61a-6448-46ab-963a-17f4a0fff463",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c416ad0-dc6a-42d3-8a20-30bd29a94c18",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "69816716-c5ac-4bac-999e-8d41ecec64a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "94ba9792-4dbb-4f5c-89fe-b591dfef0d7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69816716-c5ac-4bac-999e-8d41ecec64a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4b78c72-0209-4769-b908-8f57e2b5bd53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69816716-c5ac-4bac-999e-8d41ecec64a3",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "7d8bd305-4b71-4d5b-b258-3ee60f0c3068",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "b13f9ffc-ebbc-4205-8e5d-0050738ca169",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d8bd305-4b71-4d5b-b258-3ee60f0c3068",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43d3a34f-c883-4363-903c-08242bd046d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d8bd305-4b71-4d5b-b258-3ee60f0c3068",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "c2d9cfb4-4c53-4d99-ba43-918b40b17b11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "28fd603f-aae7-41bd-93a4-6036275c55ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2d9cfb4-4c53-4d99-ba43-918b40b17b11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b915b21b-5372-49f8-9546-015b1d4def8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2d9cfb4-4c53-4d99-ba43-918b40b17b11",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "4519e54b-7f00-4e05-9b5c-ae60122aaaa4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "4644bd49-f323-4097-8a16-ee97bf99c1bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4519e54b-7f00-4e05-9b5c-ae60122aaaa4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad2cd1a2-bb2c-4048-8e75-1e929eba3ef9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4519e54b-7f00-4e05-9b5c-ae60122aaaa4",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "168ba50c-f2a2-461b-8e84-e0a693c9aac7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "c1793bc4-c38c-4d7e-b438-c6eda90bd5f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "168ba50c-f2a2-461b-8e84-e0a693c9aac7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d500b368-4896-4f0b-85ec-00f62703c6b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "168ba50c-f2a2-461b-8e84-e0a693c9aac7",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "709bee75-1e15-4c1f-91b9-28cb8e85fa03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "aaf46353-e4b3-4d79-80f0-2881bebdc594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "709bee75-1e15-4c1f-91b9-28cb8e85fa03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77134141-4103-4d04-a3fc-1c7537a579ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "709bee75-1e15-4c1f-91b9-28cb8e85fa03",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "65aa1599-c67d-4cae-9fa2-7069672ce166",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "6a657aca-6df8-4f01-8d1e-c57981d511a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65aa1599-c67d-4cae-9fa2-7069672ce166",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2aaccf7d-a84f-400c-9341-c7c31a83b7be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65aa1599-c67d-4cae-9fa2-7069672ce166",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "501009f5-e069-4c5a-ab9f-681eef0e580e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "bcdf8fb2-91d7-44d3-a690-65fdacf081da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "501009f5-e069-4c5a-ab9f-681eef0e580e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe263b0c-5d2a-4f61-ac5f-4f6948dfbdd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "501009f5-e069-4c5a-ab9f-681eef0e580e",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "97bba8bb-7233-467d-bf86-66b8b1965996",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "0bee449c-aff3-4c35-884c-3367384d2707",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97bba8bb-7233-467d-bf86-66b8b1965996",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8945367-5936-434b-9a89-2d157181b2f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97bba8bb-7233-467d-bf86-66b8b1965996",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "95371c0a-cc13-4913-b56c-3d35996ed2b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "9c927469-d037-4e2c-a131-01f0ea73961e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95371c0a-cc13-4913-b56c-3d35996ed2b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0952615d-90ee-49d1-9a67-36f1dac2c44c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95371c0a-cc13-4913-b56c-3d35996ed2b8",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "fbb5af68-b57f-4000-9cfe-cea10cd61ea5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "9196daf9-0790-49d4-9112-3aba61a0aa09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbb5af68-b57f-4000-9cfe-cea10cd61ea5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28366649-3045-4d21-ad7a-9dbad51f22f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbb5af68-b57f-4000-9cfe-cea10cd61ea5",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "aca3fcef-965b-4ab4-97d0-ad9a29faee5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "22c719d6-28b4-4cf4-aa07-b9f62b79ca48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aca3fcef-965b-4ab4-97d0-ad9a29faee5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef9b7edc-816e-47a9-91f1-6e35ed1e02e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aca3fcef-965b-4ab4-97d0-ad9a29faee5f",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "d8f003e2-f49b-4c2f-845b-ebe9c5a9f695",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "95118964-56e0-4f2b-899f-bcd34e48bedf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8f003e2-f49b-4c2f-845b-ebe9c5a9f695",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "651cee33-8045-4ad4-928a-f91a0877eea3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8f003e2-f49b-4c2f-845b-ebe9c5a9f695",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "27396a4f-a2d5-428a-b47a-2bad86961747",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "f830d15f-4e2c-439e-a14f-88b00f663005",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27396a4f-a2d5-428a-b47a-2bad86961747",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6e34ee9-f514-4657-a27f-51bd8f9cd210",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27396a4f-a2d5-428a-b47a-2bad86961747",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "353985cf-636a-46ef-b2b7-b45daa9297ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "a3107fef-7593-459f-a47b-c12805cdd989",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "353985cf-636a-46ef-b2b7-b45daa9297ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8fa2cfa-3c1b-4afd-bd93-8a04d4f91b91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "353985cf-636a-46ef-b2b7-b45daa9297ee",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "ec6e01ac-3288-4a69-ae4e-5d03a5f077cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "f877964f-9a32-42a3-9cfe-23bb2f324a80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec6e01ac-3288-4a69-ae4e-5d03a5f077cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b80301b9-caa2-4efa-a0cf-5a18b9281292",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec6e01ac-3288-4a69-ae4e-5d03a5f077cb",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "c4ea42b5-8424-4936-84ef-d464238cd8c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "41016bd7-5922-46c1-b1cb-fe75fefcc662",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4ea42b5-8424-4936-84ef-d464238cd8c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46d1926d-ecc4-4cbc-876f-8ce82e920218",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4ea42b5-8424-4936-84ef-d464238cd8c2",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "e5c508c7-99ca-452d-9bef-1d95c80bf202",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "257cfdb7-c827-4956-aafe-4defce35619e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5c508c7-99ca-452d-9bef-1d95c80bf202",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ee94a61-73ab-49f2-9ef0-19da6dfc26d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5c508c7-99ca-452d-9bef-1d95c80bf202",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "ce064d52-f062-4213-8669-77b1dc212f92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "e3e62f28-113f-440b-8b33-66d9d55a46cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce064d52-f062-4213-8669-77b1dc212f92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7c32a0c-bbb3-4104-9f5b-5e025bc9f5b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce064d52-f062-4213-8669-77b1dc212f92",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "5d4892d2-7556-4930-bf49-75202c71b155",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "8cb5264c-77bb-46fc-97dc-9bc2be125839",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d4892d2-7556-4930-bf49-75202c71b155",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00e3c696-22cb-417a-a088-4c341370a015",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d4892d2-7556-4930-bf49-75202c71b155",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "425f8871-57cb-444e-9cb9-b08990f9b73b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "80e5b666-e8e0-4707-8a5c-5d7febcdea3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "425f8871-57cb-444e-9cb9-b08990f9b73b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95f5155d-dcfd-49ce-b49f-83c630d4aa55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "425f8871-57cb-444e-9cb9-b08990f9b73b",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "e91662da-36fa-4611-b5f6-b23676ce4dca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "3a8c5836-36de-4dc4-ac4f-e9e7cdc3f4ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e91662da-36fa-4611-b5f6-b23676ce4dca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e302c89-29ef-4707-a7e2-c36265fd4665",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e91662da-36fa-4611-b5f6-b23676ce4dca",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "afd4385d-f979-4d1e-beb9-8234da7baecd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "906a93e8-a331-4587-bd9b-8d63202481e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afd4385d-f979-4d1e-beb9-8234da7baecd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0b83103-c1c8-4e61-aed3-a21ab00f0d0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afd4385d-f979-4d1e-beb9-8234da7baecd",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "bcb2c3ae-367a-4168-bbe1-4170a69571c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "f268c0f6-9c4d-4bbd-ae4d-16cde24fa6ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcb2c3ae-367a-4168-bbe1-4170a69571c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52e4acf9-3dd2-4f7a-80f7-0cd03dc6d530",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcb2c3ae-367a-4168-bbe1-4170a69571c6",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "82620e54-f2ac-48e4-a0d7-d460bff43b9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "3882896c-8c8c-4565-a9bb-25c55ec4f855",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82620e54-f2ac-48e4-a0d7-d460bff43b9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d9a279e-3b04-4bfc-8511-6dad051d0a3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82620e54-f2ac-48e4-a0d7-d460bff43b9b",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "f4764fb5-c8b9-47ea-a838-641589f13564",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "2058cc20-aadc-41b8-a570-6ca0d2ba610c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4764fb5-c8b9-47ea-a838-641589f13564",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55931f7c-302f-438e-a1b8-6e3700d7da0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4764fb5-c8b9-47ea-a838-641589f13564",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "a4c433a2-56b5-4bbc-a45e-a86a0c4ea6ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "ff645de6-a7c0-4b4b-9459-9a80a8113c3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4c433a2-56b5-4bbc-a45e-a86a0c4ea6ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f37af76b-8432-4289-8422-27a09700b5f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4c433a2-56b5-4bbc-a45e-a86a0c4ea6ed",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "4b5facee-02fa-4f54-aa4c-c47c564bdd0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "0eeb65ac-a6ed-4fdb-9bad-9912fb950ec4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b5facee-02fa-4f54-aa4c-c47c564bdd0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9afa324-467a-4d35-943b-bd4fd1f37ced",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b5facee-02fa-4f54-aa4c-c47c564bdd0a",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "d56f120b-f230-418b-8c16-040b190b4fad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "4f6962a6-bec6-41e5-b0f7-6d80dc748ea7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d56f120b-f230-418b-8c16-040b190b4fad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe0ee201-9829-41d6-afec-e5a1051bcdca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d56f120b-f230-418b-8c16-040b190b4fad",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "d7d099b6-0a65-4dfb-989b-b7d50f5aaf37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "c5899cc5-2964-44d6-835f-33e5cadd37eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7d099b6-0a65-4dfb-989b-b7d50f5aaf37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e139030-0219-487c-8fd4-a4f135ee95ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7d099b6-0a65-4dfb-989b-b7d50f5aaf37",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "0c924d64-98d9-45fd-8f3b-d40481771c35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "cbda0810-f339-440e-8aaa-5d72fcf86cfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c924d64-98d9-45fd-8f3b-d40481771c35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7edcfaa-90c9-473e-879e-65eb9aefbd0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c924d64-98d9-45fd-8f3b-d40481771c35",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "c2a9f9a1-d5b9-4be2-aec5-ac36ba81d203",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "4742f7cc-f9c3-4ec2-aeee-db490ccf9cd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2a9f9a1-d5b9-4be2-aec5-ac36ba81d203",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0bf3393-d2c1-4f06-9fc1-041c2863c619",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2a9f9a1-d5b9-4be2-aec5-ac36ba81d203",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "8f759ef3-fd3e-47db-b76a-ccbd70ff3f32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "46e6ad3d-7d18-4912-b97d-d980cac7ff25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f759ef3-fd3e-47db-b76a-ccbd70ff3f32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c80d1d63-a264-4297-a793-a62543fd219b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f759ef3-fd3e-47db-b76a-ccbd70ff3f32",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "488d2db5-574c-4cbc-8928-d48c0538b9ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "89942c0f-4269-4dce-b178-2ba855e39936",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "488d2db5-574c-4cbc-8928-d48c0538b9ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27abd379-082d-43cc-99a7-38477736085b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "488d2db5-574c-4cbc-8928-d48c0538b9ef",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "2d39cc04-0fa8-4c97-8860-d32fabc3511b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "5b04b9bb-2e57-4c87-8e20-dab0e65bdfb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d39cc04-0fa8-4c97-8860-d32fabc3511b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d23346d7-3dcb-40f2-a104-c1eb865b634b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d39cc04-0fa8-4c97-8860-d32fabc3511b",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "75ff79f2-6edf-46df-b177-6d6cb9cd0e6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "b6b99562-520c-42e0-baa9-693f5162ebfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75ff79f2-6edf-46df-b177-6d6cb9cd0e6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6edd046c-260e-4cc8-9971-711e3fcf0046",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75ff79f2-6edf-46df-b177-6d6cb9cd0e6f",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "423e215e-bad1-4d02-a09e-f8fb5c18d2b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "cb968da1-cca9-4a1f-b2cc-94aff4aad97a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "423e215e-bad1-4d02-a09e-f8fb5c18d2b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb6a8025-77a6-4431-9fa9-806feb001c70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "423e215e-bad1-4d02-a09e-f8fb5c18d2b6",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "2d39acda-e877-44a4-9975-4a907a8a9273",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "30b488d5-4e64-4157-b843-f845988597b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d39acda-e877-44a4-9975-4a907a8a9273",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44f83690-b26e-4ad4-baf3-08e0a0957809",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d39acda-e877-44a4-9975-4a907a8a9273",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "cfaf36e3-2b95-4ee4-99af-fbd7a1c5a101",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "5f404307-bc88-41dd-8505-373b1bb69caa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfaf36e3-2b95-4ee4-99af-fbd7a1c5a101",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24e82a5c-2bdf-435a-bd8d-9e95e1f835f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfaf36e3-2b95-4ee4-99af-fbd7a1c5a101",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "36b86473-4c35-461d-bb86-103d3593dca9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "30c08548-28db-4275-95a9-bc82a7e8a756",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36b86473-4c35-461d-bb86-103d3593dca9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7051ac93-8d0b-48bb-bf34-ff6baa5db44a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36b86473-4c35-461d-bb86-103d3593dca9",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "1f925b20-18f9-4c00-84c4-b28828e0a064",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "d1cca880-eedf-4e22-a80f-4fafe375d324",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f925b20-18f9-4c00-84c4-b28828e0a064",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63343159-bd54-4d4a-addb-b9397003c090",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f925b20-18f9-4c00-84c4-b28828e0a064",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "a5ecd80d-f7ac-4a59-b9f1-f303d5903946",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "40edaa10-5741-4a84-99cb-0beb42e1364a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5ecd80d-f7ac-4a59-b9f1-f303d5903946",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cd6d308-57a6-4dce-ab9b-f06e64c750ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5ecd80d-f7ac-4a59-b9f1-f303d5903946",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "59b9d6f5-af6c-4dcc-b4b0-d4e6a0a9c178",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "22e979f2-f5d9-40ec-8a5f-a2eddbf9fa69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59b9d6f5-af6c-4dcc-b4b0-d4e6a0a9c178",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "584d3b87-fedd-4d8e-a324-e9e9452eb38d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59b9d6f5-af6c-4dcc-b4b0-d4e6a0a9c178",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "899adfe6-f56b-4515-aec2-1f40cacd9e65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "836016cb-44ff-4ac2-9b61-9ad5098031ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "899adfe6-f56b-4515-aec2-1f40cacd9e65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffc633b7-7669-4789-903e-adfd90ce9569",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "899adfe6-f56b-4515-aec2-1f40cacd9e65",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "2cd94c15-b14d-4f49-8ed6-aeff286137a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "bc819ee2-3d26-4cba-9e3c-4ce570e9e94e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cd94c15-b14d-4f49-8ed6-aeff286137a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "533c9c9c-207d-47f6-89d3-f99cc5c24a4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cd94c15-b14d-4f49-8ed6-aeff286137a8",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "2012d50b-9b9d-4e0e-a595-43b8a3c1b886",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "7f09a5c8-08b3-4423-b1f2-daa318774a0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2012d50b-9b9d-4e0e-a595-43b8a3c1b886",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "deb9efc3-dbd8-47b5-8eba-79b75f7c8af8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2012d50b-9b9d-4e0e-a595-43b8a3c1b886",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "d3048d18-6652-47be-8c31-689f7c5a03e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "94d1430f-7f31-4d58-a72f-9d099f86640c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3048d18-6652-47be-8c31-689f7c5a03e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d21b32b-ce64-44c4-ab50-5043b800974c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3048d18-6652-47be-8c31-689f7c5a03e5",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "59bc66dc-f5d9-4eb3-ab96-f4f7479db522",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "6b1c6ec3-9a75-4968-96d5-b388760e39e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59bc66dc-f5d9-4eb3-ab96-f4f7479db522",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65816939-20d7-4c4c-a0c3-e3abfb1c14b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59bc66dc-f5d9-4eb3-ab96-f4f7479db522",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "9f08db65-1994-43b9-9ae1-368cd6299d49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "0677090b-0d1d-4012-a3c9-615fa8c764f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f08db65-1994-43b9-9ae1-368cd6299d49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1280139f-969d-4fdb-9e87-0e06b83d5c2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f08db65-1994-43b9-9ae1-368cd6299d49",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "9d774f55-cefd-42e5-8863-6d8f44b6260c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "e106460c-375b-4e54-93ea-4d6924a2d4a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d774f55-cefd-42e5-8863-6d8f44b6260c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10e0308b-4f9c-47d1-98df-0d5ea73d05ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d774f55-cefd-42e5-8863-6d8f44b6260c",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "c11fc3d4-fb40-47b8-94fe-0339d63e8130",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "7ec36381-1e69-47cb-bd46-9a1ca4499cbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c11fc3d4-fb40-47b8-94fe-0339d63e8130",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f534f5d-d472-4289-9ad2-cbdfdb164b86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c11fc3d4-fb40-47b8-94fe-0339d63e8130",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "c387c490-abb5-404b-a6a8-b174fffc3fe6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "9325e4f8-ffa2-4f31-977f-5e8f0d69647d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c387c490-abb5-404b-a6a8-b174fffc3fe6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14827003-82b9-438b-93f5-f4a964450790",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c387c490-abb5-404b-a6a8-b174fffc3fe6",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "a71545c3-1a26-4aae-a4d7-b2aa18b41f44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "60b6e7d7-5d8e-4814-9d65-4324f6813ded",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a71545c3-1a26-4aae-a4d7-b2aa18b41f44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed286672-9c8d-4c86-96d1-29c31a42dcf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a71545c3-1a26-4aae-a4d7-b2aa18b41f44",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "f720535b-c828-4842-b6f8-31ddc47edfc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "9b92ce32-37f2-4dbd-a905-0bff4d15154d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f720535b-c828-4842-b6f8-31ddc47edfc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e9a91af-c63b-485d-a735-60502f232500",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f720535b-c828-4842-b6f8-31ddc47edfc2",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "0b54de83-979b-43ff-a160-f73068925c34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "4939e745-673b-4a4a-a5ae-34c9b816c02c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b54de83-979b-43ff-a160-f73068925c34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78c0673f-5138-4ec8-a746-e1851e742705",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b54de83-979b-43ff-a160-f73068925c34",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "aac29b67-1f90-4f2c-ac75-e597a5b097c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "72ad91a1-787c-4cb4-a3a5-93f4523cb554",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aac29b67-1f90-4f2c-ac75-e597a5b097c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b952ae4f-5e81-4cdc-8756-724ce5b0137a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aac29b67-1f90-4f2c-ac75-e597a5b097c1",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "baa75900-ffaf-4c87-a8b1-5a758dcee3e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "5d24aa8e-9173-493f-85cf-7f32898b10b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baa75900-ffaf-4c87-a8b1-5a758dcee3e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a16f6242-f677-4238-b89a-627278845ee3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baa75900-ffaf-4c87-a8b1-5a758dcee3e0",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "1c2668d7-37e5-48a4-a473-355f34a606ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "976e3c0c-509c-491e-a3fa-1fc116bf3190",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c2668d7-37e5-48a4-a473-355f34a606ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7372d943-20e8-4379-bb5e-88e5b8bd09e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c2668d7-37e5-48a4-a473-355f34a606ed",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "7549ff5c-3d88-41c0-b71a-16d0c2ceb6bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "fe2727a6-fc96-4dd2-8aff-7b694ee23bff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7549ff5c-3d88-41c0-b71a-16d0c2ceb6bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcc67ff4-e831-433a-89c1-8969d3af8827",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7549ff5c-3d88-41c0-b71a-16d0c2ceb6bb",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "73a593b8-ee42-4d3d-89f5-d65d4cde6efe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "d6e6cacf-8189-46b6-8eb0-20f93385912e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73a593b8-ee42-4d3d-89f5-d65d4cde6efe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "366d0616-a350-4c74-8e95-100c169a8d2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73a593b8-ee42-4d3d-89f5-d65d4cde6efe",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "c88f41e5-46e0-4541-96b0-57c3e8201e58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "b9972387-7085-4e4c-9bb4-542c0203489e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c88f41e5-46e0-4541-96b0-57c3e8201e58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7dc62f4-4818-4a22-bcae-428bab3e3b56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c88f41e5-46e0-4541-96b0-57c3e8201e58",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "e8b9c7ce-bee1-4c58-bfff-0a154bd6b3d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "fb3aa225-44ce-4d4e-a3f3-6ba97a33b137",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8b9c7ce-bee1-4c58-bfff-0a154bd6b3d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "564c71c1-cfb0-4b28-9d39-cfe4e0ca3c9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8b9c7ce-bee1-4c58-bfff-0a154bd6b3d8",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "6b8649b8-6595-4771-9129-36eee3deb943",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "899b1098-0e68-45a5-baa8-9927482a390a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b8649b8-6595-4771-9129-36eee3deb943",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cafc0885-a614-4f4e-b6b9-321a53facb9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b8649b8-6595-4771-9129-36eee3deb943",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "eaaf4274-7b99-486e-bddc-f633257c373e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "36375f70-6ad6-4db6-a7ec-64836e3292d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaaf4274-7b99-486e-bddc-f633257c373e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62db283d-0011-4df0-b27a-10619f3962ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaaf4274-7b99-486e-bddc-f633257c373e",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "7edabfd5-8721-4566-acd6-26f8513f4b4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "998626d7-c558-4233-845c-4858cee9b371",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7edabfd5-8721-4566-acd6-26f8513f4b4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28987ac3-a8d6-47c7-b760-1b2c195ea7d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7edabfd5-8721-4566-acd6-26f8513f4b4e",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "3921de75-45e4-4834-b295-a82db8dd4780",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "78dfc6ad-ddf3-4188-aaad-4463b0c93152",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3921de75-45e4-4834-b295-a82db8dd4780",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f48fe80a-042b-4ac3-8877-f9f342056769",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3921de75-45e4-4834-b295-a82db8dd4780",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "60bd7e8c-9e9e-4c0e-bd36-7b31ca77cf90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "40fba592-2b3d-47a9-9731-aac8ade3711d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60bd7e8c-9e9e-4c0e-bd36-7b31ca77cf90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73b9b81c-dc60-43bd-8d33-5088c1e1be5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60bd7e8c-9e9e-4c0e-bd36-7b31ca77cf90",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "df2a3ebf-c55a-4c8d-8805-fa5f795ec48f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "cffc77af-3932-4c87-add3-ecc2cee32f16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df2a3ebf-c55a-4c8d-8805-fa5f795ec48f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a66c965-5f2b-4ded-bec0-1003a0cb15ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df2a3ebf-c55a-4c8d-8805-fa5f795ec48f",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "b9bad5b8-a333-4a2c-8bf5-51c188271b1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "2a689e0a-bef8-47cf-a7fe-4a2617a9caf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9bad5b8-a333-4a2c-8bf5-51c188271b1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23a3402d-0636-4aa0-972d-d109e5bc7fc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9bad5b8-a333-4a2c-8bf5-51c188271b1c",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "9c1183c9-be9a-4b6b-9fb0-c03c7e9ef504",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "0226e3d1-ae4f-44f3-b820-971b313cb464",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c1183c9-be9a-4b6b-9fb0-c03c7e9ef504",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99a63551-07ac-4491-8297-0f44d1bd9739",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c1183c9-be9a-4b6b-9fb0-c03c7e9ef504",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "97bb3abc-6624-426d-b09e-ea7efe84b221",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "bf14f9a5-a987-40f9-b7ae-1ea1f600be29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97bb3abc-6624-426d-b09e-ea7efe84b221",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9c360e9-9957-4787-ad69-897231e4ffdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97bb3abc-6624-426d-b09e-ea7efe84b221",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "13ca7046-899d-4302-ad14-cb45ef87b323",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "b5a97aa1-1758-4281-ab7b-92a3dce7860e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13ca7046-899d-4302-ad14-cb45ef87b323",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3113c2d7-fed3-4e19-9956-7e49d1a15895",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13ca7046-899d-4302-ad14-cb45ef87b323",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "0523a0d3-9074-485e-9a6c-aa3f94e29395",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "f9ef259b-993b-4aac-b168-5e9d2e5f3e22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0523a0d3-9074-485e-9a6c-aa3f94e29395",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a4ee8aa-f4cc-461a-a462-0cd02be5f5f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0523a0d3-9074-485e-9a6c-aa3f94e29395",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "892799d7-0058-4b4f-b218-7f9533e157dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "6bac998f-3c5e-4907-a3cc-02e4dfeb42ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "892799d7-0058-4b4f-b218-7f9533e157dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0386ed10-c638-4b20-951a-c9a35f6d1211",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "892799d7-0058-4b4f-b218-7f9533e157dd",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "664ba111-1e02-4f73-b492-0758a69a4a79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "00a1369c-f37b-4a0e-8338-911b9dc2ce14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "664ba111-1e02-4f73-b492-0758a69a4a79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8282a6b-b0f3-4e44-a3cd-f3ca273f7da9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "664ba111-1e02-4f73-b492-0758a69a4a79",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "67a44eaa-9884-45bc-bc71-f1de1d7d2a58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "0a99f846-4ad5-42b5-b8c5-885403d19bba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67a44eaa-9884-45bc-bc71-f1de1d7d2a58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99144641-32ae-4e88-a818-f70727734593",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67a44eaa-9884-45bc-bc71-f1de1d7d2a58",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "de84c4a9-ff11-442e-8654-5da9770b1c10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "cf4c2702-7feb-4cdb-a98b-0d7e6e93437e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de84c4a9-ff11-442e-8654-5da9770b1c10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b04697bb-3104-4ffe-bb7d-725db571ef5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de84c4a9-ff11-442e-8654-5da9770b1c10",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "a9008190-6e83-41be-a0aa-29a9c9ac4727",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "a5ea7b4d-a79b-407e-b7f3-a5b66a698c00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9008190-6e83-41be-a0aa-29a9c9ac4727",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e0104c4-88f8-4cfd-9509-1dfd79092272",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9008190-6e83-41be-a0aa-29a9c9ac4727",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        },
        {
            "id": "2acd1a62-405c-4641-a40b-167332923e5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "compositeImage": {
                "id": "524624b7-ad1d-4315-8f9c-4de9b38e658e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2acd1a62-405c-4641-a40b-167332923e5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e98dc3b-122d-4dc1-8264-a65f2d47d69f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2acd1a62-405c-4641-a40b-167332923e5d",
                    "LayerId": "6902a099-7ee5-4124-96e2-552effb4a375"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "6902a099-7ee5-4124-96e2-552effb4a375",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "460e3c4e-eaa2-4e1f-a6d7-a0bf3dde39a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 5
}