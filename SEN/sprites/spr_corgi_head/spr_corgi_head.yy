{
    "id": "ae60d4cc-1588-4a52-9286-4545dc3771ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_corgi_head",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 6,
    "bbox_right": 41,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad75c099-c0d2-4fb0-afe9-1ada2b554dce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae60d4cc-1588-4a52-9286-4545dc3771ec",
            "compositeImage": {
                "id": "8295551d-71c4-4216-8454-a5292549a36f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad75c099-c0d2-4fb0-afe9-1ada2b554dce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdf08f29-26cf-4929-9418-a3883a99621c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad75c099-c0d2-4fb0-afe9-1ada2b554dce",
                    "LayerId": "363da2b9-eb2e-4ffc-b54f-6f1bdd86472c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "363da2b9-eb2e-4ffc-b54f-6f1bdd86472c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae60d4cc-1588-4a52-9286-4545dc3771ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 35
}