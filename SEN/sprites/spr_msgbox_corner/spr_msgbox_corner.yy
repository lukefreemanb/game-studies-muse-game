{
    "id": "ef9e1c04-bf12-482b-b99f-9c65fc89f623",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msgbox_corner",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7fef80f-bd10-4f28-81f8-5164ce563ae7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef9e1c04-bf12-482b-b99f-9c65fc89f623",
            "compositeImage": {
                "id": "7c21ac78-0eb9-4f39-87d8-50224c94ddf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7fef80f-bd10-4f28-81f8-5164ce563ae7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86e5f170-073a-40a6-bda8-a0413401bc7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7fef80f-bd10-4f28-81f8-5164ce563ae7",
                    "LayerId": "a0ba4197-3031-4cb5-8d5b-29876e83b30f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "a0ba4197-3031-4cb5-8d5b-29876e83b30f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef9e1c04-bf12-482b-b99f-9c65fc89f623",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 0,
    "yorig": 0
}