{
    "id": "96fefac7-17a0-4d60-a72d-4aaeb8fc20cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_test_image",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 999,
    "bbox_left": 0,
    "bbox_right": 999,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c36da6e8-2d1c-4569-8104-fe24c5df2eee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96fefac7-17a0-4d60-a72d-4aaeb8fc20cb",
            "compositeImage": {
                "id": "57f96cd8-299e-4afe-8160-bfb59ba990f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c36da6e8-2d1c-4569-8104-fe24c5df2eee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5113ffb6-d4e9-4499-b53d-fdfde20c4db9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c36da6e8-2d1c-4569-8104-fe24c5df2eee",
                    "LayerId": "7f5d66f0-6724-4b89-86fc-052dd3ec270f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1000,
    "layers": [
        {
            "id": "7f5d66f0-6724-4b89-86fc-052dd3ec270f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96fefac7-17a0-4d60-a72d-4aaeb8fc20cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1000,
    "xorig": 0,
    "yorig": 0
}