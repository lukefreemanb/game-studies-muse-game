{
    "id": "392085f5-0dea-4aa5-83a6-386789f7f6bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tree_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 88,
    "bbox_left": 28,
    "bbox_right": 36,
    "bbox_top": 32,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2768d550-2952-42f0-be3a-b89571ccad0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392085f5-0dea-4aa5-83a6-386789f7f6bf",
            "compositeImage": {
                "id": "1772d9de-98dc-48b3-b179-be3ebc3da87e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2768d550-2952-42f0-be3a-b89571ccad0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fea955d3-3a8c-421f-a514-99b69c9128ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2768d550-2952-42f0-be3a-b89571ccad0b",
                    "LayerId": "b6440c65-6d61-4079-ab61-fbe040534082"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "b6440c65-6d61-4079-ab61-fbe040534082",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "392085f5-0dea-4aa5-83a6-386789f7f6bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 95
}