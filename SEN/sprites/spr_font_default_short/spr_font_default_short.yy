{
    "id": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_font_default_short",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21d69dbe-8404-495f-9d05-12043eb494b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "e4ff4452-36bb-4702-b746-c6c000e94e27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21d69dbe-8404-495f-9d05-12043eb494b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "514d694a-3bce-49be-9b52-d6a495170df5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21d69dbe-8404-495f-9d05-12043eb494b8",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "297833f9-ec17-4103-9963-c5e1b0a183ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "2d3a5f13-f7a1-4c49-9f3f-e5757a37f920",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "297833f9-ec17-4103-9963-c5e1b0a183ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "917b8d77-ac5b-4503-bfe3-85f4a717d66a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "297833f9-ec17-4103-9963-c5e1b0a183ac",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "be407d96-7258-4621-9433-0abb30f55135",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "1cd22ca4-99c0-4ae1-8f72-f4a5f8ed6f35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be407d96-7258-4621-9433-0abb30f55135",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9301943f-7a67-4f3f-9c72-f6cd7be97b8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be407d96-7258-4621-9433-0abb30f55135",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "50597295-8eac-40c5-83af-a806efc64146",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "9351015f-e211-4801-91a5-57f63dcf62d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50597295-8eac-40c5-83af-a806efc64146",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "536ce953-370c-4016-bec7-31de7ae2d09a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50597295-8eac-40c5-83af-a806efc64146",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "951c3064-b46e-4615-b580-ebbd81e5dd1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "f3286cd3-8a8f-42b7-ba3f-fba018dc95af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "951c3064-b46e-4615-b580-ebbd81e5dd1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c36c599-f840-4867-ab3c-9eb9cd2dab82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "951c3064-b46e-4615-b580-ebbd81e5dd1a",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "9ac23675-3bcc-40ac-9ff8-dfc212935354",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "bfcbb87e-8856-4615-ac5c-dc07e04fe207",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ac23675-3bcc-40ac-9ff8-dfc212935354",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e6e19dd-eb8c-4b32-b51b-298fbe55084a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ac23675-3bcc-40ac-9ff8-dfc212935354",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "59798e7d-e940-40ae-a2b8-7d59e4ce807c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "ca634061-471c-4b00-9c46-9f8fe693a3be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59798e7d-e940-40ae-a2b8-7d59e4ce807c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83b77137-c06b-41c0-90e8-da65abf85f6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59798e7d-e940-40ae-a2b8-7d59e4ce807c",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "15db94d3-2e76-4855-9507-cec3f1e6cc5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "abf274ce-7b4f-4b9a-a799-690068fe7ac8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15db94d3-2e76-4855-9507-cec3f1e6cc5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "130835c4-3f66-47f3-b693-9d1c77982856",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15db94d3-2e76-4855-9507-cec3f1e6cc5c",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "d33af107-c584-43a7-95e4-426be663c426",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "959e399a-e535-413f-957b-8ed46a8ca869",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d33af107-c584-43a7-95e4-426be663c426",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "004fd372-e8dd-4879-b326-1b5f9f8da534",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d33af107-c584-43a7-95e4-426be663c426",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "c4f44681-ce6d-41b1-8b2a-71345f6a40dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "85db4c28-403a-404d-8627-79790b09f2a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4f44681-ce6d-41b1-8b2a-71345f6a40dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da82c8fb-b30a-43bd-bb7e-8f86df7b4662",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4f44681-ce6d-41b1-8b2a-71345f6a40dd",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "aa270f98-7a51-4632-902b-b4ac1095fd94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "d692048a-ecb2-4c74-b536-8c0ad9fb8061",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa270f98-7a51-4632-902b-b4ac1095fd94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b857183-1266-4713-a063-316d661b630a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa270f98-7a51-4632-902b-b4ac1095fd94",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "ab57fc24-127e-4c3f-8bc1-78d13fb8782d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "3d3bf0fe-3261-4c06-9b9d-71030ce5c18f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab57fc24-127e-4c3f-8bc1-78d13fb8782d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b662284-3f95-4a8d-a0fc-cfade54b95c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab57fc24-127e-4c3f-8bc1-78d13fb8782d",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "ea27f200-d441-469a-8835-d9ed8820ca64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "9e27ae27-ca8f-4692-9f4d-b232dc64d93b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea27f200-d441-469a-8835-d9ed8820ca64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b8620c6-2b43-4fa7-bf22-f9ee346cc56a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea27f200-d441-469a-8835-d9ed8820ca64",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "1bac6324-87c6-4ca1-8a45-90e5153d7fca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "e82b0261-c8e8-43c3-bd6a-9ebb3f5b90a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bac6324-87c6-4ca1-8a45-90e5153d7fca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81099653-c944-4e81-b511-765c47a61f50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bac6324-87c6-4ca1-8a45-90e5153d7fca",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "1de3271e-237c-41a4-bf75-227ff27edd79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "3b15754c-4bb4-476e-83c5-5c16184d579e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1de3271e-237c-41a4-bf75-227ff27edd79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9d33e40-d5fd-4a34-b671-1d33323abb01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1de3271e-237c-41a4-bf75-227ff27edd79",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "2eaca8f0-0c1a-4eb4-987c-8a2d3c8677fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "0db7df99-7234-46b6-b350-1af26c2d72cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2eaca8f0-0c1a-4eb4-987c-8a2d3c8677fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6852bcd4-409e-402a-8e64-c5b7f8e87704",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2eaca8f0-0c1a-4eb4-987c-8a2d3c8677fb",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "4c72a182-0107-44fc-ba31-262fbde50d53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "beeb263c-ea7f-4267-ac43-71731efb9b4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c72a182-0107-44fc-ba31-262fbde50d53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc12144c-a77a-42c7-b2f1-64f65c887cd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c72a182-0107-44fc-ba31-262fbde50d53",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "e96946b4-b49c-4782-a547-601d7f2098c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "e33d0bb2-51e2-4982-a7d9-ab208af6b1ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e96946b4-b49c-4782-a547-601d7f2098c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21c67753-89cb-4850-ac99-e49e68c95b71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e96946b4-b49c-4782-a547-601d7f2098c3",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "b363c44a-f777-4d06-86af-6ac32a17b433",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "0ef8ce88-b7d5-49b2-b5bb-834e12957401",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b363c44a-f777-4d06-86af-6ac32a17b433",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bf1e66e-f3f5-4fff-9ee1-9f521356e6d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b363c44a-f777-4d06-86af-6ac32a17b433",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "b9b90f95-35a6-44de-9c86-40a131aea513",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "f3f1aecc-ae56-4f9a-a773-47c28d23ecc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9b90f95-35a6-44de-9c86-40a131aea513",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fbb5ffb-3fe3-42c1-87c8-2d6126d02a1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9b90f95-35a6-44de-9c86-40a131aea513",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "bdf6e40d-92cb-4bd8-b72d-95ec41c2eaf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "666d8224-30d0-43d2-85fb-3c3e6a76753e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdf6e40d-92cb-4bd8-b72d-95ec41c2eaf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da2c6ff6-f5e8-4e66-b674-1ccb051b882a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdf6e40d-92cb-4bd8-b72d-95ec41c2eaf0",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "4a954a0c-2eec-4fd1-9c9f-de6c0d74c6d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "5539a62c-2207-4ec7-aee8-5bd650579b17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a954a0c-2eec-4fd1-9c9f-de6c0d74c6d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b12595f-d78a-4bea-bee2-7f8930b1d74f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a954a0c-2eec-4fd1-9c9f-de6c0d74c6d9",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "1bafee11-85a6-4593-b544-9a405244e974",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "fc6af64b-beb3-4b40-ae59-87e473f0121c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bafee11-85a6-4593-b544-9a405244e974",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ac6f0a4-ca4d-403b-9b80-6f0ef94f7fab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bafee11-85a6-4593-b544-9a405244e974",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "e3927c17-cc47-42be-ad7c-1f4e18ae0078",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "5b59d312-2b5e-4941-a573-f160ab584c7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3927c17-cc47-42be-ad7c-1f4e18ae0078",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20d65296-e8ab-49ed-b659-a230547be2cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3927c17-cc47-42be-ad7c-1f4e18ae0078",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "fc8d018d-f1c8-44cd-896a-d72427f7da0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "b8b30868-ec3f-4595-ab97-5a0c95495a11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc8d018d-f1c8-44cd-896a-d72427f7da0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d34ba2e7-473f-47eb-b69b-14f2d1d36519",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc8d018d-f1c8-44cd-896a-d72427f7da0a",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "d6e97513-7db6-4194-a1b7-8cdb1e898f61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "a236a4f1-d12e-4d71-b0a4-b882cbd7a969",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6e97513-7db6-4194-a1b7-8cdb1e898f61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87231db2-ce56-4b51-990c-27df4ca3f493",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6e97513-7db6-4194-a1b7-8cdb1e898f61",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "88b595c5-8dd2-4e33-a39c-6d6b3114ab6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "589f0f1e-c08c-41e5-a229-529ad1c29a39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88b595c5-8dd2-4e33-a39c-6d6b3114ab6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acb66de7-4c5e-438d-8212-a94cdb44f545",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88b595c5-8dd2-4e33-a39c-6d6b3114ab6e",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "b7e63eda-30d2-42cc-93c6-76a9bfd015c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "6c45fe4c-ff38-4573-a761-755cccb647ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7e63eda-30d2-42cc-93c6-76a9bfd015c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b92aa08-cda6-414c-bfef-9646defccb54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7e63eda-30d2-42cc-93c6-76a9bfd015c3",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "8b91e3e0-b197-468d-8536-ba5cf7d068b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "89cb8571-db84-4cc3-a3ec-dcbc2e9126fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b91e3e0-b197-468d-8536-ba5cf7d068b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea70f081-d96a-43af-bd96-0fd31dfda196",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b91e3e0-b197-468d-8536-ba5cf7d068b3",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "3ee1b263-9a09-4412-b6e5-14e5d604782f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "6d6fde15-0a58-441a-8a06-b5afe14809bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ee1b263-9a09-4412-b6e5-14e5d604782f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eda57fa7-cf89-4cb7-8302-0e4b043e7f45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ee1b263-9a09-4412-b6e5-14e5d604782f",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "1932f911-1f50-4680-9073-b14d3bd440ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "30a634ef-eb7d-4c0d-8155-517cb4150f65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1932f911-1f50-4680-9073-b14d3bd440ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22c69d8a-963f-4309-8217-869f348a105b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1932f911-1f50-4680-9073-b14d3bd440ec",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "7b662c12-21a6-4ed4-9868-f1ae5fad910b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "0006cb3a-3038-485a-8b9d-f79ac29c9103",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b662c12-21a6-4ed4-9868-f1ae5fad910b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67143e57-6deb-4e86-a08e-c999c0a4f0eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b662c12-21a6-4ed4-9868-f1ae5fad910b",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "ff20eaad-ddcb-4e66-b3f3-ffad83f8bcc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "cb0b706d-b94c-4fc0-b894-105d344bfef0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff20eaad-ddcb-4e66-b3f3-ffad83f8bcc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8db971d-85fe-40c8-bde1-ad530105f36d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff20eaad-ddcb-4e66-b3f3-ffad83f8bcc6",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "6308872a-a13d-4f10-aa16-8ec4c099b3a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "d04307e7-f5a0-41d3-acf9-b90253e81f01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6308872a-a13d-4f10-aa16-8ec4c099b3a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "610a9027-8da6-4a7b-af1e-02311486c169",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6308872a-a13d-4f10-aa16-8ec4c099b3a2",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "3613ac47-679e-4e78-a5c7-34e7f7bcc35d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "6c356978-5c72-4940-b91b-e5bd74896745",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3613ac47-679e-4e78-a5c7-34e7f7bcc35d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d400e4b9-97e9-4db1-a6d6-102696ea2d9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3613ac47-679e-4e78-a5c7-34e7f7bcc35d",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "b08b0d8e-d926-4a40-9bbe-978a931e3fc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "af301d24-c30e-4035-9a0b-7f381892c395",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b08b0d8e-d926-4a40-9bbe-978a931e3fc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9633b9cd-9a59-45f7-8ddd-0b1ad71ff6c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b08b0d8e-d926-4a40-9bbe-978a931e3fc2",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "ec8e930f-f036-458c-bd7b-8987c24795ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "256d4061-7bce-4780-a2a2-b8b6f09a7fde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec8e930f-f036-458c-bd7b-8987c24795ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5651f30-af4b-4749-ad7f-35168ea50065",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec8e930f-f036-458c-bd7b-8987c24795ad",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "2eba32ba-62bb-4fdd-ab01-a0d33ac26d9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "0517ee36-7aa4-4e8d-b1b0-9b395eb671d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2eba32ba-62bb-4fdd-ab01-a0d33ac26d9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0994700b-f43e-48ab-a03a-47e02ae2fdd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2eba32ba-62bb-4fdd-ab01-a0d33ac26d9b",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "206dc5b4-1310-4857-b2bb-d2ec494743d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "d3ce1eb0-d353-46ae-b365-50e3d14395f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "206dc5b4-1310-4857-b2bb-d2ec494743d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de71eab9-7a4f-4032-a30e-3fa6ed3086cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "206dc5b4-1310-4857-b2bb-d2ec494743d1",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "9e4e988c-fb44-48f1-aff5-bf249f84c34a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "78e1bff7-a7fb-45e1-9b35-319ad14ca0d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e4e988c-fb44-48f1-aff5-bf249f84c34a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b881140e-2510-4ba8-aa8e-b612cb51473d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e4e988c-fb44-48f1-aff5-bf249f84c34a",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "eb49f94a-7304-45ab-9b2b-5106a29c085e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "6756d812-6fa9-4bf0-9169-d76d7a4d00a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb49f94a-7304-45ab-9b2b-5106a29c085e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ea1c37d-dbac-4c83-8e15-69976da7b24f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb49f94a-7304-45ab-9b2b-5106a29c085e",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "afb424fc-4040-4dfc-a0d9-48e55d70611c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "8d8d04d6-f345-4d77-99e1-33ff5ef66b2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afb424fc-4040-4dfc-a0d9-48e55d70611c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6337a37-ddb5-4564-b3ce-908b87db318d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afb424fc-4040-4dfc-a0d9-48e55d70611c",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "d37c0df8-3bf3-40c0-a04b-0432f304c202",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "083e9250-f6d2-4345-b8ad-4850501fb203",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d37c0df8-3bf3-40c0-a04b-0432f304c202",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2f7cfa6-630f-464c-adab-0cd05f4eea18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d37c0df8-3bf3-40c0-a04b-0432f304c202",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "6cd1e052-d944-4262-b2a8-4aa861ef9535",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "b432ace0-4716-43f8-93ca-f9fbd9d68b72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cd1e052-d944-4262-b2a8-4aa861ef9535",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36d950a5-6012-4793-9648-44ef65add94d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cd1e052-d944-4262-b2a8-4aa861ef9535",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "e95fa433-af8f-44c3-a49d-a91b3c0f2bd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "3d3ee7c5-fa35-455e-865b-0e1eed540b69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e95fa433-af8f-44c3-a49d-a91b3c0f2bd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6b17dbd-29ac-489c-8f61-0fa77e9a0383",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e95fa433-af8f-44c3-a49d-a91b3c0f2bd9",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "fe0086ae-569e-42a5-a98f-4156afd68f67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "a759df42-320c-4fea-bf08-6c0f3efa1de4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe0086ae-569e-42a5-a98f-4156afd68f67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a1b3808-6568-4dbb-ac42-c1da191fdbad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe0086ae-569e-42a5-a98f-4156afd68f67",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "cb0b7742-1dfa-4f20-8051-0a4f2bc7105e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "4e0a31a2-85ca-496c-98fc-6840d310deb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb0b7742-1dfa-4f20-8051-0a4f2bc7105e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f825be3-3996-4b6d-ab0a-53055a40e4ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb0b7742-1dfa-4f20-8051-0a4f2bc7105e",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "fb202eaa-605f-4c5c-9758-5a1a94be0f39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "9e6f772d-51ec-4e2f-a2d9-e7962adbb70b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb202eaa-605f-4c5c-9758-5a1a94be0f39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a4a198b-9c93-42b1-8e54-2db77b6977d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb202eaa-605f-4c5c-9758-5a1a94be0f39",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "157ea272-b7e5-4d0f-8110-6e2122151ce6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "bb44c183-87d7-4ed3-bd4c-1a04b30ace75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "157ea272-b7e5-4d0f-8110-6e2122151ce6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4803698-24a4-4965-b94a-ca1c9507b69c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "157ea272-b7e5-4d0f-8110-6e2122151ce6",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "8e767b05-f09e-485e-97a8-706bdcd1ab36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "17d60b14-aaaf-4ae1-9a54-6410ef6bb516",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e767b05-f09e-485e-97a8-706bdcd1ab36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bfa5937-af73-4102-a9c6-bce703d62ab7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e767b05-f09e-485e-97a8-706bdcd1ab36",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "7c5bde69-d3b9-4972-b128-8a959cdf2d42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "9bf3cd48-3124-4b41-ae7c-55fd13e88950",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c5bde69-d3b9-4972-b128-8a959cdf2d42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fb326a9-7c5f-48ad-a46b-93702c8a0638",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c5bde69-d3b9-4972-b128-8a959cdf2d42",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "9d7d45dd-7247-41db-864b-1616c6f1aeda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "7b44c6fe-b25d-40bf-9d0f-4013399694e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d7d45dd-7247-41db-864b-1616c6f1aeda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48ae114a-aa4a-4d56-b4a4-5f0665a3fea3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d7d45dd-7247-41db-864b-1616c6f1aeda",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "07aafb0d-03d3-4ed8-926d-538b6dee61e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "0961874b-e6af-4bbb-b871-b66e72592501",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07aafb0d-03d3-4ed8-926d-538b6dee61e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4df63ec5-7c4e-4d98-8a85-fd1efe9a8f32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07aafb0d-03d3-4ed8-926d-538b6dee61e6",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "caae9773-80fd-4e33-b4c2-7677f13ac325",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "90b78886-6b9b-4070-9484-c9bedca2dc6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caae9773-80fd-4e33-b4c2-7677f13ac325",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "681adb80-fb6d-4653-af76-a44d379160b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caae9773-80fd-4e33-b4c2-7677f13ac325",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "76bb1f3f-d582-4ce4-bc99-c0d4c64a2444",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "b14f50fe-d81d-4ee8-97a0-378ac5650494",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76bb1f3f-d582-4ce4-bc99-c0d4c64a2444",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f50d5563-cac8-4773-a0a2-5b127017a474",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76bb1f3f-d582-4ce4-bc99-c0d4c64a2444",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "b16f3525-5910-4863-a6a3-1742fbae9bd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "ac74c9a3-60be-4935-a04d-2ef0685750bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b16f3525-5910-4863-a6a3-1742fbae9bd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33a4f619-6ed8-4726-8dad-3583a7b58eec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b16f3525-5910-4863-a6a3-1742fbae9bd8",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "f1dacc80-1d94-4634-bb7d-04681f06c745",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "552c8091-52ca-442b-873a-530252051fed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1dacc80-1d94-4634-bb7d-04681f06c745",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb5ad9ac-7d6f-49f6-81e4-7f9cfad1a462",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1dacc80-1d94-4634-bb7d-04681f06c745",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "b23635de-8269-4935-b079-ba5202eba61a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "c7682f5d-1402-4c62-8a38-45c173764ec5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b23635de-8269-4935-b079-ba5202eba61a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5405bfba-6441-403d-b4d9-1348dd64bf8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b23635de-8269-4935-b079-ba5202eba61a",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "2f0e66d7-613b-45c5-a30e-476777da7504",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "1bb55b8d-3837-453f-b921-2a8e2f899eee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f0e66d7-613b-45c5-a30e-476777da7504",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "263435cc-f3ac-462c-849f-e395833b1f6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f0e66d7-613b-45c5-a30e-476777da7504",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "7eeaa8a4-69c2-4e4f-b582-57b7bc99555c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "e1237d74-4d33-42f1-b652-4860f985d344",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7eeaa8a4-69c2-4e4f-b582-57b7bc99555c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20b34468-563e-443c-9813-2413f9908a75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7eeaa8a4-69c2-4e4f-b582-57b7bc99555c",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "a717ecdc-176b-4cea-8f2a-31d8a3eb404f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "0218ff86-af96-4b92-8e31-22e29ad1cb6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a717ecdc-176b-4cea-8f2a-31d8a3eb404f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cff18390-6847-4641-8f05-35c2b62fd0f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a717ecdc-176b-4cea-8f2a-31d8a3eb404f",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "cf4fcd0a-3dcc-4aa3-b54e-5437dd4e3bab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "d4d0099a-7087-4386-bc3e-be2f03cb456a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf4fcd0a-3dcc-4aa3-b54e-5437dd4e3bab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8accdb6c-f1a6-4dff-8e4e-1b8a2160733e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf4fcd0a-3dcc-4aa3-b54e-5437dd4e3bab",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "7ca60e3c-dfea-460c-b0aa-2a39708832e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "afb5a50d-62a5-4d7e-9e85-677cdc458c28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ca60e3c-dfea-460c-b0aa-2a39708832e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3137613e-f133-4175-b2a9-9113c3396407",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ca60e3c-dfea-460c-b0aa-2a39708832e8",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "ce46e24c-db0d-4f07-be5f-4e281054463a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "b310a105-8c89-4839-8a27-dd04484df3a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce46e24c-db0d-4f07-be5f-4e281054463a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "655b6d88-46b4-4db7-bba7-3b743db997d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce46e24c-db0d-4f07-be5f-4e281054463a",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "e81262ef-61d8-44cb-9118-90a7563b73fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "1e911ae4-e930-435a-8fbf-4e8b5bfde909",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e81262ef-61d8-44cb-9118-90a7563b73fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5919214b-743c-48d7-975e-217644687f41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e81262ef-61d8-44cb-9118-90a7563b73fc",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "3ff6863b-3c1d-4e65-ad22-6197da33f900",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "54ba751c-99aa-4db1-94e0-438f3422a07c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ff6863b-3c1d-4e65-ad22-6197da33f900",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b54c862a-b40c-433b-8f40-f578c64567c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ff6863b-3c1d-4e65-ad22-6197da33f900",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "ddf0672d-ac8b-4e47-b5c4-0648088b80dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "fdc94cfc-9448-4c3c-b7c2-a33cfed179ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddf0672d-ac8b-4e47-b5c4-0648088b80dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e191aa70-c4bf-471e-ac1d-8e0c0bda5bcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddf0672d-ac8b-4e47-b5c4-0648088b80dd",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "c377f7ae-1e19-4a20-8328-72fc1eb50f28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "8a6bd5f6-d462-491c-b612-4e8648dd297b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c377f7ae-1e19-4a20-8328-72fc1eb50f28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f63d0129-dc23-41b5-b15d-5a864e5451d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c377f7ae-1e19-4a20-8328-72fc1eb50f28",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "9827f5e1-b290-44c4-8d83-0fb20a708bf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "0076e1cc-d4ce-4e85-9e4b-a0070a22b054",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9827f5e1-b290-44c4-8d83-0fb20a708bf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "998d5942-650e-44a7-b8af-2e42c072b6f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9827f5e1-b290-44c4-8d83-0fb20a708bf7",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "f16d21d3-6c53-4e26-8eb8-55cf42efb0e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "b8c01b81-d2c3-46f7-9ffa-9ad76124408e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f16d21d3-6c53-4e26-8eb8-55cf42efb0e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d26cd77-cb8b-4923-9277-eb66e1ea3f6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f16d21d3-6c53-4e26-8eb8-55cf42efb0e9",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "62eb59b6-d829-4f47-81ff-7fac0c592a0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "e8147c6b-19a6-44ce-8c50-f400b0afc0c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62eb59b6-d829-4f47-81ff-7fac0c592a0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62329668-b0a0-4536-b470-e13b30e43b52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62eb59b6-d829-4f47-81ff-7fac0c592a0c",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "43e00f46-7fb6-4aba-8d8f-455ce33997e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "a43c99fc-869d-46fe-86ea-e5558e0ac800",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43e00f46-7fb6-4aba-8d8f-455ce33997e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2f2167d-95ee-4430-a17d-baeef506193e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43e00f46-7fb6-4aba-8d8f-455ce33997e9",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "c834e044-b407-45a4-866b-4c863031b5bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "b1f7342c-0067-43cd-9e1f-696e15114fc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c834e044-b407-45a4-866b-4c863031b5bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab6b5baf-bc0b-4897-8974-6c5423071bb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c834e044-b407-45a4-866b-4c863031b5bd",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "938dd49b-cfc7-4199-ad09-0c583a1dbe05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "86059ce1-1d58-413c-81b9-dbec8f958126",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "938dd49b-cfc7-4199-ad09-0c583a1dbe05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2c6242a-20ab-4a59-9190-90793695567b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "938dd49b-cfc7-4199-ad09-0c583a1dbe05",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "d7c933f3-9eb4-4c47-bedc-033ed718057c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "91a19da0-9bfb-4604-a219-f97e7d7cbbe0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7c933f3-9eb4-4c47-bedc-033ed718057c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5438785f-acf5-4a8c-89a1-ea0ab1dd2159",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7c933f3-9eb4-4c47-bedc-033ed718057c",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "1708e201-8bef-47fc-9daa-1c85e232dce5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "d684bdfe-03c2-44ac-9d76-4a3e42f01744",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1708e201-8bef-47fc-9daa-1c85e232dce5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9661832d-71a4-4368-8516-4385418be81b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1708e201-8bef-47fc-9daa-1c85e232dce5",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "10f7ac36-9b65-42ec-864f-1cbb3947c3e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "a455a3fd-570b-401d-9759-ebad0ea92e82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10f7ac36-9b65-42ec-864f-1cbb3947c3e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89789624-f17d-4605-8b35-e677865af73a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10f7ac36-9b65-42ec-864f-1cbb3947c3e1",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "66da680a-7da4-4574-9286-b2b9ff1d3242",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "b11ae732-9fcc-4163-9e46-afc475c1d41a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66da680a-7da4-4574-9286-b2b9ff1d3242",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd45f374-85b5-4ed8-bb90-8867d853a4b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66da680a-7da4-4574-9286-b2b9ff1d3242",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "d990a866-a7ee-47a9-94ea-16773aa3f084",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "aa617796-ce05-4813-8881-a1753e51c9c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d990a866-a7ee-47a9-94ea-16773aa3f084",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "130f61a0-4f26-46c0-909e-75bd5f04da1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d990a866-a7ee-47a9-94ea-16773aa3f084",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "ba2b7ea0-43e5-4153-a377-7a9b1b0cce20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "a0fb6cd9-3fa5-4409-8887-17227d1c5e2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba2b7ea0-43e5-4153-a377-7a9b1b0cce20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2ee2ef6-a507-4d03-aec0-1f41c3b4cfc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba2b7ea0-43e5-4153-a377-7a9b1b0cce20",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "47467553-8e63-4ce0-8662-d3ffa8cdafd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "c3c60d5a-c723-4efe-82b2-a0732fb48590",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47467553-8e63-4ce0-8662-d3ffa8cdafd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef60aaa0-dc0c-4f02-bdff-6f1bf19f8f3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47467553-8e63-4ce0-8662-d3ffa8cdafd4",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "9d51c1d6-f898-45e4-800e-473dfea0b02d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "532a0043-0d95-4971-a119-83890df4a95f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d51c1d6-f898-45e4-800e-473dfea0b02d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24f7d1bf-17e7-48a1-b1d1-cdc04067c8f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d51c1d6-f898-45e4-800e-473dfea0b02d",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "257dea61-287d-4ad0-9982-3a815703ff6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "59ed3c21-14b9-4bfe-8607-f4be059f53f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "257dea61-287d-4ad0-9982-3a815703ff6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2290c49a-1315-41b9-bcf5-063c1c1545ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "257dea61-287d-4ad0-9982-3a815703ff6f",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "062b44bf-f2d2-4300-8582-46b81c6b28ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "63d14864-53ac-44fe-8826-6d72650bc57c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "062b44bf-f2d2-4300-8582-46b81c6b28ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0548d594-3d85-4f1b-8b8d-602768315ad7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "062b44bf-f2d2-4300-8582-46b81c6b28ad",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "74db3b01-8039-4569-b14b-4af2be0c42f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "654898dd-fed7-49bb-940b-976d3dc93fbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74db3b01-8039-4569-b14b-4af2be0c42f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da4d63bf-9288-4ef5-b7b0-b8078531417f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74db3b01-8039-4569-b14b-4af2be0c42f0",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "d5dbb4fe-68ab-4c3b-841f-cc6fd5071516",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "0e2dab27-cd54-449e-ad13-02ab8c9f4a78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5dbb4fe-68ab-4c3b-841f-cc6fd5071516",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "248b0b7b-89fa-4f58-85ac-12705ea9f19e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5dbb4fe-68ab-4c3b-841f-cc6fd5071516",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "2d6f5e44-29b0-4238-9b96-da1db6a34562",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "e4fffece-37c0-4d74-85d1-e66ac4019247",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d6f5e44-29b0-4238-9b96-da1db6a34562",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4661a00-f77d-466e-bb1e-53f92325cd24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d6f5e44-29b0-4238-9b96-da1db6a34562",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "da0d833f-e477-4df7-8c5c-15b8d24a97a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "aed7a014-2a15-49c7-a5bc-6793522412cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da0d833f-e477-4df7-8c5c-15b8d24a97a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96cac66d-ff1b-454b-90b7-9550e9a152cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da0d833f-e477-4df7-8c5c-15b8d24a97a9",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "c04b4b49-4960-4c43-b0ba-5a6cd33c29cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "1df65236-e7c3-4484-b35e-5f5763a3d4a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c04b4b49-4960-4c43-b0ba-5a6cd33c29cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7c62b17-95a2-472a-8ade-4c2d59dfa403",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c04b4b49-4960-4c43-b0ba-5a6cd33c29cc",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "a238a95e-58ca-4d9e-a648-c427b6ae03e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "fe21ceb1-53f2-4175-a39c-8b773857ac31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a238a95e-58ca-4d9e-a648-c427b6ae03e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "580c7f2e-d6fd-4909-b9ea-3256927cf24f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a238a95e-58ca-4d9e-a648-c427b6ae03e8",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "0637ccdd-aa56-49ed-9bc0-af6be0971034",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "ce8dfaf4-b265-45e9-a992-3a6665ab2a20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0637ccdd-aa56-49ed-9bc0-af6be0971034",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bf096fc-347c-490d-8899-f3198bfa5b3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0637ccdd-aa56-49ed-9bc0-af6be0971034",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "cec236b4-d51e-4a4e-ace2-00c52fa45319",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "bc8e3c76-87ad-4335-9d64-8cbdf40332c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cec236b4-d51e-4a4e-ace2-00c52fa45319",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52160836-93cb-497e-9b26-c54e8883fe91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cec236b4-d51e-4a4e-ace2-00c52fa45319",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "0522e82c-9489-4813-a1a1-f445f44fdc98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "bad58abc-0da8-4406-a43a-7082ce5804da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0522e82c-9489-4813-a1a1-f445f44fdc98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e20c97a-334b-418b-9f92-086db1211f05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0522e82c-9489-4813-a1a1-f445f44fdc98",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "577e02e6-6878-4f62-991e-0a3bca025cfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "04ba33d3-93c8-4a35-9635-070c6da0bc00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "577e02e6-6878-4f62-991e-0a3bca025cfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc4e14dc-4115-4718-93c1-056aa89debb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "577e02e6-6878-4f62-991e-0a3bca025cfb",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "82f769ad-d0a8-4459-899e-25e4499d48de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "a8a3c92a-d25c-47ae-ab14-e2594545fcc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82f769ad-d0a8-4459-899e-25e4499d48de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85f65869-e102-4c34-a6c3-94c3cd7a78d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82f769ad-d0a8-4459-899e-25e4499d48de",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "f964720b-208d-420a-931d-e6e8cfa936fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "65799974-9016-4c11-8884-9b3fbe0946e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f964720b-208d-420a-931d-e6e8cfa936fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83baebe3-f3da-4c57-836c-77faead18c6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f964720b-208d-420a-931d-e6e8cfa936fe",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "649dbedc-9a7e-4803-9b52-062781df85ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "9679d12a-371a-41fc-9cb4-cad2e6097b2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "649dbedc-9a7e-4803-9b52-062781df85ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6174891f-1ec2-475e-83b4-0a0a8d911568",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "649dbedc-9a7e-4803-9b52-062781df85ac",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "2f901ae1-5b93-416e-8944-96eb343c55a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "2a3b4418-68e2-485a-b1ed-b6226062469e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f901ae1-5b93-416e-8944-96eb343c55a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "202613e6-550b-40dc-913e-c2b9dbbff08f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f901ae1-5b93-416e-8944-96eb343c55a5",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "1e02ef3d-e928-4ddc-9852-f680ce030b72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "6c960763-36d1-485d-bb85-e0291c829534",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e02ef3d-e928-4ddc-9852-f680ce030b72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3771d65d-ac57-4f4b-8513-b4a47a548290",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e02ef3d-e928-4ddc-9852-f680ce030b72",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "05d996fc-585b-47a4-bdea-720bd8db2388",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "edf6c550-6c76-4869-9ff1-04d91b976831",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05d996fc-585b-47a4-bdea-720bd8db2388",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20a2e79f-140d-4498-87cd-f4a88b30c7f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05d996fc-585b-47a4-bdea-720bd8db2388",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "7681aae5-fc55-445f-bd87-b82a552e629e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "f355c814-c3f3-4eab-bbdd-61f49e60b73a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7681aae5-fc55-445f-bd87-b82a552e629e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2a44248-c007-4908-9fdc-95f61cb2225a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7681aae5-fc55-445f-bd87-b82a552e629e",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "a49fce1a-d6f4-4b41-86b5-30a7b3f38680",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "b9376459-a882-4ff3-83b5-e3ad6a07f2ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a49fce1a-d6f4-4b41-86b5-30a7b3f38680",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bebc6c17-e961-4894-9a2d-4541336f5d4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a49fce1a-d6f4-4b41-86b5-30a7b3f38680",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "7f414036-d04b-460f-811b-189a66625b45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "1a187531-22e6-4d04-81ea-aec1bcfdf6c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f414036-d04b-460f-811b-189a66625b45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d3bea63-f1a3-4985-8ad4-31dc27560a4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f414036-d04b-460f-811b-189a66625b45",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "5fef604a-db4c-45db-ba7b-53e8b1320cab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "f37acdc3-ca2e-4866-b510-f478c1134b37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fef604a-db4c-45db-ba7b-53e8b1320cab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afae8e05-8c34-4c53-801a-3cda445d14aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fef604a-db4c-45db-ba7b-53e8b1320cab",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "554f388b-f545-4dd2-90e2-d7bd9922a5d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "f44be95e-f60e-4ba1-8e78-438d8344c8f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "554f388b-f545-4dd2-90e2-d7bd9922a5d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0e10063-dbf8-4fc1-b65e-ac890ecad604",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "554f388b-f545-4dd2-90e2-d7bd9922a5d4",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "3929d2ee-9b58-44e5-b8da-947cef13c7c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "37284aa1-94a5-4bf8-b37c-62b229ddb42e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3929d2ee-9b58-44e5-b8da-947cef13c7c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e95be23-8313-4596-a47f-47034fc5286a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3929d2ee-9b58-44e5-b8da-947cef13c7c1",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "5a0cab31-9361-42b5-ae51-be10abaee712",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "b78b1821-168c-4155-998c-3143e620a806",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a0cab31-9361-42b5-ae51-be10abaee712",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18f053bf-78f6-4b40-a431-5fbceefee8fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a0cab31-9361-42b5-ae51-be10abaee712",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "8f7767c2-cccf-427c-b1b1-e9d36d792fb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "9f53ceb1-f5e7-4a8e-8c25-144001659649",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f7767c2-cccf-427c-b1b1-e9d36d792fb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eb40344-3ced-40a4-80c2-ff18cb74be8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f7767c2-cccf-427c-b1b1-e9d36d792fb0",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "c17b3e1f-84d1-4a17-8548-ebe4291cf1f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "1e5c7d7b-9806-47fe-98f0-18e37b134173",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c17b3e1f-84d1-4a17-8548-ebe4291cf1f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e1244b8-37c7-4d70-a0ee-e02f8e48d08b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c17b3e1f-84d1-4a17-8548-ebe4291cf1f3",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "5b14e9ae-0373-498a-9910-30a53195d667",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "337f94bf-392e-4174-84a5-66c5a0d71ba9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b14e9ae-0373-498a-9910-30a53195d667",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4934763f-c71e-49cf-8de8-d42f162b4031",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b14e9ae-0373-498a-9910-30a53195d667",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "4362c660-7c9d-4734-b423-e4c900ae6598",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "c273d980-540d-411d-b1de-53b32dee0631",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4362c660-7c9d-4734-b423-e4c900ae6598",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eca4370a-6d5f-4311-b8ec-e68b50029fd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4362c660-7c9d-4734-b423-e4c900ae6598",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        },
        {
            "id": "64b8de78-6dee-47f3-b8f1-985b67457d3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "compositeImage": {
                "id": "0a8f6beb-4585-440b-bb71-74a74041e82f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64b8de78-6dee-47f3-b8f1-985b67457d3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "693a1a63-9cb0-403e-a1a4-76aee84a0b59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64b8de78-6dee-47f3-b8f1-985b67457d3d",
                    "LayerId": "c3b57960-f76e-401c-8a7e-84002b42d6f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "c3b57960-f76e-401c-8a7e-84002b42d6f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4430d0b3-bac3-43ab-b059-3db3b4c2e3e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 0,
    "yorig": 0
}