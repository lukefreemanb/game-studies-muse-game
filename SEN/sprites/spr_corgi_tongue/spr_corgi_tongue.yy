{
    "id": "db2d800b-c945-414e-843e-1276a3979502",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_corgi_tongue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 21,
    "bbox_right": 34,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35982bf7-a2e6-49a7-932d-ab7e54ea7adf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db2d800b-c945-414e-843e-1276a3979502",
            "compositeImage": {
                "id": "1fec3433-eddc-4f3a-974b-b259086b12a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35982bf7-a2e6-49a7-932d-ab7e54ea7adf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "407f8bf1-d555-48fe-baa8-8bbe3d670c0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35982bf7-a2e6-49a7-932d-ab7e54ea7adf",
                    "LayerId": "e4040157-37fe-49a1-8819-de16ce3a1f52"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "e4040157-37fe-49a1-8819-de16ce3a1f52",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db2d800b-c945-414e-843e-1276a3979502",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 35
}