{
    "id": "490fe3c6-edf8-4079-828c-73db12314946",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_glimmer2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 6,
    "bbox_right": 8,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "162395c5-4dd1-4b7a-b0c3-d60451f7f3e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "13f4c13c-0b25-4250-96bd-daf3de1ccf4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "162395c5-4dd1-4b7a-b0c3-d60451f7f3e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df3e107c-7deb-41b2-a21b-45ee7280c87c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "162395c5-4dd1-4b7a-b0c3-d60451f7f3e0",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "ce8fa570-dbe2-4ee5-8f1d-c81c0f9aa18c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "8c32aa30-4968-4473-9b4d-2eb2a6a15a76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce8fa570-dbe2-4ee5-8f1d-c81c0f9aa18c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b35b3a6-3129-4f4a-9441-65d2e991d241",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce8fa570-dbe2-4ee5-8f1d-c81c0f9aa18c",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "04d5b3fe-069b-402a-8f0f-a2ae44f19a97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "5891aae4-443d-42b2-8783-a5f1e679591b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04d5b3fe-069b-402a-8f0f-a2ae44f19a97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95d9a94b-7d4a-4e71-9a7d-15c5600726a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04d5b3fe-069b-402a-8f0f-a2ae44f19a97",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "4d44b289-19eb-4d3e-a54b-bfef6b5c3c36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "29599377-05fb-414c-8ee5-7f7bebfbab8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d44b289-19eb-4d3e-a54b-bfef6b5c3c36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c38e42cf-215b-419d-814b-b19af6be4816",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d44b289-19eb-4d3e-a54b-bfef6b5c3c36",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "b40e60bd-e210-4299-b0f6-061a4a628e3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "5f1458ca-fead-4285-9446-60b3b3403f2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b40e60bd-e210-4299-b0f6-061a4a628e3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8f28cdf-868a-44e9-9847-f721d3513ec3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b40e60bd-e210-4299-b0f6-061a4a628e3f",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "2c5368aa-ddc7-4eff-84f9-3b9ae25dd6a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "f87472de-727c-46fa-8162-796f1f24fed0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c5368aa-ddc7-4eff-84f9-3b9ae25dd6a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f100db71-d11b-44c2-92ab-2f3ac667302a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c5368aa-ddc7-4eff-84f9-3b9ae25dd6a3",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "57820fdb-a168-4470-9e55-06414431b663",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "f15fd8c1-d0b8-4cba-8e77-054d045d40f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57820fdb-a168-4470-9e55-06414431b663",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fd00139-5f1b-4e34-938c-1ad8999c4620",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57820fdb-a168-4470-9e55-06414431b663",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "023e5b35-6962-4386-bef4-2c639a185d99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "c2f2e18b-b81b-4d68-a3ec-44a0de92b5eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "023e5b35-6962-4386-bef4-2c639a185d99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17015a77-e709-4f8b-81af-80a30f2d302f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "023e5b35-6962-4386-bef4-2c639a185d99",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "97ebf556-4441-40a2-a806-458d93da4bab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "69587aee-933b-41d9-b3fe-71d621cd4780",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97ebf556-4441-40a2-a806-458d93da4bab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c3761c6-3591-4de1-a692-2c0ad3780d6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97ebf556-4441-40a2-a806-458d93da4bab",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "77e4d5ae-f163-4ff5-a662-314a7fd14ee3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "614018ea-37ae-4b01-9a43-4ea4372f32df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77e4d5ae-f163-4ff5-a662-314a7fd14ee3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8793cc29-a309-471f-8a28-b2e54c3e537e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77e4d5ae-f163-4ff5-a662-314a7fd14ee3",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "75b05852-6afd-4986-9bd2-588202a744c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "1e4f8ce2-7f12-4a1c-891e-463c3bdc37b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75b05852-6afd-4986-9bd2-588202a744c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab35ebaa-d81a-48c5-bf0f-801a91da5e89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75b05852-6afd-4986-9bd2-588202a744c6",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "44ac59b7-e474-494a-ace3-e01045a8ced4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "5a61bce7-7249-463b-91f7-da22a4adb6cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44ac59b7-e474-494a-ace3-e01045a8ced4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b416efef-4d88-475e-bc33-7119966fa938",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44ac59b7-e474-494a-ace3-e01045a8ced4",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "13fc96c8-4d63-49ed-beae-c2d2c9e7186c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "33c57337-8e15-4594-b995-87f7ba72376d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13fc96c8-4d63-49ed-beae-c2d2c9e7186c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99faee44-dbbb-42d1-b530-f5784784ac13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13fc96c8-4d63-49ed-beae-c2d2c9e7186c",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "3abd6c57-53ce-418f-ba39-772291988c00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "f552f64c-2e80-4c5f-a3ad-8a7fa901e787",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3abd6c57-53ce-418f-ba39-772291988c00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfd6b861-fc96-4fa5-b1d8-4baaf68ebf46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3abd6c57-53ce-418f-ba39-772291988c00",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "2241343a-6f19-407d-a461-5451c75e1272",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "b7dd882b-8315-4418-9d91-33962681535b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2241343a-6f19-407d-a461-5451c75e1272",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4caadac-3744-44c5-a6b9-b0cdce21f79e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2241343a-6f19-407d-a461-5451c75e1272",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "3a7d0c4a-b3e1-494d-be17-f9ea513c3455",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "c426f2fc-6314-45a8-83cf-6b5ebe9a47b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a7d0c4a-b3e1-494d-be17-f9ea513c3455",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fb3b46f-01bd-4966-a0a0-93e1ed85fa75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a7d0c4a-b3e1-494d-be17-f9ea513c3455",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "54a55670-0d6f-4cf4-aa75-4193f38e61a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "7aae05a7-5f46-46b7-9372-20ff8dabc21d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54a55670-0d6f-4cf4-aa75-4193f38e61a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6eeb8be-e4ef-42af-bb5f-65285619d56f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54a55670-0d6f-4cf4-aa75-4193f38e61a3",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "1ed8e8a9-bd36-4107-9970-908aad32fff6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "9124a45a-5286-4153-8806-962c3af459a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ed8e8a9-bd36-4107-9970-908aad32fff6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b9471b5-408d-4420-9396-a892c53a0771",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ed8e8a9-bd36-4107-9970-908aad32fff6",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "ebd4fa5a-c55a-433f-b84d-4b01c83f5cc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "1fdff65f-0447-48ed-a5ec-59c893145759",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebd4fa5a-c55a-433f-b84d-4b01c83f5cc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9180fee9-320f-4fab-83db-353dad8d5d8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebd4fa5a-c55a-433f-b84d-4b01c83f5cc4",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "c11c136a-3819-423c-9ccb-5577ae47057c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "5904c631-92fb-41c8-9d1a-3201db37bc5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c11c136a-3819-423c-9ccb-5577ae47057c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a59db4bf-3c0d-46ed-b0b0-882467787dfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c11c136a-3819-423c-9ccb-5577ae47057c",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "2e9e47ee-ce86-4416-9d7d-2e7fc7c608d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "4462d3d4-31da-4396-81b4-0d131d8f85a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e9e47ee-ce86-4416-9d7d-2e7fc7c608d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd6b81fe-6c45-4d56-b369-4dd025617f6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e9e47ee-ce86-4416-9d7d-2e7fc7c608d9",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "a8cea5e2-234f-4d23-9d8f-6ca24e951a69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "bb7748ff-887e-4249-8408-dc9a9f2cf36a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8cea5e2-234f-4d23-9d8f-6ca24e951a69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26ae8299-b9dd-4beb-8cdd-e8a5d2c7e177",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8cea5e2-234f-4d23-9d8f-6ca24e951a69",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "4d1960e1-2872-44f3-bde0-493cd713d944",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "d12c0004-67f2-411f-9217-9a494830e306",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d1960e1-2872-44f3-bde0-493cd713d944",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "481cc45d-39b2-4099-a9cf-905f4012f0a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d1960e1-2872-44f3-bde0-493cd713d944",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        },
        {
            "id": "0454235a-7d84-45f2-8c34-4d3b74a19696",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "compositeImage": {
                "id": "18969d10-4570-4efa-887b-b6e65e9a46a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0454235a-7d84-45f2-8c34-4d3b74a19696",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbdf8262-da29-425d-9bf7-5c97e631e39f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0454235a-7d84-45f2-8c34-4d3b74a19696",
                    "LayerId": "f7c3b36f-f928-4397-be93-58f0a9197411"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f7c3b36f-f928-4397-be93-58f0a9197411",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "490fe3c6-edf8-4079-828c-73db12314946",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}