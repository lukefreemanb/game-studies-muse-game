///scr_script(identifier)
//Dialogue
q = ""; //a string used to identify the question 
msg = ""; //what the message to retrieve is
ex = false; //exit

switch(argument0) {
	//OVERWORLD DIALOGUE
	//test
	case "test": 
		msg = ["[twitch]H-[p:2]Hello? [p:3]Are you there?[p:3]#It's me, [p:3][c:orange]M[c:white].",
			"[twitch]Don't you remember me?[q:Yes,No]"];
		q = "test";
	break;
	case "testa": 
		msg = ["[twitch]Really? [p:3]I thought you'd have forgotten by now.",
			"[twitch]That's great, [p:3]that's really, really great!!"];
	break;
	case "testb": 
		msg = ["[twitch]Oh... [p:3]well I guess that's to be expected by now.",
			"[twitch]I guess I'll have to remind you about what's going on."];
	break;
	case "toad1":
		msg = ["[bounce][twitch]*kero kero*#[p:3]        *kero kero*#[p:3]                *kero kero*",
			"(Human, [p:3]you best tread lightly.)",
			"...",
			"(Why? [p:3]I don't know.)",
			"(The Boss told me to act intimidating#if I ever saw a human.#[p:2]Don't take it personally, [p:2]kiddo.)",
			"[bounce]*kero kero*"];
	break;
	case "sad dog": 
		msg = ["H-[p:3]Hi... [p:3]#H-[p:2]Huh? [p:3]What am I doing?",
			"I-[p:3]I'm just hid-[p:2], I mean, I'm standing#behind this tree stump...",
			"Y-[p:3]you're not here to bully me too, [p:2]#are you? [q:I am,I'm not]"];
		q = "sad dog";
	break;
		case "sad doga":
			ex = true; //default do nothing
		break;
		case "sad dogb":
			msg = ["Oh... [p:3]it's someone nice for once...",
				"[setXY:16,4](He looks like he needs a hug.)",
				"(Give him a hug?)[q:Yes,No]"];
			q = "sad dog1";
		break;
			case "sad dog1a":
				msg = ["(You lean in for a hug.)",
					"Oh... [p:3]I... [p:3]#thank you...",
					"(He sinks his head into your chest.)",
					"I really needed that...",
					"(You feel a warm, [p:2]wet feeling on your#shirt.)",
					"(Smells like tears.)",
					"... ... ...",
					"(It seems he doesn't want to let go...)",
					"(Keep hugging him?)[q:Yes,No]"];
				q = "sad dog2"
			break;
				case "sad dog2a":
					msg = ["Ah! [p:3]Sorry! [p:3]I didn't mean to...",
						"(You hug him tighter to let him know#it's okay.)",
						"... ... ...",
						"(He finally let's go.)",
						"Thank you... [p:2]I really needed that.",
						"I hope the bullies don't get to you too..."];
				break;
				case "sad dog2b":
					msg = ["(You pull yourself away from him.)",
						"Ah! [p:3]I got your shirt all wet...",
						"I hope the bullies don't get you too..."];
				break;
			case "sad dog1b": 
				msg = ["(You decide not to hug him.)"];
			break;
	//testing alphonse
	case "alphonse0":
		msg = ["It's a helmet. [p:2]Who could it belong to...?"];
		global.alphonse = 1;
	break;
	case "alphonse1":
		msg = [
			"Ah, [p:2]I remember now! [p:2]This is Alphonse's#helmet!#It's been a while since I've seen this!"
		]
	break;
	case "wisp":
		msg = ["Hey there!", "Is it me or are you the most human-looking#spirit I've ever seen?",
			"Let's play!"];
	break;
	case "skeleton1":
		msg = ["A skeleton...#[p:3]There's a note!",
		"#[p:7][bounce]'my nama ded'"]
	break;
	case "pot1":
		msg = ["A small pot.",
		"How on earth did this get down here #intact?!"]
	break;
	case "wallCrack1":
		msg = ["I can see stars sparkling #through this crack in the wall.",
		"It's almost as if I'm looking#at the night sky..."]
	break;
	
	//GAME EVENTS
	case "inventory full":
		msg = ["Ah, [p:2]it seems there's no more room in#my pockets."];
	break;
	
	//ITEMS
	case "Orange":
		msg = ["You got an Orange!"];
	break;

	//SAVING
	case "save":
		msg = ["You SAVED the game!"];
		scr_save();
	break;
}

global.msg = msg; //updates the current message to display
global.q = q; //updates the current question to ask if any
if (global.msg != "") {
	//global.paused = false;
	create_one(0, 0, "Instances", obj_textbox); //creates the textbox
}

if (ex == true) {
	global.paused = false;	
}