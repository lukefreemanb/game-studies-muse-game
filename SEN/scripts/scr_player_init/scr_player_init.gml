///scr_player_init()
//player stats
global.playerName = "";
global.hp = 20; //
global.hpMax = 20; //
global.money = 0; //$$$$$$$$$$$$$$$$
global.time = 0; //time spent in-game

//items
//create all the empty slots for items
global.itemMax = 10; //maximum number of items allowed (actual total -1, to account for 0)
global.item = array_create(global.itemMax*2, ""); //set up item array
//global.item = ds_list_create();
global.itemTotal = -1; //total number of items, because arrays start at 0, and we check if current pos+1 is free
global.itemNumber = 0; //which item it is

