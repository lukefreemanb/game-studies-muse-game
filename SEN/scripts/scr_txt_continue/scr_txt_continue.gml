///scr_txt_continue()
//Used to advance the phone message
if (instance_exists(obj_txt)) {
	if (instance_exists(obj_txt_ask)) {
		audio_play_sound(snd_text_send, 1, false);
	} else {
		audio_play_sound(snd_text_receive, 1, false);
	}
	with(obj_txt) {
		nCurrent++;
		done = false;
		txt = "";
		pos = 1; 
		posBegin = 1;
	}
}