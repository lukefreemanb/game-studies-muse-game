///scr_textbox_tag_map()
//Colours
scr_map_tag("white", c_white);
scr_map_tag("aqua", c_aqua);
scr_map_tag("blue", c_blue);
scr_map_tag("black", c_black);
scr_map_tag("red", c_red);
scr_map_tag("yellow", c_yellow);
scr_map_tag("lime", c_lime);
scr_map_tag("teal", c_teal);
scr_map_tag("orange", c_orange);
scr_map_tag("gray", c_gray);
scr_map_tag("fuchsia", c_fuchsia);
scr_map_tag("silver", c_silver);
scr_map_tag("green", c_green);
scr_map_tag("dkgray", c_dkgray);
scr_map_tag("custom0", make_color_rgb(55, 125, 55));

//Fonts
scr_map_tag("default", font_add_sprite(spr_font_default, ord("!"), true, 1));
//Sounds

//Images
scr_map_tag("testimage", spr_test_image);