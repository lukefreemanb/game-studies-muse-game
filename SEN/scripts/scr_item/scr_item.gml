///scr_item
///scr_item_config(name, sprite, type, description)
/*
item types:
- item (default)
- food (consumable)
- use (consumable, but not in the eaten way)

itemName = argument0;
itemSprite = argument1;
itemType = argument2;
itemDesc = argument3;
*/
item = argument0;

switch(item) {
	case "Orange":
		scr_item_config("Orange", spr_orange, "food", "An orange.\n\nHeals 4 HP.");
	break;
	case "Apple Pie":
		scr_item_config("Apple Pie", spr_orange, "food", "Still warm inside.\n\nHeals 6 HP.");
	break;
	case "Music Box":
		scr_item_config("Music Box", spr_orange, "item", "It's copper, but sounds like gold.");
	break;
}	

//return parts of the item
switch(argument1) {
	case "name":
		return itemName;
	break;
	case "sprite":
		return itemSprite;
	break
	case "type":
		return itemType;
	break;
	case "description":
		return itemDesc;
	break;
	case 0:
		exit;
	break;
}