///draw_rectangle_solid(x, y, width, height, colour, curve, outline)
_x = argument0;
_y = argument1;
_w = argument2;
_h = argument3;
_col = argument4;
_curve = argument5;
_outline = argument6;
//draw_rectangle_colour(round(_x), round(_y), _x+_w, _y+_h, _col, _col, _col, _col, true);
//draw a rectangle at (x,y) with (w,h)
draw_roundrect_colour_ext(floor(_x), floor(_y), floor(_x+_w), floor(_y+_h), _curve, _curve, _col, _col, _outline);