///scr_monster_stats
//This will handle basic monster details, including how many parts they take to animate and other details
monsterName = argument0; //name
monsterX = argument1;
monsterY = argument2;
n = argument3; //number of animated parts
applyOutline = argument4; //whether to apply outline shader