///create_one(x, y, object)
//checks to see if the object exists and only creates it if it doesn't
if (!instance_exists(argument3)) {
    instance_create_layer(argument0, argument1, argument2, argument3);
}