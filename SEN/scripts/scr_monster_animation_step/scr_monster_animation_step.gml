///scr_monster_animation_step(monster)
switch(argument0) {
	case "corgi": //CORGI
		for (i = 0; i < n; i++) {
			if (i == 1) { //base head
				//set the sprite
				_sprite[i] = spr_corgi_body;
				_xx[i] = monsterX;
				_yy[i] = monsterY+24;
				_sine[i] += 2;
				if (_sine[i] > 360) {
					_sine[i] = 0;	
				}
				_yoffset[i] = 3*cos(degtorad(_sine[i]));
			} 
			if (i == 2) {
				_sprite[i] = spr_corgi_paw_left;
				_xx[i] = monsterX-16;
				_yy[i] = monsterY+18;
				_sine[i] += 2;
				if (_sine[i] > 360) {
					_sine[i] = 0;	
				}
				_angle[i] = 6*cos(degtorad(_sine[i])); 
				_yoffset[i] = 3*cos(degtorad(_sine[i]));
			}
			if (i == 3) {
				_sprite[i] = spr_corgi_paw_right;
				_xx[i] = monsterX+16;
				_yy[i] = monsterY+18;
				_sine[i] += 2;
				if (_sine[i] > 360) {
					_sine[i] = 0;	
				}
				_angle[i] = -6*cos(degtorad(_sine[i])); 
				_yoffset[i] = 3*cos(degtorad(_sine[i]));
			}
			if (i == 4) {
				_sprite[i] = spr_corgi_paw_hindleft;
				_xx[i] = monsterX-8;
				_yy[i] = monsterY+48;
				_sine[i] += 2;
				if (_sine[i] > 360) {
					_sine[i] = 0;	
				}
				_angle[i] = 6*cos(degtorad(_sine[i])); 
				_yoffset[i] = 3*cos(degtorad(_sine[i]));
			}
			if (i == 5) {
				_sprite[i] = spr_corgi_paw_hindright;
				_xx[i] = monsterX+8;
				_yy[i] = monsterY+48;
				_sine[i] += 2;
				if (_sine[i] > 360) {
					_sine[i] = 0;	
				}
				_angle[i] = -6*cos(degtorad(_sine[i])); 
				_yoffset[i] = 3*cos(degtorad(_sine[i]));
			}
			if (i == 6) {
				_sprite[i] = spr_corgi_head;
				_xx[i] = monsterX;
				_yy[i] = monsterY;
				//trig
				_sine[i] += 2;
				if (_sine[i] > 360) {
					_sine[i] = 0;	
				}
				//yoffset
				_yoffset[i] = 3*cos(degtorad(_sine[i]));
				//angle
				_angle[i] = 6*cos(degtorad(_sine[i])); 
			}
			if (i == 7) {
				_sprite[i] = spr_corgi_tongue;
				_xx[i] = monsterX;
				_yy[i] = monsterY;
				//trig
				_sine[i] += 2;
				if (_sine[i] > 360) {
					_sine[i] = 0;	
				}
				//yoffset
				_yoffset[i] = 3*cos(degtorad(_sine[i]));
				//angle
				_angle[i] = 16*cos(degtorad(_sine[i])); 
			}
			if (i >= 1) {
				draw_sprite_ext(_sprite[i], _frame[i], _xx[i]+_xoffset[i], _yy[i]+_yoffset[i], _xscale[i], _yscale[i], _angle[i], _colour[i], _alpha[i]);
			}
		}
	break;
	case "test":
	break;
	case "wisp":
		for (i = 0; i < n; i++) {
			if (i == 1) {
				_sprite[i] = spr_wisp;	
				_xx[i] = monsterX;
				_yy[i] = monsterY;
				_sine[i] += 2;
				if (_sine[i] > 360) {
					_sine[i] = 0;	
				}
				_angle[i] = 16*cos(degtorad(_sine[i])); 
			}
			if (i >= 1) {
				draw_sprite_ext(_sprite[i], _frame[i], _xx[i]+_xoffset[i], _yy[i]+_yoffset[i], _xscale[i], _yscale[i], _angle[i], _colour[i], _alpha[i]);
			}
		}
	break;
}