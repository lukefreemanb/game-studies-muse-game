//array_merge(array1, array2)
array1 = argument0;
array2 = argument1;
finalArray = "";

for (i = 0; i < (array_length_1d(array1)); i++) {
	finalArray[i] = array1[i];
}

for (j = (array_length_1d(array1)); j < (array_length_1d(array1)+array_length_1d(array2)); j++) {
	finalArray[j] = array2[j-(array_length_1d(array1))];
}

return finalArray;