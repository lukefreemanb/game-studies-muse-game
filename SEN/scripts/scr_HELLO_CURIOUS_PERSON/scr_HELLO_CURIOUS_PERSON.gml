/*

Attention, Hackerz

...I don't know how you decompiled a GMS2 file but I guess 
if you're reading this, you managed to do it somehow. Kudos.

Well, you know the drill.

I hope your stay here is fruitful, but it'd be nice if you didn't
tell anyone about the secrets of this game. After all, games are all about
secrets, and we wouldn't want any secrets getting out now would we? 

What was the saying again? 

"Curiosity killed the..."

Anyhow, I hope you enjoy your time here and that you found 
what you were looking for. 

- D