///scr_control_config
/*
This will set the default button configuration and allow for customisation
The game's going to be designed for a handheld: PSP, Switch etc.
A Button
B Button
X Button
Y Button
Up
Right
Left 
Down
Start 
Select

gamepad only:
- p
*/

//Check if a controller is connected
if (gamepad_is_connected(0))    {
    //GAMEPAD CONTROLS
	global.gpDeadzone = .35;
    gamepad_set_axis_deadzone(0, global.gpDeadzone);
    //Direction
    global.xaxis = (gamepad_axis_value(0, gp_axislh));
    global.yaxis = (gamepad_axis_value(0, gp_axislv));
	if (global.xaxis > global.gpDeadzone) || (global.xaxis < -global.gpDeadzone) {
		if (global.xaxisReset == false) {
			global.xaxisReset = true;
		}
	}
	if (global.xaxis < global.gpDeadzone) || (global.xaxis > -global.gpDeadzone) {
		if (global.xaxisReset == true) {
			global.xaxisReset = false;	
		}
	}
	if (global.yaxis > global.gpDeadzone) || (global.yaxis < -global.gpDeadzone) {
		if (global.yaxisReset == false) {
			global.yaxisReset = true;
		}
	} 
	if (global.yaxis < global.gpDeadzone) || (global.yaxis > -global.gpDeadzone) {
		if (global.yaxisReset == true) {
			global.yaxisReset = false;	
		}
	}
	
	//Pressing buttons
	global.buttonPressedA = gamepad_button_check_pressed(0, gp_face1);
    global.buttonPressedB = gamepad_button_check_pressed(0, gp_face2);
    global.buttonPressedX = gamepad_button_check_pressed(0, gp_face3);
    global.buttonPressedY = gamepad_button_check_pressed(0, gp_face4);
	global.buttonPressedLeft = gamepad_button_check_pressed(0, gp_padl);
	global.buttonPressedUp = gamepad_button_check_pressed(0, gp_padu);
	global.buttonPressedRight = gamepad_button_check_pressed(0, gp_padr);
	global.buttonPressedDown = gamepad_button_check_pressed(0, gp_padd);
    global.buttonPressedShoulderL = gamepad_button_check_pressed(0, gp_shoulderl);
    global.buttonPressedShoulderR = gamepad_button_check_pressed(0, gp_shoulderr);
    global.buttonPressedShoulderLB = gamepad_button_check_pressed(0, gp_shoulderlb);
    global.buttonPressedShoulderRB = gamepad_button_check_pressed(0, gp_shoulderrb);
	global.buttonPressedStart = gamepad_button_check_pressed(0, gp_start);
	
	//Releasing buttons
	global.buttonReleasedA = gamepad_button_check_released(0, gp_face1);
	global.buttonReleasedB = gamepad_button_check_released(0, gp_face2);
	global.buttonReleasedX = gamepad_button_check_released(0, gp_face3);
	global.buttonReleasedY = gamepad_button_check_released(0, gp_face4);
	global.buttonReleasedLeft = gamepad_button_check_released(0, gp_padl);
	global.buttonReleasedUp = gamepad_button_check_released(0, gp_padu);
	global.buttonReleasedRight = gamepad_button_check_released(0, gp_padr);
	global.buttonReleasedDown = gamepad_button_check_released(0, gp_padd);
    global.buttonReleasedShoulderL = gamepad_button_check_released(0, gp_shoulderl);
    global.buttonReleasedShoulderR = gamepad_button_check_released(0, gp_shoulderr);
    global.buttonReleasedShoulderLB = gamepad_button_check_released(0, gp_shoulderlb);
    global.buttonReleasedShoulderRB = gamepad_button_check_released(0, gp_shoulderrb);
	global.buttonReleasedStart = gamepad_button_check_released(0, gp_start);
	
	//Holding buttons
    global.buttonA = gamepad_button_check(0, gp_face1);
    global.buttonB = gamepad_button_check(0, gp_face2);
    global.buttonX = gamepad_button_check(0, gp_face3);
    global.buttonY = gamepad_button_check(0, gp_face4);
	global.buttonLeft = gamepad_button_check(0, gp_padl);
	global.buttonUp = gamepad_button_check(0, gp_padu);
	global.buttonRight = gamepad_button_check(0, gp_padr);
	global.buttonDown = gamepad_button_check(0, gp_padd);
	global.buttonShoulderL = gamepad_button_check(0, gp_shoulderl);
	global.buttonShoulderR = gamepad_button_check(0, gp_shoulderr);
	global.buttonShoulderLB = gamepad_button_check(0, gp_shoulderlb);
	global.buttonShoulderRB = gamepad_button_check(0, gp_shoulderrb);
    global.buttonStart = gamepad_button_check(0, gp_start);
} else {
	//PC CONTROLS
	//Pressing buttons
	global.buttonPressedA = keyboard_check_pressed(ord("Z"));
	global.buttonPressedB = keyboard_check_pressed(ord("X"));
	global.buttonPressedX = keyboard_check_pressed(ord("C"));
	global.buttonPressedY = keyboard_check_pressed(ord("V"));
	global.buttonPressedLeft = keyboard_check_pressed(vk_left);
	global.buttonPressedUp = keyboard_check_pressed(vk_up);
	global.buttonPressedRight = keyboard_check_pressed(vk_right);
	global.buttonPressedDown = keyboard_check_pressed(vk_down);
	global.buttonPressedShoulderL = keyboard_check_pressed(ord("A"));
    global.buttonPressedShoulderR = keyboard_check_pressed(ord("S"));
	global.buttonPressedShoulderLB = keyboard_check_pressed(ord("Q"));
    global.buttonPressedShoulderRB = keyboard_check_pressed(ord("W"));
	global.buttonPressedStart = keyboard_check_pressed(vk_enter);

	//Releasing buttons
	global.buttonReleasedA = keyboard_check_released(ord("Z"));
	global.buttonReleasedB = keyboard_check_released(ord("X"));
	global.buttonReleasedX = keyboard_check_released(ord("C"));
	global.buttonReleasedY = keyboard_check_released(ord("V"));
	global.buttonReleasedLeft = keyboard_check_released(vk_left);
	global.buttonReleasedUp = keyboard_check_released(vk_up);
	global.buttonReleasedRight = keyboard_check_released(vk_right);
	global.buttonReleasedDown = keyboard_check_released(vk_down);
	global.buttonReleasedShoulderL = keyboard_check_pressed(ord("A"));
    global.buttonReleasedShoulderR = keyboard_check_pressed(ord("S"));
	global.buttonReleasedShoulderLB = keyboard_check_pressed(ord("Q"));
    global.buttonReleasedShoulderRB = keyboard_check_pressed(ord("W"));
	global.buttonReleasedStart = keyboard_check_released(vk_enter);

	//Holding buttons
	global.buttonA = keyboard_check(ord("Z"));
	global.buttonB = keyboard_check(ord("X"));
	global.buttonX = keyboard_check(ord("C"));
	global.buttonY = keyboard_check(ord("V"));
	global.buttonLeft = keyboard_check(vk_left);
	global.buttonUp = keyboard_check(vk_up);
	global.buttonRight = keyboard_check(vk_right);
	global.buttonDown = keyboard_check(vk_down);
	global.buttonShoulderL = keyboard_check_pressed(ord("A"));
    global.buttonShoulderR = keyboard_check_pressed(ord("S"));
	global.buttonShoulderLB = keyboard_check_pressed(ord("Q"));
    global.buttonShoulderRB = keyboard_check_pressed(ord("W"));
	global.buttonStart = keyboard_check(vk_enter);
	
	//Direction
	global.xaxis = (global.buttonRight-global.buttonLeft);
	global.yaxis = (global.buttonDown-global.buttonUp);
}

//Debugging
global.buttonPressedR = keyboard_check_pressed(ord("R"));
global.buttonPressedF = keyboard_check_pressed(ord("F"));/**/