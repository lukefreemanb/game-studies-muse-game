///string_get_width(string)
//sorts through the widths of each line of a string and returns the widest value
var i;
var n = 0;
var char = "";
var width = 4;
var length;
var charWidth = 0;
var finalLength = 0;
length[0] = "";
for (i = 1; i <= string_length(argument0); i++) {
	char = string_char_at(argument0, i);
	if (char == "\n") {
		n++;
		width = 4;
	} else {
		width += string_width(char);
		length[n] = width;
	}
}

for (j = 0; j < array_length_1d(length); j++) {
	if (finalLength < length[j]) {
		finalLength = length[j];
	}
}

return finalLength;
