///draw_rectangle_ouline(x, y, width, height, thickness, colour)
//draws a solid rectangle using 4 lines with custom thickness

_xx = argument0;
_yy = argument1;
_width = argument2; 
_height = argument3;
_weight = argument4;
_colour = argument5;

draw_set_colour(_colour);
draw_line_width(_xx, _yy, _xx+_width, _yy, _weight); //top horizontal
draw_line_width(_xx, _yy+_height, _xx+_width, _yy+_height, _weight); //bottom horizontal
draw_line_width(_xx, _yy-(_weight/2), _xx, _yy+_height+(_weight/2), _weight); //left vertical
draw_line_width(_xx+_width, _yy-(_weight/2), _xx+_width, _yy+_height+(_weight/2), _weight); //right veritcal