///scr_debug(x, y, text)
//Just displays whatever variables you feed into it as text on the screen
draw_set_alpha(.5);
draw_rectangle_colour(argument0-32, argument1-32, 480+64, array_length_1d(argument2)*16, c_black, c_black, c_black, c_black, false);
draw_set_alpha(1);
draw_set_colour(c_white);
draw_set_font(global.fontDefault);
//draw_set_font(fnt_debug);
for (i = 0; i < array_length_1d(argument2); i++) {
	draw_text(argument0, argument1+(i*12), argument2[i]);
}
