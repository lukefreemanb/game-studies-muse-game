///scr_monster_animation_setup(number_of_animated_parts)
//sets up the base values for each animated part for however many animated parts there are
for (i = 0; i < argument0; i++) {
	//set defaults - every single animated part will have these properties
	_sprite[i] = "";	//sprite
	_frame[i] = 0;		//frame
	_xx[i] = 0;			//x
	_yy[i] = 0;			//y
	_xoffset[i] = 0;	//x offset
	_yoffset[i] = 0;	//y offset
	_angle[i] = 0;		//angle	
	_xscale[i] = 1;		//xscale
	_yscale[i] = 1;		//yscale
	_colour[i] = -1;	//colour
	_alpha[i] = 1;		//alpha
	_sine[i] = 0;		//sine
	_cos[i] = 0;		//cos
	_tan[i] = 0;		//tan
	_amp[i] = 1;		//amplitude
}