///scr_delete(object)
//just checks to see whether an object exists before deleting it.
if (instance_exists(argument0)) {
    with(argument0) instance_destroy();
}
