///string_get_height(string)
//returns the number of lines in a string
var i;
var n = 1; //length of a text has to start at 1 line, not 0
var char = "";
for (i = 1; i <= string_length(argument0); i++) {
	char = string_char_at(argument0, i);
	if (char == "\n") {
		n++;
	} 
}

return n;