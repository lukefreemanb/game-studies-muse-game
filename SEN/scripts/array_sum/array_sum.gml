///array_sum(array)
var sum = 0;
var array = argument0;
var length = argument1;
for (var i = 0; i < length; i++) {
	sum += array[i];
}

return sum;