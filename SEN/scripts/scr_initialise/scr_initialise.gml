///scr_initialise
//Sets up all the global variables
//Global game variables
randomise();
//SAVING
global.save = ds_map_create(); //create the ds map for the save
ds_map_replace(global.save, "Current Room", room_get_name(room)); //
global.saveFile = "save.sav"; //create the save file
global.paused = false; //if game is paused

//Textbox variables
global.msg = ""; //message
global.q = ""; //question
global.answer = ""; //answer
global.fontDefault = font_add_sprite(spr_font_default_serif, ord(" "), true, 1);
global.fontDefaultCentred = font_add_sprite(spr_font_default_serif_centred, ord("!"), true, 1);
global.fontDefaultShort = font_add_sprite(spr_font_default_short, ord("!"), true, 1);
global.tbFont = global.fontDefaultCentred;
global.tagMap = ds_map_create();
global.tbX = 16; //default : 16
global.tbY = 176; //default : 176
global.tbW = 336; //default : 336
global.tbH = 72; //default : 64
global.tbFontDefault = global.fontDefaultCentred;
global.tbXDefault = global.tbX;
global.tbYDefault = global.tbY;
global.tbWDefault = global.tbW;
global.tbHDefault = global.tbH;

//Texting app variables
//global.txtMsg = "";
global.txtMsg = noone;
global.txtQ = "";
global.txtAnswer = "";

//screenshake
global.shakeX = 0; 
global.shakeY = 0;
ss = 0; //screenshake delay

//room transitions
global.currentRoom = room;
global.targetRoomX = 0;
global.targetRoomY = 0;
global.targetRoom = "";
global.currentMusic = "";

//Encounters
global.encounterMonster = "corgi"; //which monster is being encountered

//Controls
//Direction
global.xaxis = "";
global.yaxis = "";
global.xaxisReset = false;
global.yaxisReset = false;

//Pressing buttons
global.buttonPressedA = "";
global.buttonPressedB = "";
global.buttonPressedX = "";
global.buttonPressedY = "";
global.buttonPressedLeft = "";
global.buttonPressedUp = "";
global.buttonPressedRight = "";
global.buttonPressedDown = "";
global.buttonPressedShoulderL = "";
global.buttonPressedShoulderR = "";
global.buttonPressedShoulderLB = "";
global.buttonPressedShoulderRB = "";
global.buttonPressedStart = "";
	
//Releasing buttons
global.buttonReleasedA = "";
global.buttonReleasedB = "";
global.buttonReleasedX = "";
global.buttonReleasedY = "";
global.buttonReleasedLeft = "";
global.buttonReleasedUp = "";
global.buttonReleasedRight = "";
global.buttonReleasedDown = "";
global.buttonReleasedShoulderL = "";
global.buttonReleasedShoulderR = "";
global.buttonReleasedShoulderLB = "";
global.buttonReleasedShoulderRB = "";
global.buttonReleasedStart = "";
	
//Holding buttons
global.buttonA = "";
global.buttonB = "";
global.buttonX = "";
global.buttonY = "";
global.buttonLeft = "";
global.buttonUp = "";
global.buttonRight = "";
global.buttonDown = "";
global.buttonShoulderL = "";
global.buttonShoulderR = "";
global.buttonShoulderLB = "";
global.buttonShoulderRB = "";
global.buttonStart = "";

//testing - game states
global.alphonse = 0;

//view
global.viewFollowing = obj_player;
view_camera[0] = camera_create_view(0, 0, 320, 240, 0, global.viewFollowing, -1, -1, 160, 120);

//Run the initialise scripts
scr_player_init(); //player variables
scr_textbox_tag_map(); //sets up textbox tag map
//Initiate
create_one(0, 0, "Instances", obj_overworld_menu);
if (room == rm_initiate) {
	room_goto_next();	
}