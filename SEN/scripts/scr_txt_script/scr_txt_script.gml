///scr_txt_script(identifier)
//Dialogue
msgPrevious = global.txtMsg;
q = ""; //a string used to identify the question 
switch(argument0) {
	//test
	case "test":
		msg = [
			"Greetings.[p:2]", "Welcome to the game.[p:5]", "I hope you enjoy your stay.[p:5]",
			"Are you ready to proceed?[q:Yes - I'm ready,No - I'm not ready]"
		]
		q = "test";
	break;
	case "testing": 
		msg = [
			"Greetings.. [p:18]", "And Welcome.. [p:18]", "I want to play a game..[p:18]",
			"CHESS MOTHERFUCKER!!! [p:22]", "Oh. [p:18]", "You're wondering how I know who you are? [p:18]",
			"Well, let's just say you've already\nmade quite the name for yourself... [p:18]", "It seems you've already demonstrated\nquite the affinity for...violence. [p:18]", 
			"Enough smalltalk though.\nYou must be wondering why you\nwoke up in an empty white room\nby yourself. [p:24]", 
			"Your questions will be answered soon,\nbut for now, I need you to listen to me.[p:18]",
			"Put on the mask and exit\nthrough the door in front of you[p:18]",
			"I'm going to unlock the door now\nAre you ready?[q:Yes - I'm ready,No - I'm not ready]"
		]
		//q = "test";
	break;
		case "testa": 
			msg = [
				"Great![p:5]",
				"Take a few second to familiarise\nyourself with your surroundings.[p:5]",
				"I hope it doesn't seem too...\nbizarre.[p:5]",
				"Let me know when you're ready\nto move on.[q:I'm ready,I need a few more moments]"
			];
			q = "test1";
		break;
			case "test1a":
				msg = [
					"Fantastic.[p:5]",
					"See that bat on the floor?[p:5]",
					"I need you pick it up[p:5]",
					"Text me back when you've\nequipped it."
				]
			break;
			case "test1b":
				msg = [
					"A person that's not curious\nabout their own whereabouts?[p:5]",
					"How curious...[p:7]",
					"How curious indeed...\n....\n...\nbut seriously, though?"
				]
			break;
		case "testb": 
			msg = [
				"Hurry up and get ready then.[p:3]",
				"We don't have all day."
			]
		break;
		
}

global.txtMsg = array_merge(msgPrevious, msg);
global.txtQ = q; //updates the current question to ask if any
