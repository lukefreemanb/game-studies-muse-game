///string_split(string, separator)
//splits a string into an array divided by ","
//so basically, msg = string_split("hello,world") becomes:
//msg[0] = "hello" and msg[1] = "world". pretty neat for questions with multiple answers!
var answer;
var n = 0;
var char;
var i;

answer[0] = "";
for(i = 1; i <= string_length(argument0); i++){
    char = string_char_at(argument0, i);
    if(char == argument1){
        n++;
        answer[n] = "";
    } else{
        answer[n] += char;
    }
}

return answer;