///scr_load
//check to see if a save file exists
//tbh this shouldn't even be run if there's no save file existant...
if (!file_exists(global.saveFile)) {
	exit; //if it doesn't, exit
}
//if it does, then load
ds_map_destroy(global.save);
global.save = ds_map_secure_load(global.saveFile); //load data from stored ds map
save = global.save;
//load everything back in if a save file exists
global.name = ds_map_find_value(save, "name");
global.hp = ds_map_find_value(save, "hp");
global.hpmax = ds_map_find_value(save, "hpmax");
global.money = ds_map_find_value(save, "money");
global.time = ds_map_find_value(save, "time");

//checks to see where the room you're meant to be loaded into is
var _room = ds_map_find_value(save, "Current Room");
if (!is_undefined(_room) && room_get_name(room) != _room) {
	room_goto(asset_get_index(_room)); 	
} else {
	room_restart();
} 