///scr_save
save = global.save;
ds_map_replace(save, "name", global.name);
ds_map_replace(save, "hp", global.hp);
ds_map_replace(save, "hpmax", global.hpmax);
ds_map_replace(save, "money", global.money);
ds_map_replace(save, "time", global.time);
for (i = 0; i < global.itemMax; i++) {
	ds_map_replace(save, "item"+string(i), global.item[i]);	
}
ds_map_secure_save(global.save, global.saveFile);