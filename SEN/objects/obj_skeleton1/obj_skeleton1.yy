{
    "id": "8784b016-7eab-4f86-92c6-40f77bb99062",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_skeleton1",
    "eventList": [
        {
            "id": "9058eb4f-f9cb-453f-b9ac-2856435f09b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8784b016-7eab-4f86-92c6-40f77bb99062"
        },
        {
            "id": "e7b47b5a-dee3-405a-8de9-755714e0e53a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8784b016-7eab-4f86-92c6-40f77bb99062"
        },
        {
            "id": "1eb57904-56e7-4700-b3a3-456f09ba0054",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8784b016-7eab-4f86-92c6-40f77bb99062"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "12f49012-6f3c-4640-b58e-43fb421b9a13",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "be3b2c01-69b0-46db-b404-987dadacd460",
    "visible": true
}