{
    "id": "7da0627e-7010-475f-ac5e-7c202e043152",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_global",
    "eventList": [
        {
            "id": "26e47bc7-9cef-45a1-be27-471cfec7c7e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7da0627e-7010-475f-ac5e-7c202e043152"
        },
        {
            "id": "70c77b4d-5008-4349-84e5-3c5882f22b63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7da0627e-7010-475f-ac5e-7c202e043152"
        },
        {
            "id": "0910e8f0-a95b-4aed-9490-f1a6961e4863",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7da0627e-7010-475f-ac5e-7c202e043152"
        },
        {
            "id": "02115fac-443a-49a9-8539-3496679ab85d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "7da0627e-7010-475f-ac5e-7c202e043152"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}