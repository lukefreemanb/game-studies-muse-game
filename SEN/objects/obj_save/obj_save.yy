{
    "id": "8382a097-7a1a-4a3b-bfcd-8ffdb5d2aaec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_save",
    "eventList": [
        {
            "id": "86718656-7a08-4b14-bdb4-653d689a582e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8382a097-7a1a-4a3b-bfcd-8ffdb5d2aaec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "12f49012-6f3c-4640-b58e-43fb421b9a13",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9c69316d-03ca-4aa8-823b-fd63603e5743",
    "visible": true
}