//FADING
if (state == 0) {
    if (a < 1)  a += .1;
}

if (a > .99)    {
    state = 1;
    obj_player.x = targetX;
    obj_player.y = targetY;
    room_goto(targetRoom);
}

if (state == 1) {
    a -= .1;
}

if (a < 0.05) && (state == 1) {
    global.paused = 0;
    instance_destroy();
}
