{
    "id": "289abb2f-7813-489f-ac88-7ceb84bf13f8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_transition_fade",
    "eventList": [
        {
            "id": "fa4e425a-6575-4dd8-850f-ef9a803d6d8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "289abb2f-7813-489f-ac88-7ceb84bf13f8"
        },
        {
            "id": "35e8c9c6-43a0-409e-97bb-50ed27b4500d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "289abb2f-7813-489f-ac88-7ceb84bf13f8"
        },
        {
            "id": "499f32d3-5b38-45f2-8f8d-bee533d78844",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "289abb2f-7813-489f-ac88-7ceb84bf13f8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}