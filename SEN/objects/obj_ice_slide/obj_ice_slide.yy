{
    "id": "e9abe966-1c99-445a-b149-0768dfe1ad22",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ice_slide",
    "eventList": [
        {
            "id": "ee2a795e-de6d-410c-b912-7f82e29a23f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e9abe966-1c99-445a-b149-0768dfe1ad22"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2c66c96a-e7d4-4e24-8463-15201ccd4f45",
    "visible": false
}