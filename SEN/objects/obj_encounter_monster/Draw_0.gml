///Draw teh surface
if (surface_exists(surf)) {
	//apply outline shader if it's on, else do not
	if (applyOutline == true) {
		shader_set(sh_outline);
		shader_set_uniform_f(upixelW, texelW);
		shader_set_uniform_f(upixelH, texelH);
		draw_surface_ext(surf, 0, 0, 1, 1, 0, -1, 1);	
		shader_reset();
	} else {
		draw_surface_ext(surf, 0, 0, 1, 1, 0, -1, 1);	
	}
}