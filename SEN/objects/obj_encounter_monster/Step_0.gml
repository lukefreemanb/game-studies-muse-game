depth = -10;
//refresh surface
if (!surface_exists(surf)) {
	surf = surface_create(room_width, room_height);	
}

//Draw self onto surface and ANIMATE
surface_set_target(surf);
draw_clear_alpha(c_black, 0);
scr_monster_animation_step(global.encounterMonster);
//draw_sprite_ext(spr_hand, 0, mouse_x, mouse_y, 1, 1, 0, -1, 1);
surface_reset_target();