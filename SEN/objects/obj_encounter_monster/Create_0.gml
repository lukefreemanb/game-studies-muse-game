//load in all the relevant data to the current monster!
scr_monster_create_setup(global.encounterMonster); 
scr_monster_animation_setup(n); //n = number of animated parts
scr_monster_animation_create(global.encounterMonster);

//surface
surf = surface_create(room_width, room_height); //create own surface

//shader
upixelW = shader_get_uniform(sh_outline, "pixelW");
upixelH = shader_get_uniform(sh_outline, "pixelH");
texelW = texture_get_texel_width(surface_get_texture(surf));
texelH = texture_get_texel_height(surface_get_texture(surf));
