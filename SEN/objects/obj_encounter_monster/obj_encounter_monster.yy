{
    "id": "32768ccf-8c8a-40c8-99eb-09410568a721",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_encounter_monster",
    "eventList": [
        {
            "id": "b5b21c9f-83d6-4475-b6c0-4cdbf5a0771c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "32768ccf-8c8a-40c8-99eb-09410568a721"
        },
        {
            "id": "846aeda4-38b8-4bc4-9a19-f99b40bd6330",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "32768ccf-8c8a-40c8-99eb-09410568a721"
        },
        {
            "id": "1ec7bc55-bbf6-4070-80f8-a97afa7891f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "32768ccf-8c8a-40c8-99eb-09410568a721"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}