{
    "id": "0e98f34b-fd0d-4597-94cf-2361315a4f9a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_pot1",
    "eventList": [
        {
            "id": "5cc79420-a6d3-46d3-ba9d-97cc85d242c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0e98f34b-fd0d-4597-94cf-2361315a4f9a"
        },
        {
            "id": "2b073e9b-d81c-420b-b9b5-e583c7475ac8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0e98f34b-fd0d-4597-94cf-2361315a4f9a"
        },
        {
            "id": "0ea915ff-081a-4bef-96be-7b660eb7916c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0e98f34b-fd0d-4597-94cf-2361315a4f9a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "12f49012-6f3c-4640-b58e-43fb421b9a13",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c45831a7-1bd3-4e45-b10b-ec7cd877bf76",
    "visible": true
}