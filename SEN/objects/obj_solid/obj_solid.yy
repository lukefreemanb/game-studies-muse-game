{
    "id": "1008eda3-472f-4183-8d8b-41630d8bdda4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_solid",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fbe19f1d-450c-4d5e-b9fd-135f87f3c06c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "89355b56-5574-4dcb-abdc-58633ceab21c",
    "visible": false
}