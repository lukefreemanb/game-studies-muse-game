{
    "id": "b763bb88-9440-4f85-b2ac-b7a55a36a4fc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_txt_ask",
    "eventList": [
        {
            "id": "79311311-b4cd-4bfe-bf96-8233fc46f4ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b763bb88-9440-4f85-b2ac-b7a55a36a4fc"
        },
        {
            "id": "965bc007-85d9-4bc5-ad6b-19022b61fd5d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b763bb88-9440-4f85-b2ac-b7a55a36a4fc"
        },
        {
            "id": "840146bb-9443-41db-9cb0-78fd0d3e58bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b763bb88-9440-4f85-b2ac-b7a55a36a4fc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}