//scrolling through the options
if (global.buttonPressedUp) && (select > 0) {
	audio_play_sound(snd_text_type, 1, false);
    select--;
}

if (global.buttonPressedDown)&&(select < nTotal) {
	audio_play_sound(snd_text_type, 1, false);
    select++;
}

msgWidth = string_width(string(answer[select]));
//selecting an answer
if (global.buttonPressedA) {
    switch(select) {
        case 0: q += "a"; break;
        case 1: q += "b"; break;
        case 2: q += "c"; break;
        case 3: q += "d"; break;
        case 4: q += "e"; break;
    }
    instance_destroy(); //delete self
	msg = [string(answer[select])];
	global.txtMsg = array_merge(msgPrevious, msg);
	with(obj_txt) {
		isReceiving = false;
		receivingTimer = 60;
		//Set the receiving messages to true
		msgReceiving[nCurrent] = isReceiving;
	}
    scr_txt_script(q); //finds the relevant section in the script depending on the answer
}