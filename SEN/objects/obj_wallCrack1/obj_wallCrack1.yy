{
    "id": "83038bab-2daf-4a57-8719-e2e64718762b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wallCrack1",
    "eventList": [
        {
            "id": "64400e0d-94f1-4520-b0ae-8498426c9fa0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "83038bab-2daf-4a57-8719-e2e64718762b"
        },
        {
            "id": "163276d7-089d-40ce-9a33-1b2897568799",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "83038bab-2daf-4a57-8719-e2e64718762b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "12f49012-6f3c-4640-b58e-43fb421b9a13",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "beb345b1-8608-4737-9316-7d15d9a9a60c",
    "visible": true
}