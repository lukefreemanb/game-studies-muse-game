{
    "id": "3f50a851-c0ed-40e4-a9ff-27d7e5232a30",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rock",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fbe19f1d-450c-4d5e-b9fd-135f87f3c06c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a96217ba-2cb0-4592-9ba6-45c7070eae33",
    "visible": true
}