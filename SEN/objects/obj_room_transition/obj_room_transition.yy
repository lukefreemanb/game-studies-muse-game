{
    "id": "d79a76f4-fc88-47d6-a1fb-0c20aaf0a2ce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_room_transition",
    "eventList": [
        {
            "id": "a2e5d4db-be9f-4773-9174-4f2850ef7afb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "51af49e7-eb94-404f-bbb3-e042a6f4255f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d79a76f4-fc88-47d6-a1fb-0c20aaf0a2ce"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "042780f4-c802-457f-a0f2-c7aa7d008a49",
    "visible": false
}