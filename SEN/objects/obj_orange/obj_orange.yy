{
    "id": "014cf6ad-aa1f-40d3-8489-d0f02b282713",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_orange",
    "eventList": [
        {
            "id": "94b2f44b-c4cd-4c40-9d25-e3b373501ffd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "014cf6ad-aa1f-40d3-8489-d0f02b282713"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f4ebfef8-6335-41e5-b671-0eae9f7b71de",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "451a628b-b992-4640-9601-0711f2512d20",
    "visible": true
}