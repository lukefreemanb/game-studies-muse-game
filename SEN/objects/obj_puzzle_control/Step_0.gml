/// @description Global Puzzle code

if (room == rm_puzzle) {
	#region Puzzle controlles
		// controlles
		var p_left = (keyboard_check_pressed(vk_left));
		var p_right = (keyboard_check_pressed(vk_right));
		var p_up = (keyboard_check_pressed(vk_up));
		var p_down = (keyboard_check_pressed(vk_down));
		var p_enter = (keyboard_check_pressed(vk_space));
		var p_debug = (keyboard_check_pressed(ord("P")));
		
		if (p_left) then puzzle_control_pos_x --;
		if (p_right) then puzzle_control_pos_x ++;
		if (p_up) then puzzle_control_pos_y --;
		if (p_down) then puzzle_control_pos_y ++;
		if (p_debug) then puzzle_debug = !puzzle_debug;
		
		// Changing selected 
		if (p_enter) {
			var xx = (puzzle_control_pos_x);
			var yy = (puzzle_control_pos_y);
			// Changes all the ds_grid spots around it
			ds_grid_set(puzzle_grid, xx, yy, !ds_grid_get(puzzle_grid, xx, yy));
			ds_grid_set(puzzle_grid, xx - 1, yy, !ds_grid_get(puzzle_grid, xx - 1, yy));
			ds_grid_set(puzzle_grid, xx + 1, yy, !ds_grid_get(puzzle_grid, xx + 1, yy));
			ds_grid_set(puzzle_grid, xx, yy - 1, !ds_grid_get(puzzle_grid, xx, yy - 1));
			ds_grid_set(puzzle_grid, xx, yy + 1, !ds_grid_get(puzzle_grid, xx, yy + 1));
		}
		
		// So that you cant select past the puzzle limit
		puzzle_control_pos_x = clamp(puzzle_control_pos_x, 0, grid_size - 1);
		puzzle_control_pos_y = clamp(puzzle_control_pos_y, 0, grid_size - 1);
	#endregion
	
	#region Puzzle Complete
		if (puzzle_one) {
			show_message("Nigga ya did it");
		}
	#endregion
	
	#region checking puzzle
		if (room == rm_puzzle) {															// Checking room
			if (!puzzle_one) {																// Check puzzle isnt completed
				for (var i = 0; i < 10; i ++) {												// If the current slot is wrong, it breaks the loop,
					if (i == 9) {															// so the only way that it can get to i = 9, is when
						puzzle_one = (true);												// the puzzle is correct and hasn't been broken when
					} else {																// been checked.
						var l = (0);														// Sets the var used for the array back to 0 after loop
						for (var j = 0; j < grid_size; j ++) {								// sets the x grid check		
							for (var k = 0; k < grid_size; k ++) {							// sets the y grid check		
								puzzle_player_anwser[l] = ds_grid_get(puzzle_grid, j, k);	// loading the puzzle awnser into the array														
								l++;														// add +1 to the var for the array		
							}																//
						}																	//
																							//															
						if (puzzle_player_anwser[i] != puzzle_one_anwser[| i]) {			//						
							break;															//
						}																	// This is where it breaks the loop if the puzzle is wrong														
					}																		// after lading all the arrays
				}
			}
		}
	#endregion
}


