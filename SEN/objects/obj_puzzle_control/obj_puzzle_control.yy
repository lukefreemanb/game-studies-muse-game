{
    "id": "ce3e69ec-6989-45b6-9d3b-fa83b5a27514",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_puzzle_control",
    "eventList": [
        {
            "id": "7728bb74-d2bd-440f-a7ab-156cb28bc571",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ce3e69ec-6989-45b6-9d3b-fa83b5a27514"
        },
        {
            "id": "1e02e07b-6fd0-462b-bd9c-69724b691f8b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ce3e69ec-6989-45b6-9d3b-fa83b5a27514"
        },
        {
            "id": "036000ae-8471-4c03-a468-6b598637e5e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "ce3e69ec-6989-45b6-9d3b-fa83b5a27514"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}