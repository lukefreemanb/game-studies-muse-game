/// @description Global puzzle vars

#region README
/*
	Put all the vars you want to create that are consistant, create itin this object as a normal var,
	under 'Puzzle refrence vars', NOT as a global var. 
	
	For example if you need a var to remember if the puzzle has been completed;
		puz_room_one_comp = (false);
	Then you can reference it by;
	
		var puz_one = (obj_puzzle_control.puz_room_one_comp);
		if (puz_one) {
			// Can go to through door;
		} else {
			// Show message that you need to do puzle
		}
*/
#endregion

#region Puzzle refrence vars
	puzzle_one = (false);
	
#endregion

#region puzzle_combo
	puzzle_one_anwser = ds_list_create();
	ds_list_add(puzzle_one_anwser,
					0, 1, 0,
					1, 1, 1,
					0, 1, 0);
					
#endregion	

#region puzzle vars 
	puzzle_clear = (false);
	puzzle_control_pos_x = (0);
	puzzle_control_pos_y = (0);
	
	puzzle_debug = (true);
	
	puzzle_player_anwser[0] = (0);
	
#endregion

#region Grid setup
	// Grid for puzzle
	puzzle_grid = ds_grid_create(3, 3);
	
	// Prepping grid 
	grid_size = (3);
	
	// i = x
	// j = y
	for (var i = 0; i < grid_size; i ++) {
		for (var j = 0; j < grid_size; j ++) {
			ds_grid_set(puzzle_grid, i, j, 0);
		}
	}


#endregion

