/// @description Drawing puzzle debug

 if (puzzle_debug) {
	 var xx = (puzzle_control_pos_x);
	 var yy = (puzzle_control_pos_y);
	 var _col;
	 
	 draw_text(35, 30, string(xx) + "," + string(yy));
	 draw_text(35, 45, ds_grid_get(puzzle_grid, xx, yy));
	 
	 for (var k = 0; k < 9; k ++) {
		draw_text(55, 30 + (k * 15), puzzle_player_anwser[k]); 
		draw_set_color(c_red);
		draw_text(70, 30 + (k * 15), puzzle_one_anwser[| k]);
		draw_set_color(c_white);
	 }
	 var l = 0
	 for (var i = 0; i < grid_size; i ++) {
		for (var j = 0; j < grid_size; j ++) {
			if (i == xx and j == yy) then _col = (c_aqua) else _col = (c_white);		// Checking colour
			draw_set_color(_col);														// Setting colour
			draw_text(90 + (i * 15), 30 + (j * 15), ds_grid_get(puzzle_grid, i, j));
			draw_set_color(c_red);
			draw_text(90 + (i * 15), 80 + (j * 15), puzzle_one_anwser[| l]);
			draw_set_color(c_white);
		}
	}	
 }

 //Small scale selecting (test);
 