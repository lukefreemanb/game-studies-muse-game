{
    "id": "99c81e5b-e192-49a5-a142-5261353000be",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tile_puzzle",
    "eventList": [
        {
            "id": "ef57c583-b521-4066-9cc5-d115515bc958",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "99c81e5b-e192-49a5-a142-5261353000be"
        },
        {
            "id": "a6a2eda8-11bc-4d2f-825b-b3baf02d2e90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "99c81e5b-e192-49a5-a142-5261353000be"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "12f49012-6f3c-4640-b58e-43fb421b9a13",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "535f937e-1e6d-453f-a659-d9f84c70f329",
    "visible": true
}