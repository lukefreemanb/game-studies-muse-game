///Step
stringLength = string_length(msg[n]); //string length of current message

if (!paused) && (!done) {
    if (!first) timer += scrollSpeed;
    while(timer > 1 || first) {
        timer--; 
        posBegin = pos; 
        if (!first) pos++; 
        if (timer < 0) timer = 0; 
        first = false;
        char = string_char_at(msg[n], pos); 
        //skip spaces and the "#" sign
        if (char == " ") || (char == "#") {
            pos++; 
            char = string_char_at(msg[n], pos);
        }
        //TAGS (non-visual)
        if (char == "[") { 
            i = pos;
            char2 = ""; 
            while(char2 != "]") { 
                i++; 
                char2 = string_char_at(msg[n], i);
            }
            diff = (i-pos)+1; 
            tag = string_copy(msg[n], pos, diff); 
            //Text speed
            if (string_count("[s:", tag)) {
                s = string_copy(tag, 4, (diff-4));
                scrollSpeed = real(s);
            }
            //Pausing
            if (string_count("[p:", tag)) {
                p = real(string_digits(tag));
                pauseTimer = (p*10);
                paused = true; //pause
            }
            //Sound (for the text blip)
            if (string_count("[z:", tag)) {
                z = string_copy(tag, 4, (diff-4)); //set the new sound
                //tbs = tb.mapsound[?z];
            }
            //SFX
            if (string_count("[ssfx:", tag)) {
                ssfx = string_copy(tag, 7, (diff-7)); //set the new sound
                //placeholder, play a once off sound effect that's not the text blip
            }
            //Music
            if (string_count("[m:", tag)) {
                m = string_copy(tag, 4, (diff-4));
            }
			//Questions
            if (string_count("[q:", tag)) {
                q = string_copy(tag, 4, (diff-4));
                answer = string_split(q, ",");
                global.answer = answer;
                create_one(0, 0, "Instances", obj_ask);
            }
			//Set where textbox should be
			if (string_count("[setXY:",tag)) {
				d = string_copy(tag, 8, (diff-8));
				xy = string_split(d, ","); //format: (x, y)
				global.tbX = real(string_digits(xy[0]));
				global.tbY = real(string_digits(xy[1]));
			}
			//Set the size of the textbox
			if (string_count("[setWH:",tag)) {
				d = string_copy(tag, 8, (diff-8));
				wh = string_split(d, ","); //format: (w, h)
				global.tbW = real(string_digits(wh[0]));
				global.tbH = real(string_digits(wh[1]));
			}	
            //Variables
			if (string_count("[var:",tag)) {
				variable = string_copy(tag, 6, (diff-6));
			}
            pos += diff; //add the difference of the  tags
            first = true;
        }
		//Plays the voice soundbyte
		voiceTimer++; 
	    if (voiceTimer > 1) && (!skipping) {
	        if (pos < string_length(msg[n])) {
	            audio_play_sound(voice, 10, false);
	            voiceTimer = 0;
	        }
	    }
        //Buffer the text for the draw event (makes sure the tags aren't drawn)
        if (pos > stringLength) done = 1; //when text finished scrolling
        drawpos += pos-posBegin;
        if (pos <= stringLength+1) txt += string_copy(msg[n], posBegin, pos-posBegin); 
    }
}

//pause timer
if (pauseTimer > 0) {
	pauseTimer--;	
}
//reset pause timer, set paused to false, resume text
if (pauseTimer < 1) {
	pauseTimer = 0;
	paused = false;
}
nTotal = array_length_1d(msg); 

//Next message         
if (done == true) { //if not done, continue
	if (global.buttonPressedA) {
		if (n < nTotal-1) {
			scr_textbox_continue();
		} else { //if done, delete and reset to defaults
			global.paused = false;
			scr_textbox_default();
			instance_destroy();
		}
	}
}


//Skipping
if (global.buttonPressedB) {
	skipping = true; 
	scrollSpeed = 1+(stringLength-pos); //so the +1 actually fixes a glitch... by skipping one of the text left
}
if (skipping) {
	pauseTimer = 0;	
}
//limit to not skip beyond possible text
if (pos > stringLength) pos = stringLength;

