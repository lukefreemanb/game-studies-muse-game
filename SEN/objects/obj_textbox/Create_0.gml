///Initialise
global.paused = true; //pause the game
paused = false; //whether paused or not (this is for delays in displaying text, not actual game pausing)
pauseTimer = 0; //pause timer

//drawing the text
timer = 0; //timer
scrollSpeed = .5; //text scroll speed
pos = 1; //position 
posBegin = 1; //position begin
drawpos = 1; //draw position
txt = ""; //txt
done = false; //whether current message is completed or not
first = true; //first run 
xx = global.tbX; //x position of current letter
yy = global.tbY; //y position of current letter
tbW = global.tbW; //textbox width
tbH = global.tbH; //textbox height
font = global.tbFont; //textbox font
question = false; //whether or not you're being asked a question
scaleX = 1; //xscale of text
scaleY = 1; //yscale of text
angle = 0; //angle of text

//text fx
sine = 0; //trig, woohoo
amp = 1; //amplitude
xoff = 0; //xoffset 
yoff = 0; //yoffset
voice = snd_txt1; //soundbyte to play when scrolling
voiceTimer = 0;
twitch = false;
twitchTimer = irandom_range(60, 120);
twitchTimer2 = 0;
//Skipping text
skipping = 0;

//selecting
canpress = 1; //so you can't spam menu options
msg = global.msg; //set the msg to the current msg
n = 0; //current message
nTotal = array_length_1d(msg); //retrieves the array length of msg
stringLength = "";

depth = -10000;
