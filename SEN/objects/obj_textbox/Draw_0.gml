///draw the textbox
xx = global.tbX; 
yy = global.tbY;

//Set everything back to the default every new message
draw_set_colour(c_white);
ripple = false;
wave = false;
shake = false;
tremble = false;
bounce = false; 
twitch = false;
scaleText = false;
scaleX = 1;
scaleY = 1;
draw_rectangle_solid(global.vx-8, global.vy+yy-8, global.tbW, global.tbH, c_black, false, false);
//draw_rectangle_colour(global.vx-8, global.vy+yy-8, global.vx+global.tbW, global.vy+yy+global.tbH, c_black, c_black, c_black, c_black, false);
padding = 2;
//draw_rectangle_outline(xx+(padding) , yy-8+(padding), tbW-(padding*2)-1, tbH-(padding*2)-1, 2, c_white);

//Scroll the text
for(i = 1; i < drawpos; i++) {
    char = string_char_at(txt, i);
    if (char == "#") { //skipping lines
        xx = global.tbX;
        yy += ceil(string_height("I"))+2; //add the height of the tallest character
    }
    //Check for tags
    if (char == "[") {
        a = i;
        char2 = "";
        while(char2 != "]") { 
            a++; //add 1 to a as long as "]" is not read
            char2 = string_char_at(txt, a);
        }
        diff = (a-i); //used for ignoring the "[", "]" and tags
        tag = string_copy(txt, i, diff+1); //get the tag from the current text
        
        //Get colour tag
        if (string_count("[c:", tag)) {
            c = string_copy(tag, 4, (diff-4) + 1);
            colour = global.tagMap[?c];
            draw_set_colour(colour); //set the colour to the retrieved colour
        }
        //Get font tag
        if (string_count("[f:", tag)) {
            f = string_copy(tag, 4, (diff-4) + 1);
            font = global.tagMap[?f];
            draw_set_font(font); //set the font to the retrieved font
        }
        //Text FX
        //Check for text effects
        if (string_count("[ripple", tag))  ripple = true;
        if (string_count("[bounce", tag))  bounce = true;
        if (string_count("[shake", tag))   shake = true;
        if (string_count("[tremble", tag)) tremble = true;
        if (string_count("[wave", tag))    wave = true;
		if (string_count("[twitch", tag))  twitch = true;
		if (string_count("[scale", tag)) scaleText = true; 
        //Clear
        if (string_count("[x", tag)) {
            ripple = false;
            bounce = false;
            shake = false;
            tremble = false;
            wave = false;
			twitch = false;
			scaleText = false;
        }
        i += diff; //skip the tag, including the "[" and "]" as if it were never there
    }
    //Text effects 
    if (char != "[") && (char != "#") {
	    xoff = 0;
		yoff = 0;
		if (scaleText) {
			scaleX = random_range(.9, 1.1);
			scaleY = random_range(.9, 1.1);
		}
        //Ripple 
        if (ripple) {
            yoff = -sin(degtorad(sine-xx));
        }
        //Bounce 
        if (bounce) {
            yoff = -abs(-sin(degtorad(sine-xx)));
        }
        //Shake 
        if (shake) { 
            xoff = round(random_range(-1, 1));
            yoff = round(random_range(-1, 1));
        }
        //Tremble
        if (tremble) { 
            xoff = round(random_range(0, 1));
            yoff = round(random_range(0, 1));
        }
        //Wave
        if (wave) {
            xoff = -sin(degtorad(sine-xx));
            yoff = xoff;
        }
		//Twitch
		if (twitch) {
			twitchTimer--;
			if (twitchTimer < 3 && twitchTimer >= 1) {
				twitchAmp = 1;
				twitchRand = choose(0, 1, 2, 3);
				if (twitchRand == 0) xoff = (1*twitchAmp); 
				if (twitchRand == 1) xoff = (-1*twitchAmp);
				if (twitchRand == 2) yoff = (1*twitchAmp);
				if (twitchRand == 3) yoff = (-1*twitchAmp);
			}
			if (twitchTimer < 1) {
				twitchTimer = irandom_range(600, 1200);
			}
		}
        //Draw the text
		draw_set_font(font); //set the font
		draw_set_halign(fa_left); //set the horizontal al ignment
        draw_text_transformed(global.vx+xx+xoff+12, global.vy+yy+yoff+10, char, scaleX, scaleY, angle); //draw the text
        if (char != " ") {
            xx += floor(string_width(char)); //How far apart to space each character
        } else { 
            xx += floor(.5*string_width(char)); //spaces are too wide so i made them take up half as much space, literally
        }
    }
}

//Trig - for the text effects (not really trig, gm does all the trig for us lmao)
sine += 8;
if (sine > 360)  sine -= 360;

/*
//DEBUGGING
debugArray = [
	string(pos) + "/" + string(stringLength),
	"current msg: " + string(msg[n]),
	"n: " + string(n) + "/" + string(nTotal),
	"txt : " + string(txt), 
	"skipping:" + string(paused),
	string(pauseTimer)
]
scr_debug(global.vx+8, global.vy+8, debugArray);

