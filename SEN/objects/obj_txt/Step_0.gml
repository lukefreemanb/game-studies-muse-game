//step
xx = choose(-1, 0, 1);
yy = choose(-1, 0, 1)
msg = global.txtMsg;
stringHeight = string_height("I");
if (nCurrent > 0) {
	stringLength = string_length(msg[nCurrent-1]); //string length of current message
}

if (timer < stringLength) && (done == false) {
    timer += stringLength;
	done = true;
}

while(timer > 1) {
	timer--; 
	isTag = false;
	posBegin = pos; 
	pos++; 
	if (timer < 0) timer = 0; 
	char = string_char_at(msg[n], pos);
	//skip spaces and the "#" sign
	if (char == " ") || (char == "#") {
	    pos++; 
	    char = string_char_at(msg[n], pos);
	}
	//TAGS 
	if (char == "[") { 
	    i = pos;
	    char2 = ""; 
	    while(char2 != "]") { 
	        i++; 
	        char2 = string_char_at(msg[n], i);
			isTag = true;
	    }
	    diff = (i-pos)+1; 
	    tag = string_copy(msg[n], pos, diff); 
	    //Pausing
	    if (string_count("[p:", tag)) {
	        p = real(string_digits(tag));
	        alarm[0] = (p*10);
	        paused = true; //pause
	    }
		//Questions
        if (string_count("[q:", tag)) {
            q = string_copy(tag, 4, (diff-4));
            answer = string_split(q, ",");
            global.txtAnswer = answer;
            create_one(0, 0, "Instances", obj_txt_ask);
        }
		if (string_count("[pic:", tag)) {
			pic = string_copy(tag, 6, (diff-6));
			photoSprite[n] = global.tagMap[?pic];
		}
	    pos += diff; //add the difference of the  tags
	} 
	if (pos <= stringLength+1) && (isTag == false) {
		txt += string_copy(msg[n], posBegin, pos-posBegin); //stores the final message without tags
	}
	txtFinal[n] = txt; //stores the final message as a usable array in terms of n
}

surface_set_target(surface);
	//background black
	draw_set_font(global.fontDefault);
	draw_set_halign(fa_left);
	col = c_black;
	draw_rectangle_colour(0, 0, surfaceWidth, surfaceHeight, col, col, col, col, false);
	//avatar
	//draw_sprite_ext(spr_avatar, 0, 32, 32, 1, 1, 0, -1, 1); 

	for (n = 0; n < nCurrent; n++) {
		//Current message
		//photoSpriteWidth[n] = sprite_width(photoSprite); //get the width of the current image sprite, if there is one 
		//photoSpriteHeight[n] = sprite_height(photoSprite); //get the height of the current image sprite, if there is one
		msgWidth[n] = string_get_width(string(txtFinal[n])); //width of the message, in pixels, without the tags
		msgHeight[n] = string_get_height(string(msg[n])); //height of the message, in number of lines
		msgTotalHeight[n] = array_sum(msgHeight, n) + msgHeight[n]; //Total height of all the messages expressed as a sum of all lines, minus 1
		msgBoxHeight = (msgHeight[n]*stringHeight);
		msgPad = 10; //padding between message boxes
		padding = 6; //padding between edge of message box and text inside it
		if (n == 0) {
			msgY[n] = msgYStart-msgYOffset;
		} else {
			msgY[n] = msgYStart+((msgTotalHeight[n-1])*stringHeight)+((n)*msgPad)-msgYOffset;
		}
		msgBoxBottomY = (msgY[n]-padding)+(msgHeight[n]*stringHeight)+padding; //y coordinate of the bottom of the last message box
		if (msgBoxBottomY > 160) { 
			msgYOffset += (msgBoxBottomY-160)/8;
		} 
		//Only draw the message if it's sufficiently high enough
		if (msgBoxBottomY > 165) {
			msgDraw[n] = false;
		} else {
			msgDraw[n] = true;
		}	
		//Depending on sender and recipient, draw the message
		if (msgDraw[n] == true) {
			if (msgReceiving[n] == true) { //when you receive messages, draw the received messages like this
				draw_rectangle_solid(msgX-(padding+2), msgY[n]-padding, msgWidth[n]+(padding+2), msgBoxHeight+padding, c_white, 6, true);
				draw_set_colour(c_white); 
				draw_set_halign(fa_left);
				draw_text(msgX, msgY[n], txtFinal[n]); 
				if (n == (nCurrent-1)) {
					draw_sprite_ext(spr_msgbox_corner, 0, msgX-12, msgBoxBottomY-padding, 1, 1, 0, -1, 1);
					if (paused == true) {
						//draw_sprite_ext(spr_typing, -1, msgX-8, msgBoxBottomY+8, 1 ,1 ,0, -1, 1);
					}
				}
			} 
			if (msgReceiving[n] == false) { //when you send message, draw the sent messages like this
				draw_rectangle_solid(msgX+(padding-4)+240, msgY[n]-padding, -msgWidth[n]-(padding-2), msgBoxHeight+padding, c_gray, 6, true);
				draw_set_halign(fa_right);
				draw_set_colour(c_gray); 
				draw_text(msgX+240, msgY[n], txtFinal[n]); 
			}

		}
		//draw_line_colour(0, 160, 400, 161, c_red, c_red);
	}
	draw_rectangle_outline(0, 0, surfaceWidth-2, surfaceHeight, 2, c_white);
surface_reset_target();

//Handles sending messages
if (receivingTimer > 0) {
	receivingTimer--;
}
if (receivingTimer == 1) {
	msgReceiving[nCurrent] = true; //set the first message to false as well
	isReceiving = true; //reset status to receiving texts again after you reply
	receivingTimer = 0;
	scr_txt_continue();
}

if (!surface_exists(surface)) {
	surface = surface_create(surfaceWidth, surfaceHeight);
}

//debugging
if (global.buttonPressedA) {
	if (instance_exists(obj_txt_ask)) {
		scr_txt_continue();
	}
}
