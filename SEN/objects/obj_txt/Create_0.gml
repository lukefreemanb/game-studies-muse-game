///Initialise the phone
surfaceWidth = 300;
surfaceHeight = 240;
surface = surface_create(300, 240);
//Phone recipient
isTyping = true;  
nCurrent = 0; //the most recent message
alarm[0] = 30;
msgName = "Anonymous";
msgWidth[0] = 0; //width of the message
msgHeight[0] = 0; //height of the message
msgTotalHeight[0] = 0;
msgBoxHeight = 0;
msgX = 32; //x position of the sender
msgYStart = 24; //y starting position of the sender
msgY[0] = 32; //y position of the sender
msgYOffset = 0;
msgColour = c_white;
msgDraw[0] = false; 
photo[0] = 0; //photo or na
photoSprite[0] = ""; //photo sprite resource
photoSpriteWidth[0] = 0; //photo sprite width
photoSpriteHeight[0] = 0; //photo sprite height
isReceiving = true; //true for when you are taking message, false for when you send
receivingTimer = 0; //counts down
msgReceiving[0] = true; //whether or not you are receiving or sending
stringHeight = 0;
stringLength = 0; //length of string 
msg = global.txtMsg;
isTag = false; //boolean for whether the current part of the loop is part of the tag that needs to be omitted
//Looping through the message
pos = 1; //position of pointer currently
posBegin = 1; //position of pointer at beginning of the loop
txt = ""; //the buffered text
txtFinal[0] = ""; //the final text - without the tags
timer = 0; //timer 
done = false; //whether the loop has finished or not, used for checking tags
paused = false;

meX = 0; //x position of sender, "me"
phoneY = 0; //y position of the entire phone

xx = 0;
yy = 0;

depth = -1;