if (surface_exists(surface)) {
	draw_surface(surface, 8, 8);
}
if (nCurrent < 1) {
	offset = 0;
} else {
	offset = 1;
}

draw_set_font(global.fontDefault);

/*
debugArray = [
	"len: " + string(array_length_1d(global.txtMsg)) + ",",
	"nCurrent: " + string(nCurrent),
	"global.txtMsg: " + string(global.txtMsg[nCurrent-offset])
]

scr_debug(8, 8, debugArray);

