{
    "id": "71dcb655-ad02-40b2-986c-493d0a17ff20",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_txt",
    "eventList": [
        {
            "id": "52f53bdd-b6b3-45bd-90a9-61e68e28c4ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "71dcb655-ad02-40b2-986c-493d0a17ff20"
        },
        {
            "id": "86720e7d-3203-4fb8-9043-a0483f2c725b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "71dcb655-ad02-40b2-986c-493d0a17ff20"
        },
        {
            "id": "5441a751-8361-4a34-baa0-ab0f767a51c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "71dcb655-ad02-40b2-986c-493d0a17ff20"
        },
        {
            "id": "7e4ea3ef-9940-4321-90c3-b622325335d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "71dcb655-ad02-40b2-986c-493d0a17ff20"
        },
        {
            "id": "b8d2997d-4f0c-4fe6-a6ba-751e4b4d08f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "71dcb655-ad02-40b2-986c-493d0a17ff20"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}