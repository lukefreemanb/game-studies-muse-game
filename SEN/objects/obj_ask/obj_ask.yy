{
    "id": "57b001a3-1731-4400-85fc-06417babd96a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ask",
    "eventList": [
        {
            "id": "53e98a98-3afe-446a-908f-d15cb701eb58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "57b001a3-1731-4400-85fc-06417babd96a"
        },
        {
            "id": "253ed351-87c3-4764-a0c6-a66f425f8767",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "57b001a3-1731-4400-85fc-06417babd96a"
        },
        {
            "id": "f95dee8b-5eb4-4568-8222-6690aa728585",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "57b001a3-1731-4400-85fc-06417babd96a"
        },
        {
            "id": "cdb45551-85c1-4c9c-b435-f2b6ab2fe667",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "57b001a3-1731-4400-85fc-06417babd96a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}