//scrolling through the options
if (active) {
	if (global.buttonPressedUp) && (select > 0) {
		audio_play_sound(snd_text_type, 1, false);
	    select--;
	}

	if (global.buttonPressedDown)&&(select < nTotal-1) {
		audio_play_sound(snd_text_type, 1, false);
	    select++;
	}

	msgWidth = string_width(string(answer[select]));
	//selecting an answer
	if (global.buttonPressedA) {
	    switch(select) {
	        case 0: q += "a"; break;
	        case 1: q += "b"; break;
	        case 2: q += "c"; break;
	        case 3: q += "d"; break;
	        case 4: q += "e"; break;
	    }
	    instance_destroy(); //delete self
	    scr_delete(obj_textbox); //delete current instance of the textbox
	    scr_script(q); //finds the relevant section in the script depending on the answer
	}
}