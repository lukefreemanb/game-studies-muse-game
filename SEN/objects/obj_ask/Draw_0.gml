//draw the options 
if (active) {
	draw_set_colour(c_white);
	draw_set_halign(fa_right);
	draw_set_font(global.fontDefault);

	padding = 4;
	draw_rectangle_solid(global.vx+292+padding, global.vy+global.tbY+19+(select*20), -msgWidth-(padding*3), 18, c_white, 0, false);

	for (i = 0; i < nTotal; i++) {
		if (i == select) {
			col = c_black;
		} else {
			col = c_white;
		}
		draw_set_colour(col);
	    draw_text(global.vx+298, global.vy+global.tbY+28+(i*20), string(answer[i]));
	}
}	