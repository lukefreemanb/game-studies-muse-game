{
    "id": "df78b6dc-1937-4cb1-928d-6810e7e0e9e0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_encounter_controller",
    "eventList": [
        {
            "id": "cb7ffd78-4652-44a1-86f5-1f5f657dc356",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "df78b6dc-1937-4cb1-928d-6810e7e0e9e0"
        },
        {
            "id": "24664b7b-ce89-4fb5-a2b3-9ae2b535b118",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "df78b6dc-1937-4cb1-928d-6810e7e0e9e0"
        },
        {
            "id": "f5c51609-84cc-4679-bf92-4b3a631b210b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "df78b6dc-1937-4cb1-928d-6810e7e0e9e0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}