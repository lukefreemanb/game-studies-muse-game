///Step
//surfScreen
if (!surface_exists(global.surfEncounter)) {
	global.surfEncounter = surface_create(room_width, room_height); //create the screen surface
}
//clear the surfaces every frame
surface_set_target(global.surfEncounter);
draw_clear_alpha(c_white, 0);
surface_reset_target();
