{
    "id": "641fdc30-eea3-44df-8dce-ffbfc2f4751a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sad_dog",
    "eventList": [
        {
            "id": "9b8fa241-b054-4bd8-a8d8-be71373a4f9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "641fdc30-eea3-44df-8dce-ffbfc2f4751a"
        },
        {
            "id": "37653830-2ef8-493e-a2c1-50aae9756533",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "641fdc30-eea3-44df-8dce-ffbfc2f4751a"
        },
        {
            "id": "6de7f8c4-9f60-494a-947a-c9a30a8259b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "641fdc30-eea3-44df-8dce-ffbfc2f4751a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "12f49012-6f3c-4640-b58e-43fb421b9a13",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4064730e-4483-45aa-8e93-ca91285950d1",
    "visible": true
}