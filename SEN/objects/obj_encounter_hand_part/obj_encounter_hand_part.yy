{
    "id": "c1e0ff57-095c-4fdc-bc7d-806a8878db38",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_encounter_hand_part",
    "eventList": [
        {
            "id": "91f02fbd-b171-476e-a507-d33a9849018b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "c1e0ff57-095c-4fdc-bc7d-806a8878db38"
        },
        {
            "id": "2744dafc-e94b-439c-b216-f0f42a2322c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c1e0ff57-095c-4fdc-bc7d-806a8878db38"
        },
        {
            "id": "82150b7f-9821-4a9e-a50c-b94d5385efa3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c1e0ff57-095c-4fdc-bc7d-806a8878db38"
        },
        {
            "id": "d1ceb56e-09b1-4461-af2a-b92336655113",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c1e0ff57-095c-4fdc-bc7d-806a8878db38"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7bd18b99-d063-4db1-9141-8fe632e79ec8",
    "visible": true
}