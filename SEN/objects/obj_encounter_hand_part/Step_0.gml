if (scaleX > 0) {
	scaleX -= .01;	
}

scaleY = scaleX;
if (scaleX < .01) {
	instance_destroy();	
}

ang += 4;
surface_set_target(global.surfEncounter);
draw_sprite_ext(sprite, 0, x, y, scaleX, scaleY, ang, -1, 1);
surface_reset_target();