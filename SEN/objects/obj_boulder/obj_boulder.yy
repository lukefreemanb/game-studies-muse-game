{
    "id": "d698d8af-73c4-4cee-a9c8-06807f5c5bd2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boulder",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fbe19f1d-450c-4d5e-b9fd-135f87f3c06c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6bb75211-5d95-45b3-9199-3b8732fb3cd7",
    "visible": true
}