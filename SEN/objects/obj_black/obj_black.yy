{
    "id": "cacbffde-5eb6-4dcd-8dd6-7fe447125eb0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_black",
    "eventList": [
        {
            "id": "4a4f88d7-d994-46b0-b6d1-8efc39dff046",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cacbffde-5eb6-4dcd-8dd6-7fe447125eb0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "de768532-a59f-44b2-a429-89aa22f954e5",
    "visible": true
}