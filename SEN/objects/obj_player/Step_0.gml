///Step
depth = -y;
hspd = sign(global.xaxis)*bspd; //get the horizontal speed
vspd = sign(global.yaxis)*bspd; //get the vertical speed

//STATE MANAGER
switch(state) {
	case "idle":
		moving = false;
		img = 0;
		img2 = 0;
	break;
	case "moving":
		moving = true;
	    img += .125;
		img2 = 1;
	break;
}

if (global.paused == true) {
	state = "idle";	
}

if (global.paused == false) {
	//whether moving or not
	if (global.xaxis != 0 || global.yaxis != 0) {
	    state = "moving";
	} else {
	    state = "idle";
	}
	//SPRIT manager
	if (hspd < 0) {
		spr = spr_player_left;
		ang = 180;
	} else if (hspd > 0) {
		spr = spr_player_right;
		ang = 0;
	}
	
	if (vspd < 0) {
		spr = spr_player_up;
		ang = 270;
	} else if (vspd > 0) {
		spr = spr_player_down;
		ang = 90;
	}
    
	//Interacting with things
	if (global.buttonPressedA) && (canPress) {
		canPress = false;
		o = obj_par_interact; 
		dist = 2;
		var in;
		switch(ang) {
			case 0: in = instance_place(x+dist, y, o);break;
			case 90: in = instance_place(x, y+dist, o);break;
			case 180: in = instance_place(x-dist, y, o);break;
			case 270: in = instance_place(x, y-dist, o);break;
		}
		//if it's found something, interact with it!!!!!
		if (in) {    
			img = 0;
			img2 = 0;
			//in.ang = ang; //change the ang variable to match yours
			if (in.m=="") {
				with(in) scr_interact();
			} else {//if the interactable has a built in message, use it
				scr_script(string(in.m)); //if not, get...
			}
		}
		item = obj_par_item;
		var itemCheck;
		switch(ang) {
			case 0: itemCheck = instance_place(x+dist, y, item);break;
			case 90: itemCheck = instance_place(x, y+dist, item);break;
			case 180: itemCheck = instance_place(x-dist, y, item);break;
			case 270: itemCheck = instance_place(x, y-dist, item);break;
		}
		if (itemCheck) {
			with(itemCheck) {
				scr_item_pickup(itemid);
			}
		}
	}

	//Collision
	if (!instance_exists(obj_textbox)) canPress = true;
	sol = obj_par_solid;
	//horizontal collisions
	if (place_meeting(x+hspd, y, sol)) {
	    while (!place_meeting(x+sign(hspd), y, sol)) x += sign(hspd);
	    hspd = 0;
	} 
	x += hspd;
    
	//vertical collisions
	if (place_meeting(x, y+vspd, sol)) {
	    while (!place_meeting(x, y+sign(vspd), sol)) y += sign(vspd);
	    vspd = 0;
	}
	y += vspd;
}