{
    "id": "51af49e7-eb94-404f-bbb3-e042a6f4255f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "1327bf9a-e8fc-44ae-aced-435e8c00ec03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "51af49e7-eb94-404f-bbb3-e042a6f4255f"
        },
        {
            "id": "67ef3d6c-770b-466f-9415-665459cc8685",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "51af49e7-eb94-404f-bbb3-e042a6f4255f"
        },
        {
            "id": "8eb86aa2-bafc-4f63-b9f8-391e3839ad38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "51af49e7-eb94-404f-bbb3-e042a6f4255f"
        }
    ],
    "maskSpriteId": "08b56bae-b00d-4a2b-bf76-77c35f4e6f50",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dfba731b-1e03-403c-88f8-376201d87c8d",
    "visible": true
}