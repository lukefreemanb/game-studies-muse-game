//Movement
depth = -20;
var xAdd = (sign(global.xaxis)*accel);
var yAdd = (sign(global.yaxis)*accel);

fric = 0.2; //friction
var xSub = min(fric, abs(hspd))*sign(hspd);
var ySub = min(fric, abs(vspd))*sign(vspd);

hspd = clamp(hspd+xAdd-xSub, -maxSpeed, maxSpeed);
vspd = clamp(vspd+yAdd-ySub, -maxSpeed, maxSpeed);

//angle
f = 8; //factor
sine += f*2;
if (sine > 360) {
	sine = 0;	
}
angIdle =  6*sin(degtorad(360-sine));
//turn the hand as you rub
if (hspd > 0) {
	if (ang > 0) {
		ang -= (ang/f);	
	}
}

if (hspd < 0) {
	if (ang < (angDefault*2)) {
		ang += ((angDefault*2)-ang)/f;
	}	
}

//reset the hand
if (hspd == 0) {
	if (ang < (angDefault)) {
		ang += ((angDefault)-ang)/f;
	}
	if (ang > (angDefault)) {
		ang -= (ang-(angDefault))/f;
	}
}

x += round(hspd);
y += round(vspd);

//particle testing
for (i = 0; i < particleTotal; i++) {
	if (hspd != 0 || vspd != 0) {
		if (createParticle == true) {
			particleX[i] = x;
			particleY[i] = y;
		}
	}
	particleXScale[i] -= .02;
	particleYScale[i] -= .02;
}

/*
createParticleTimer--;
if (createParticleTimer < 1) {
	createParticleTimer = 2;
	createParticle = false;
} else {
	createParticle = true;	
}
*/

//create particles
//instance_create_layer(x+irandom_range(-4,4), y+irandom_range(-4,4), "Instances", obj_encounter_hand_part);

//surface
if (!surface_exists(surf)) {
	surf = surface_create(room_width, room_height);	
}

surface_set_target(surf);
draw_clear_alpha(c_white, 0);
draw_sprite_ext(sprite, img, x, y, 1, 1, ang-(angDefault)+angIdle, -1, 1);
/*
for (i = 0; i < particleTotal; i++) {
	draw_sprite_ext(spr_hand_particle, 0, particleX[i], particleY[i], particleXScale[i], particleYScale[i], 0, -1, 1);
}
*/
surface_reset_target();