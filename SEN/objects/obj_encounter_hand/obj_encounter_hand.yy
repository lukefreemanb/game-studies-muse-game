{
    "id": "dadfe020-cf14-4796-8706-dcd8eabb5ac0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_encounter_hand",
    "eventList": [
        {
            "id": "dc1e05ba-a6c4-47b8-976d-6dbf3469fa6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dadfe020-cf14-4796-8706-dcd8eabb5ac0"
        },
        {
            "id": "5d46392d-6581-4839-b328-3c26f20f79e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dadfe020-cf14-4796-8706-dcd8eabb5ac0"
        },
        {
            "id": "863facc0-02e1-4aa3-81ab-646563bc1d06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dadfe020-cf14-4796-8706-dcd8eabb5ac0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "10cd0000-0c5a-4ee1-8054-65d8e151b4b9",
    "visible": true
}