//Initialise
//draw
sprite = spr_hand;
img = 0;
ang = 30;
angDefault = ang/2;
sine = 0;
angIdle = 0;

//movement
accel = 1;
maxSpeed = 5;
hspd = 0;
vspd = 0;

//testing particle effect
particleTotal = 50;
particleX = array_create(particleTotal, x);
particleY = array_create(particleTotal, y);
particleXScale = array_create(particleTotal, x);
particleYScale = array_create(particleTotal, y);
createParticle = true;
createParticleTimer = 2;

surf = surface_create(room_width, room_height);

