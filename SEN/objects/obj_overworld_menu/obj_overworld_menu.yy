{
    "id": "747eac4c-f3f4-41b2-9303-72518e7ac5c5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_overworld_menu",
    "eventList": [
        {
            "id": "01328ecd-f95d-4960-bc04-181357fe1393",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "747eac4c-f3f4-41b2-9303-72518e7ac5c5"
        },
        {
            "id": "dcd9a47e-26c1-4ca9-9b1f-0d5612afad12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "747eac4c-f3f4-41b2-9303-72518e7ac5c5"
        },
        {
            "id": "2667346a-7bc0-4ab0-8494-353169b9c316",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "747eac4c-f3f4-41b2-9303-72518e7ac5c5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}