//MENU
//open andclose
if (global.buttonPressedStart) || (global.buttonPressedX) {
	if (!global.paused) {
		global.paused = !global.paused;
		menuOpen = !menuOpen;
		menuLayer = 1;
	} else {
		if (menuOpen) {
			menuOpen = false;
			menuLayer = 0;
			global.paused = false;
		}
	}
}

//Navigating the menu when it's open
if (menuOpen) {
	switch(menuLayer) { 
		case 1://first layer
		if (global.buttonPressedLeft) || ((global.xaxisReset == true) && (global.xaxis < -global.gpDeadzone)) {
			if (menuOption > 0) {
				menuOption--;
				audio_play_sound(selectSound, 1, false);
			}
		}
		if (global.buttonPressedRight) || ((global.xaxisReset == true) && (global.xaxis > global.gpDeadzone)) {
			if (menuOption < 1) {
				menuOption++;
				audio_play_sound(selectSound, 1, false);
			}
		}
		break;
		case 2://second layer - navigating the item menu
			if (menuOption == 1) {
				if (global.buttonPressedUp) || ((global.yaxisReset == true) && (global.yaxis < -global.gpDeadzone)) {
					if (itemSelect > 0) {
						itemSelect--;
						audio_play_sound(selectSound, 1, false);
					}
				}
				if (global.buttonPressedDown) || ((global.yaxisReset == true) && (global.yaxis > global.gpDeadzone)) {
					if (itemSelect < global.itemTotal) {
						itemSelect++;
						audio_play_sound(selectSound, 1, false);
					}
				}
				if (global.buttonPressedRight) || ((global.xaxisReset == true) && (global.xaxis > global.gpDeadzone)) {
					if (itemSelect+5)<=(global.itemTotal) {
						itemSelect += 5;
						audio_play_sound(selectSound, 1, false);
					}
				}
				if (global.buttonPressedLeft) || ((global.xaxisReset == true) && (global.xaxis < -global.gpDeadzone)) {
					if (itemSelect-5)>=(0) {
						itemSelect -= 5;
						audio_play_sound(selectSound, 1, false);
					}
				}
			}
		break;
	}

	//SELECTING
	if (global.buttonPressedA) {
		if (menuLayer == 1) {
			menuLayer = 2;	
		}
		switch(menuOption) {
			case 0: //STAT
			
			break;
			case 1: //ITEM
				
			break;
		}
	}
	//GOING BACK
	if (global.buttonPressedB) {
		switch(menuLayer) {
			case 2:
				menuLayer = 1;
			break;
			case 1:
				menuOpen = false;
				global.paused = false;
			break;
		}
	}
}

//global.itemTotal = array_length_1d(global.item);
//Item management - gets the total amount of items you currently have
for (i = 0; i <= global.itemMax; i++) {
	global.itemTotal = i-1;
	if (global.item[i] == noone || global.item[i] = "") {
		break;
	}
}

//limit
if (itemSelect > global.itemTotal) itemSelect = global.itemTotal;
if (itemSelect < 0) itemSelect = 0;
msgWidth = string_width(string(global.item[itemSelect]));

depth = -(room_height*2);