//draw then menu if it's open
if (menuOpen == true) { 
	draw_sprite(spr_overworld_menu_bar, menuOption, global.vx, global.vy);	
	switch(menuLayer) {
		case 2:
			if (menuOption == 1) {
				///draw_rectangle_solid(x, y, width, height, colour, curve, outline)
				//draw the inventory boxes
				draw_rectangle_solid(global.vx+20, global.vy+42, 280, 118, c_black, false, false); //invent solid
				draw_rectangle_outline(global.vx+20, global.vy+42, 280, 118, 2, c_white); //invent outline
				draw_rectangle_solid(global.vx-8, global.vy+global.tbYDefault, global.tbWDefault, global.tbHDefault, c_black, false, false); //item description box
				//draw the inventory
				if (global.itemTotal > -1) { //if there are actual items
					itemDesc = scr_item(global.item[itemSelect], "description");
					draw_set_color(c_white);
					draw_set_font(global.fontDefault);
					draw_text(global.vx+global.tbXDefault+12, global.vy+global.tbYDefault+16, string(itemDesc));
					//draw_text_transformed(global.vx+xx+xoff+12, global.vy+yy+yoff+10, char, scaleX, scaleY, angle); //draw the text
				
					//draw item highlight
					padding = 4;
					if (itemSelect <= 4) {
						itemHighlightXOff = 0;
						draw_rectangle_solid(global.vx+42-(padding*3)+itemHighlightXOff, global.vy+52+(itemSelect*20), msgWidth+(padding*2)+2, 18, c_white, 0, false);
					} else {
						itemHighlightXOff = 144;
						draw_rectangle_solid(global.vx+42-(padding*3)+itemHighlightXOff, global.vy+52+((itemSelect-5)*20), msgWidth+(padding*2)+2, 18, c_white, 0, false);
					}
				
					for (i = 0; i <= 4; i++) {
						if (i == itemSelect) {
							col = c_black;
						} else {
							col = c_white;
						}
						draw_set_colour(col);
						draw_set_halign(fa_left);
						draw_set_font(global.fontDefault);
						draw_text(global.vx+42, global.vy+60+(i*20), string(global.item[i]));
					}
					for (i = 5; i <= global.itemTotal; i++) {
						if (i == itemSelect) {
							col = c_black;
						} else {
							col = c_white;
						}
						draw_set_colour(col);
						draw_set_halign(fa_left);
						draw_set_font(global.fontDefault);
						draw_text(global.vx+42+144, global.vy+60+((i-5)*20), string(global.item[i]));
					}
				}
			}
			
			draw_set_color(c_white);
			draw_text(global.vx+144, global.vy+140, string(itemSelect) + "," + string(global.itemTotal));
			draw_text(global.vx+144, global.vy+160, string(global.item[0]) + "," + string(global.item[1]) + 
			",\n" + string(global.item[2]) + "," + string(global.item[3]) + ",\n" + string(global.item[4]) + 
			"," + string(global.item[5]) + ",\n" + string(global.item[6]) + "," + string(global.item[7]) + "," +
			string(global.item[8]) + ",\n" + string(global.item[9]) + "," + string(global.item[10]));
			
		break;
	}
}
