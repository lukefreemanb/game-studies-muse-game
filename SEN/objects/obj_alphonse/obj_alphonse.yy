{
    "id": "c18338ad-9d43-4977-b573-e12a2267d50b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_alphonse",
    "eventList": [
        {
            "id": "0801b84b-567b-4f99-8a1c-52f9205b83dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c18338ad-9d43-4977-b573-e12a2267d50b"
        },
        {
            "id": "9a08158a-96b2-4a7c-92ab-19c546b774fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c18338ad-9d43-4977-b573-e12a2267d50b"
        },
        {
            "id": "24106ef2-9d26-4759-9eec-73c8d7420089",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c18338ad-9d43-4977-b573-e12a2267d50b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "12f49012-6f3c-4640-b58e-43fb421b9a13",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "df2c24fa-9553-4bf2-9e58-44642ba26a67",
    "visible": true
}