
{
    "name": "rm_hubroom",
    "id": "6ed8c9a2-9dcf-412c-a0fc-70fe0fd91109",
    "creationCodeFile": "",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "d4ee3203-fde7-44ff-b032-0def3b377bcd",
        "7eecc4f6-30a0-4a6d-8715-2c10d1ee147e",
        "c086a0dc-5055-49e2-8305-3628865a0974",
        "6af1ea8b-d993-4c02-bb7a-daa138352e8f",
        "00f44226-5d24-468e-952f-f411ab889992",
        "bb00fb17-7160-4ffe-b78d-cb1191416b54",
        "30503569-71f5-4ece-b9b6-6286f4eb10ac",
        "589b9ae2-376c-430e-acd5-21ff2920096e",
        "6827336a-0061-4277-a668-5304ce414e08",
        "16a7e35c-b87a-48c7-98e5-86221c9f580a",
        "19927f83-433d-4a94-9ade-72d025b4c013",
        "206bd546-fe96-40db-8d80-b6f53900cc0c",
        "6147b13a-044e-4ced-862e-ad4cb4579e46",
        "a2e9337c-7bab-48a8-8e32-2f78812f6a33",
        "5ca5a11b-d2ae-41f7-9902-308d23cbc5b1",
        "41988fc6-1812-4595-94f1-3dc326136908"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "254dd4cd-53fd-4648-bed4-8606c233e2ff",
            "depth": 0,
            "grid_x": 8,
            "grid_y": 8,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_A555562","id": "d4ee3203-fde7-44ff-b032-0def3b377bcd","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_A555562","objId": "1008eda3-472f-4183-8d8b-41630d8bdda4","properties": null,"rotation": 0,"scaleX": 4,"scaleY": 9,"mvc": "1.0","x": 320,"y": 0},
{"name": "inst_7EC84A21","id": "7eecc4f6-30a0-4a6d-8715-2c10d1ee147e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7EC84A21","objId": "1008eda3-472f-4183-8d8b-41630d8bdda4","properties": null,"rotation": 0,"scaleX": 2,"scaleY": 20,"mvc": "1.0","x": 32,"y": 0},
{"name": "inst_617532CE","id": "c086a0dc-5055-49e2-8305-3628865a0974","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_617532CE","objId": "1008eda3-472f-4183-8d8b-41630d8bdda4","properties": null,"rotation": 0,"scaleX": 17,"scaleY": 3.5,"mvc": "1.0","x": 56,"y": 0},
{"name": "inst_114D6DE4","id": "6af1ea8b-d993-4c02-bb7a-daa138352e8f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_114D6DE4","objId": "1008eda3-472f-4183-8d8b-41630d8bdda4","properties": null,"rotation": 0,"scaleX": -7,"scaleY": 2,"mvc": "1.0","x": 176,"y": 288},
{"name": "inst_337D34DD","id": "00f44226-5d24-468e-952f-f411ab889992","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_337D34DD.gml","creationCodeType": ".gml","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_337D34DD","objId": "d79a76f4-fc88-47d6-a1fb-0c20aaf0a2ce","properties": null,"rotation": 0,"scaleX": 2,"scaleY": 1,"mvc": "1.0","x": 176,"y": 304},
{"name": "inst_128509E3","id": "bb00fb17-7160-4ffe-b78d-cb1191416b54","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_128509E3.gml","creationCodeType": ".gml","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_128509E3","objId": "d79a76f4-fc88-47d6-a1fb-0c20aaf0a2ce","properties": null,"rotation": 0,"scaleX": -1,"scaleY": 2,"mvc": "1.0","x": 384,"y": 144},
{"name": "inst_76795B76","id": "30503569-71f5-4ece-b9b6-6286f4eb10ac","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_76795B76","objId": "1008eda3-472f-4183-8d8b-41630d8bdda4","properties": null,"rotation": 0,"scaleX": 4,"scaleY": 7,"mvc": "1.0","x": 320,"y": 176},
{"name": "inst_1B5BD168","id": "589b9ae2-376c-430e-acd5-21ff2920096e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1B5BD168","objId": "1008eda3-472f-4183-8d8b-41630d8bdda4","properties": null,"rotation": 0,"scaleX": 8,"scaleY": 2,"mvc": "1.0","x": 208,"y": 288},
{"name": "inst_2F43CFE9","id": "6827336a-0061-4277-a668-5304ce414e08","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2F43CFE9","objId": "8d033d6a-33f6-4cc1-901e-48b7ab321853","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 352,"y": 272},
{"name": "inst_61C0D39","id": "16a7e35c-b87a-48c7-98e5-86221c9f580a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_61C0D39","objId": "8d033d6a-33f6-4cc1-901e-48b7ab321853","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 16,"y": 176},
{"name": "inst_35C16F99","id": "19927f83-433d-4a94-9ade-72d025b4c013","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_35C16F99","objId": "269f50af-4f35-4daa-ab31-f5daad0319ea","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 224,"y": 304},
{"name": "inst_6B2CCCBE","id": "206bd546-fe96-40db-8d80-b6f53900cc0c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6B2CCCBE","objId": "17474dda-264a-460a-ae3b-099aa6036e04","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 352,"y": 64},
{"name": "inst_E27264F","id": "6147b13a-044e-4ced-862e-ad4cb4579e46","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_E27264F","objId": "17474dda-264a-460a-ae3b-099aa6036e04","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 16,"y": 32},
{"name": "inst_5E9E8600","id": "a2e9337c-7bab-48a8-8e32-2f78812f6a33","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5E9E8600","objId": "8d033d6a-33f6-4cc1-901e-48b7ab321853","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 96,"y": 0},
{"name": "inst_31400D4C","id": "5ca5a11b-d2ae-41f7-9902-308d23cbc5b1","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_31400D4C","objId": "0e98f34b-fd0d-4597-94cf-2361315a4f9a","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 256,"y": 256},
{"name": "inst_2A99BA52","id": "41988fc6-1812-4595-94f1-3dc326136908","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2A99BA52","objId": "83038bab-2daf-4a57-8719-e2e64718762b","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 194,"y": 44}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRTileLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Tiles_1",
            "id": "ceccf396-6f78-4dcb-82a5-7ec83174b3c7",
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRTileLayer",
            "prev_tileheight": 16,
            "prev_tilewidth": 16,
            "mvc": "1.0",
            "tiles": {
                "SerialiseData": null,
                "SerialiseHeight": 20,
                "SerialiseWidth": 24,
                "TileSerialiseData": [
                    34,34,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,34,
                    34,26,26,26,41,26,42,42,42,42,42,42,42,42,42,42,42,42,42,43,26,26,26,34,
                    34,26,26,13,49,49,50,51,50,50,51,50,49,50,50,49,51,50,50,51,15,34,26,34,
                    34,26,26,37,57,58,59,58,58,57,58,58,58,58,58,59,58,58,58,59,39,34,26,34,
                    34,26,26,21,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,23,34,26,34,
                    34,26,26,268435479,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,46,2147483648,268435477,34,26,34,
                    34,26,26,21,2147483648,46,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,46,2147483648,2147483648,2147483648,47,2147483648,40,41,42,42,
                    34,26,26,268435479,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,48,49,50,51,
                    34,26,26,21,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,55,0,56,57,58,59,
                    26,26,26,21,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,0,0,0,0,0,
                    26,26,26,21,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,46,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,0,0,0,0,0,
                    26,26,26,268435479,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,55,2147483648,2147483648,2147483648,2147483648,2147483648,12,16,17,19,18,
                    26,26,26,21,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,23,34,26,34,
                    26,26,26,268435479,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,268435477,34,26,34,
                    34,26,26,21,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,23,34,34,34,
                    34,26,26,21,2147483648,2147483648,46,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,23,34,34,34,
                    34,26,26,268435479,2147483648,2147483648,47,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,46,2147483648,268435477,34,26,34,
                    34,26,26,21,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,47,2147483648,2147483648,2147483648,55,2147483648,2147483648,2147483648,2147483648,23,34,26,34,
                    34,26,26,29,18,30,18,19,19,30,1073741840,0,0,1879048212,18,30,18,18,30,19,31,34,26,34,
                    34,26,26,26,26,26,26,26,26,35,29,805306373,536870917,1879048221,35,26,26,26,26,26,26,34,26,34
                ]
            },
            "tilesetId": "18528e35-c79a-405c-bf8f-6f0bae4fdf49",
            "userdefined_depth": false,
            "visible": true,
            "x": 0,
            "y": 0
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "ab38a9aa-4bd7-48c4-94ef-6db216b85309",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4286074162 },
            "depth": 200,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "00000000-0000-0000-0000-000000000000",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "a222c763-6c9a-464d-b459-cbb067fde8ec",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "07889540-c247-4457-85ab-0770bd4125d2",
        "Height": 320,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 384
    },
    "mvc": "1.0",
    "views": [
{"id": "a58b2100-2a72-4cb1-8de1-cfb115378897","hborder": 32,"hport": 480,"hspeed": -1,"hview": 240,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": true,"vspeed": -1,"wport": 640,"wview": 320,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "95d459f7-409f-4c26-83d6-f3744ada1bfd","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "314ab2f1-408a-465c-bad8-e9e8c01c20e2","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "822d5162-bcee-44c5-8f83-c4e130943dcc","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "5c78463b-bca1-4853-b840-bb38a63ca76d","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "b69e2d76-c398-495d-9be0-b3b29eba0950","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "2a27ff64-19a6-4794-9ad0-4b1806f292e3","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "5fb33e16-6eda-4e8f-b489-98ebb9dc9907","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "2ad4ad89-0d74-4ad7-a944-cd5e687327b8",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": true,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}