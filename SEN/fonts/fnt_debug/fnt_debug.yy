{
    "id": "ab8f8f18-b5d1-4a00-b50d-0724f1371770",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_debug",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Calibri",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "678c7ff8-f7c4-4b09-ad64-2956642e5718",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 23,
                "y": 53
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "74c599ec-b4bb-4d2a-9689-18ff4726bdbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 25,
                "y": 87
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "53ef29f8-2927-4627-852f-f65e62431c75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 4,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 38,
                "y": 87
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "e2cb29f8-f34a-4bf2-81aa-e2a95bf4a6e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 106,
                "y": 19
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "5dbd30f1-c856-4a48-9a34-10257549349b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 42,
                "y": 36
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "96bea4de-452d-4fc1-a72a-faf5bcc58c6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c148e108-7489-4541-8d6f-4b953c20b161",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 9,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "064f1a48-a711-4120-b5a0-e4b4a1ce6773",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 4,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 46,
                "y": 87
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "d49f1c2b-96b2-4cd6-9c97-14b0043df7bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 71,
                "y": 70
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "9bfdf6fd-3592-4feb-964f-75026fc7fb4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 81,
                "y": 70
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "62b48feb-2b7b-4982-8101-c1677c1f6f00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 6,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 101,
                "y": 70
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "2e06a077-d973-4867-a878-8813d68308f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 70
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "7400387a-9434-4348-b3d8-d19addeb614f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 11,
                "offset": -1,
                "shift": 3,
                "w": 3,
                "x": 91,
                "y": 70
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b54b54fe-e789-4bf2-8aaf-393ffb6b568f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 6,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 33,
                "y": 87
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "a1609eff-2053-43d2-aced-2ecdd10a3e7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 9,
                "y": 87
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "ecf8b0c2-577e-43cc-a1eb-c9b25260250d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 47,
                "y": 19
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "df660512-d4b6-4dda-976a-82a68b2d1dfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 63,
                "y": 53
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "40b796bf-a567-4e95-95b1-3455eec89ca4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 82,
                "y": 19
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "47e579ff-8abd-4af2-ae00-0b712170442e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 56,
                "y": 53
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "770ead67-7330-40ac-9dd7-2ee9d33b9d95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 28,
                "y": 53
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a892318e-38e1-4c22-af7b-c4e156774cfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 36
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "d77908b0-773d-47ed-9035-034bb4112f32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 70,
                "y": 53
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d0829b2f-8c49-4129-9952-3a62758e2a0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 77,
                "y": 53
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e410daed-9611-42df-8f59-3d881d456dcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 35,
                "y": 53
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3f1e6006-f76c-4ca7-a9d7-8e4230f40c82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 42,
                "y": 53
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "20f80199-f554-4be2-9baa-09ff85466f7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 49,
                "y": 53
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "97827eaf-b95a-4cca-9e11-4a3482fe243f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 17,
                "y": 87
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a173fac7-26b0-41cf-b800-198ff3a7cc61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 11,
                "offset": -1,
                "shift": 3,
                "w": 3,
                "x": 86,
                "y": 70
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d9ec42a7-3e7b-4f4e-84e3-b562fce4a144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "560d3a49-f208-4207-a5dc-02b403e18ae6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 59,
                "y": 70
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "9c0efe30-6232-416d-8183-9e4086cd3d2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 70
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "af88f1d7-2ef7-4d95-924d-4478c537ce43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 84,
                "y": 53
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "49b9363e-d731-41fe-9a4a-1823af004130",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 10,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "0b850498-4894-4f68-a675-21f9c10941f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 9,
                "offset": -1,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 19
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "1f8bcb05-49b9-41b3-9acb-c3c900ca63e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "580630ce-d73a-4aeb-b104-18a530f7fcaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 91,
                "y": 53
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e6e4a027-8509-4b36-964e-d204eb848d30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 9,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 19
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "f698b1fb-2023-4ae9-9b62-76d8aaaaa803",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 98,
                "y": 53
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "57bd4342-2934-43d5-b14a-550d236bbef1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 105,
                "y": 53
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1e9062b2-b5bf-41dd-ac6e-3d1f7289aff0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 9,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 19
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "de9603e0-5cdb-40e9-8a64-c7cf88d3b31e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 9,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "3bd04626-cc6e-48a9-9aa5-a0a3875221a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 29,
                "y": 87
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "3a82b349-68ee-43e1-a08e-0b43d54b06c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 113,
                "y": 70
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b7fc41ce-bf79-465f-82eb-22eec0a204ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 90,
                "y": 19
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "526b58a1-6e99-49b0-ae44-5a5699c3bc6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 47,
                "y": 70
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "487b1d74-8af3-49ef-8ec7-e73d3218a936",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 9,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "2a78c2b9-82a7-4989-9fdd-c14a682a457a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 9,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 38,
                "y": 19
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f9eeef85-b7ac-4a60-afdc-78ee8d805540",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 9,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d7bc9200-bb61-4a04-b896-700a38f6e930",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 56,
                "y": 36
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c9bda49b-df65-4689-acaf-d58ba7b028a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "4903125e-b3da-4de5-aec6-195d000c3e28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 98,
                "y": 19
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8ca6f85d-86c8-4dc7-bf41-fc946a72bb03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 91,
                "y": 36
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f126dde0-7e8d-4da7-9bb5-e5b87084d257",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 114,
                "y": 19
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1e83717e-4081-47df-8ae7-74f1256977a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 9,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 19
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "1c4ef028-35b0-4eaa-beb0-e314dea8ac9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 18,
                "y": 36
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "163b2a55-1835-4b31-b8ba-b5e1eef392d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 9,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "5dd1c4f1-c397-4d72-b69c-c68244d864c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 10,
                "y": 36
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "7a3fdb3d-72d3-4397-86a5-98ed24bf4d1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 36
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4748dc34-0a93-4246-840a-af63ff6aebb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 49,
                "y": 36
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "dfc1c6b7-8e50-46be-9963-68254b450b3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 96,
                "y": 70
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "9b4136c1-02f0-4186-969f-f36312f156df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 68,
                "y": 19
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "3008087f-9a73-4628-9465-20054eafc4f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 66,
                "y": 70
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f72e3c3c-3d97-4649-b1f6-c8d626d2fb27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 5,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 87
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c55c3902-1035-47cf-8588-5bf0104edafa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "2551133f-57af-4c77-a5fd-5e9adc8966c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 2,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 49,
                "y": 87
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b74b153c-6f38-438a-9519-f3da89b80e19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 105,
                "y": 36
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "947d4517-7c3a-4e53-8956-812ed604103d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 53
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "69031746-d866-40d8-b4a9-41f0354ba8d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 53,
                "y": 70
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "51e3e0a2-cba9-42ac-9162-d9c2cca54122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 84,
                "y": 36
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "eb54ec76-8b1c-435d-9757-1fa96ce6ac8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 70,
                "y": 36
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ac6309e4-156f-4dd3-b274-2d80a777828c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 9,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 29,
                "y": 70
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "21e6698f-b2a3-4864-8d5d-c831b3078766",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "5f7a252f-e358-4b68-84e6-c10e63855808",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 63,
                "y": 36
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "f42aa428-b91a-4af4-b47b-b379ad2159a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 21,
                "y": 87
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "7cb1ada3-b033-4884-a3e8-3407d09afad1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 11,
                "offset": -1,
                "shift": 3,
                "w": 3,
                "x": 76,
                "y": 70
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "2338b05e-0b22-4e90-ac16-db505be5ee48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "20840c9a-73fd-4bcc-b988-7ef80b992b0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 13,
                "y": 87
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "0d011bbd-c04c-4758-b2f1-110238c4db0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 9,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "03c55e02-f4ed-43cf-bacf-9be87c5f4705",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 53
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "a1fea78c-9050-4837-ac07-f6c4c5507e46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 119,
                "y": 36
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "bffb1c10-dc2d-4d83-9e45-1ca3ed246f06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 75,
                "y": 19
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8e2ad48a-bcf5-448e-9506-2a08773d9e1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 61,
                "y": 19
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "be8002db-9212-4dd8-bfea-a84d9bc767c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 108,
                "y": 70
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "52919eb1-94cd-404c-8dc2-303c9d90093c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 41,
                "y": 70
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a80813c6-93c5-4f95-8ff7-d493ffaf8404",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 9,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 23,
                "y": 70
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9beeee16-a1cf-4e5b-8c3f-40dc93483255",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 112,
                "y": 36
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a765bfa9-cbdb-4be4-8191-0c74f45ce50f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 98,
                "y": 36
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "81a97cb2-ebe0-4262-bcad-19a64f5095e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "8bf9b7e0-bd35-4ed9-9c67-98ecf3e06057",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 77,
                "y": 36
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "7fc4fc31-d156-42c7-851f-a28ce9f15d28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 54,
                "y": 19
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f592857e-4e1b-4aa7-8a50-c16be8b051f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 35,
                "y": 70
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "13bc0093-34e5-44ff-855d-95c5e68639bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 11,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 118,
                "y": 53
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "5366d28b-0699-4fcd-b2ac-c87ea9d5fc7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 11,
                "offset": 2,
                "shift": 6,
                "w": 1,
                "x": 43,
                "y": 87
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "a129e974-8baf-4f82-899e-2e7ae438b748",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 112,
                "y": 53
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c1c378a1-56ce-4bbf-b97c-2b0e8fd40822",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 5,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 118,
                "y": 70
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "1f35fef0-b203-483d-b5c5-f48440525def",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "3c51549b-b4fa-41d4-8548-89e22d181bcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 221
        },
        {
            "id": "59db8fd5-4d62-4ffd-92f7-b1e63181b0a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 374
        },
        {
            "id": "05460176-e62b-4f6d-a524-6b7a52e997e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 376
        },
        {
            "id": "8dc7b08c-d4f3-4a3b-bc9f-17f2b0848af7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7922
        },
        {
            "id": "7556314d-5146-465b-92c2-913e20686fbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7924
        },
        {
            "id": "22742f00-dfdf-449e-bc09-2cb1dbb6bc9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7926
        },
        {
            "id": "9ce05318-e343-47c2-ae10-6a828a9162a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7928
        },
        {
            "id": "f4031dbf-0211-4404-9d67-581566755bcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8216
        },
        {
            "id": "02026618-dada-4511-9e2a-f8b0e0db7c89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8220
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 9,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}