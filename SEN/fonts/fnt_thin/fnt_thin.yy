{
    "id": "6bc42aeb-e5e6-4017-916d-a11b6f3d68a9",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_thin",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Calibri",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c8d32247-8d82-4793-8bcd-1d710c146f11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 16,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 29,
                "y": 74
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b09141d1-5a25-4cc4-9d0c-1b074f1ec085",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 10,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 65,
                "y": 92
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "4613c481-51f5-4c51-aa7a-5fcd3e974e71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 4,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 58,
                "y": 92
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "18083301-264c-4f19-9b4c-bfe2b117d3a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 38
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "48362d0a-70bc-4e77-8692-17bd95910b71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 63,
                "y": 20
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "13a12a59-4c2c-4642-82ff-784a905b65c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 10,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "0d71a2ae-b308-4aa1-b1da-fcf79379f276",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 10,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "7399c6f7-9be6-488f-867f-bcc01edfaa01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 4,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 76,
                "y": 92
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f7db400c-9af5-4d6f-ae6c-7740f11c3d4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 12,
                "y": 92
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "573d61af-4339-4bc3-a304-69d296a9eb61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 112,
                "y": 74
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5bf518db-74ee-4a16-bdea-660feb1d7491",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 22,
                "y": 92
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "c2e7e83d-f66b-442c-a698-8734515f587c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 47,
                "y": 74
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "212b3ae4-c3f3-4727-8577-8e15f5cddffd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 12,
                "offset": -1,
                "shift": 3,
                "w": 3,
                "x": 7,
                "y": 92
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9372b697-99af-4550-80c3-66e5ce570bbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 37,
                "y": 92
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "23c42479-ebc5-457f-accb-4288e0a999fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 69,
                "y": 92
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "0dfb2959-b99b-4be3-8df8-161ed647d124",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 20
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "f8c950d1-65a2-405d-9db8-3e541d46f0f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 26,
                "y": 56
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4076c31a-3bf0-4feb-8c6e-ac2da7f84f72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 50,
                "y": 56
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "45a69c78-f304-4e06-8669-a62828b76958",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 71,
                "y": 20
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d4e37464-7e27-4882-a964-1b601e491944",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7dc3145c-6c6a-4ad5-90ca-39a919b3f011",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 105,
                "y": 38
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "2e782977-8f9b-47e8-9f81-ffc188645c2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "f99278a7-20ef-4753-a258-d3256cff8719",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 10,
                "y": 56
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9f9660e0-333d-424f-a58e-fc84e7eb59aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 18,
                "y": 56
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "47d6c8d4-e34e-4986-9676-8e47a3ffef3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 58,
                "y": 56
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "4f8eb884-9484-479e-995a-4ee3df8811f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 66,
                "y": 56
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b9397da4-5ea0-4d3e-9b7d-165357119f23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 42,
                "y": 92
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "fd44da01-c78c-44be-8163-6b6bcfbd6abf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 12,
                "offset": -1,
                "shift": 4,
                "w": 3,
                "x": 122,
                "y": 74
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "40006c9f-f077-4b9e-b4ef-cbf992233d77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 40,
                "y": 74
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d317b4f4-5819-4477-8433-8c0bd1141d5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 81,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "315d0402-d7d1-4c39-9b9b-5da718fde37c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 54,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "cc2ce415-721d-4f9c-a064-4ab3fff72a39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 116,
                "y": 56
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b11626c8-b32a-4379-ad08-a5e39e908519",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 11,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "bd1bfd67-e010-425b-b868-d7c667031164",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 36,
                "y": 20
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "157e7e37-2bde-45ce-9fe0-70af4e1978a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 10,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 81,
                "y": 38
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "1d8fb0ef-9a7d-44ba-92e0-8d36c5ec437e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 10,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 42,
                "y": 56
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "bca75444-3faa-4a1e-8eef-c90c98254b3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 65,
                "y": 38
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "5c50ed4c-9bec-439e-bdec-47d804cb7a70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 10,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 74,
                "y": 56
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "aaa08a3b-4c58-4c9a-9f95-ab466f1ff74c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 10,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "33be311d-c309-43b1-b016-16bcaf2aa986",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 119,
                "y": 20
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "5528fe7a-eacf-458b-88d2-bff3e9072230",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 73,
                "y": 38
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "83624a9f-7b7b-45ca-ac1f-2cc6ad26fa04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 54,
                "y": 92
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b0c6536e-e0ed-4b88-9b62-e1cbf47c566a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 94,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "4a2fc98c-29c5-4f60-b226-ff9cc49b9c7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 10,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 95,
                "y": 20
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b3b82b88-b439-46df-98a0-e2b19e0571c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 10,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 109,
                "y": 56
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6b66d88e-8997-4ee9-ad1a-e4924fb28d24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 10,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "59465ec9-2335-4974-a18b-ee60b02b0841",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 10,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 18,
                "y": 20
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "44abcf9f-cd1c-4924-942a-9da463a96d88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 10,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 54,
                "y": 20
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ecda8523-44b8-4649-a765-deede4d605e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 10,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 111,
                "y": 20
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "3c7a0b6e-7cb8-4c6c-88f7-f20a5740e206",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 11,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9b61e0c5-35d5-47d7-a2e9-51e29a8d065c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 10,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 57,
                "y": 38
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "469216d4-ed2f-4d64-aa19-68645df31328",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 74
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "42254465-9819-4dd3-ac84-ba7e6a437584",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 38
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a0257e1c-dc40-4d93-a733-8c88f3f9a8a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 38
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "d654cdfd-679d-4056-8ee4-949841403423",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 27,
                "y": 20
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "9ed2dbe1-81d4-4ade-a6c8-316cdaadff2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 10,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "6b8e696d-e051-407c-868e-d2a90053ec8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 45,
                "y": 20
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "7bc676e9-372c-47f0-a4e4-35e22efa58d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 41,
                "y": 38
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "e7750317-8c3b-4f24-8a5d-b589a3d6fef6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 49,
                "y": 38
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "11c4c18c-1590-4fcf-9b92-ce77ce6df726",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 12,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "785f0959-dfea-47e6-bcb9-2927fa6ed24c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e663ec61-e5fc-4ad1-86e1-99549095dac4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 17,
                "y": 92
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "5688abfe-bd5b-469d-b54a-f1301d6ed016",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 61,
                "y": 74
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "addeadc3-1971-491c-b1f4-283755655832",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 20
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f6da2620-9023-4855-b425-7d55f108f857",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 3,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 80,
                "y": 92
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "cc5dbdf4-d757-4325-a3ba-1440daab68c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 81,
                "y": 56
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "9e55ddd4-0f7e-4401-a120-a76973e2faa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 103,
                "y": 20
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "195570b0-19df-4c5d-83ee-be3d1ec6db50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 100,
                "y": 74
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "17d35679-5de2-4504-912f-003bffa2c5ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 87,
                "y": 20
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "74d99656-faa7-44ed-b1d7-ca57741ea5e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 79,
                "y": 20
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c2a5e544-676b-4410-8e78-e31235fe5ed4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 10,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 16,
                "y": 74
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6f1905c7-8bbf-4ceb-8ba2-a2e3fd3c6c63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "686f7fa8-d0a8-4850-91a9-c91821f9d637",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 34,
                "y": 56
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e4b300ea-8354-42d1-b8f5-644f13e1421c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 50,
                "y": 92
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "7a297aea-4675-4e44-8280-d8704d3bf1c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 12,
                "offset": -1,
                "shift": 3,
                "w": 3,
                "x": 117,
                "y": 74
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "da51e822-2f46-4a2b-875b-cbee4ed8731f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 88,
                "y": 56
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "572e4da4-72bf-47fa-afdd-98a80d5ac46b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 46,
                "y": 92
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "0559a172-72d0-4d15-a15f-0a697298c3f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 10,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "1755e4d0-e1f2-44c7-87a4-b027928748e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 113,
                "y": 38
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "55bd26c9-297d-485e-a7c9-ae0534f12ec9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 97,
                "y": 38
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "20f47c06-b124-46e9-a60f-4fe2d66d1d5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "9d673e16-00d4-4fc7-8cc1-14716a55d269",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "21c6459b-2f58-40ef-a032-60d9b9f8000a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 88,
                "y": 74
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "90b20376-a886-4693-8ed4-0d06e2599aed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 69,
                "y": 74
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "ba305d9f-c3fd-432a-9d28-833f029e0184",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 75,
                "y": 74
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "67cde99e-561f-439f-a3ac-299e0b24d522",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 89,
                "y": 38
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "048860bf-0b38-4b76-bc9f-35bdd153fa79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 102,
                "y": 56
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "f3fb18e7-cb9e-448d-843e-0846d22ee876",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 10,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e3270c3a-4e25-4741-9af8-5f4cd96bf870",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 95,
                "y": 56
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "b2a21d7f-182d-411f-bd0c-822db21941e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 34,
                "y": 38
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f51a1270-0e4f-45ca-b299-466e728aba5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 106,
                "y": 74
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b390be3d-477d-47e2-b2bb-4558806987fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 23,
                "y": 74
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "9a7608e8-d7d0-4779-af31-d5789a75207c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 12,
                "offset": 2,
                "shift": 6,
                "w": 1,
                "x": 73,
                "y": 92
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "46ce196c-71e3-467f-8711-d16e3e3e637d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 34,
                "y": 74
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d7704983-1a08-4289-a9c9-5853698fb654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 5,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 29,
                "y": 92
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "2837bcc6-4591-4ff3-a9f9-57f1b9de4d51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "6ba75631-c076-4465-8a4c-3adbb59cb5f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "32fe48b1-85c2-45f9-aeef-c6d8ff4b5e40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 221
        },
        {
            "id": "4c41afd0-4d8f-4430-9859-0bd46ed49e0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 356
        },
        {
            "id": "5a16dd28-3872-4d0e-be7e-b5494a3a3b76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 374
        },
        {
            "id": "2eb2ab4c-a9b3-4ef5-972d-df2caf37e47f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 376
        },
        {
            "id": "407e2a6a-64cb-485b-911f-911df7047ad1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 538
        },
        {
            "id": "7f9d12dc-12f9-4f9a-b457-fb205efffe0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7922
        },
        {
            "id": "3ace53bc-3251-4df1-912a-e1c8e57c89cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7924
        },
        {
            "id": "6676b3d2-6ccf-4f03-b904-098ac683e36b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7926
        },
        {
            "id": "21b2eb35-8dd6-44ab-8ab5-8a8354a9c578",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7928
        },
        {
            "id": "5799499d-bad3-4d4c-8f70-93c85c28e562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8216
        },
        {
            "id": "7bf64ec0-9ce8-480d-aa52-6dcfed309628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8220
        },
        {
            "id": "1f2ab424-b54f-4efb-b73c-e702fb91cd43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "7adc9de4-db24-4472-b116-add4220f5668",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "cd10e2f2-7dc8-41c6-b7cb-e720e1907666",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "d1065f24-7967-4c24-8eac-db18456275af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "f97e9da2-02ce-4b91-b35b-4ad37106afa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 221
        },
        {
            "id": "5048c9db-54c0-48c9-81b0-4192a4d1d9af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 374
        },
        {
            "id": "37c1f05e-bfa5-46a4-8e96-038acc8b8c60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 376
        },
        {
            "id": "34be791f-9869-4715-8722-206794e91d5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7922
        },
        {
            "id": "f88c3285-1fd9-4404-a342-9fcb33fb1033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7924
        },
        {
            "id": "42179e3e-9c37-4ca6-88c3-0bb8cbe4037f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7926
        },
        {
            "id": "d0d039f6-1460-4f03-a2b9-7d2ffa500bec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7928
        },
        {
            "id": "758e0011-1391-4ac3-8bb5-142c24c833fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8216
        },
        {
            "id": "bbbe4ce8-dfd7-4e58-8ecf-1c22df98e7f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8220
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 10,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}